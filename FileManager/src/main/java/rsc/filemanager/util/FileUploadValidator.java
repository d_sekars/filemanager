package rsc.filemanager.util;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;
import rsc.filemanager.model.FileUpload;
 
@Component
public class FileUploadValidator implements Validator {
     
    public boolean supports(Class<?> clazz) {
        return FileUpload.class.isAssignableFrom(clazz);
    }
 
    public void validate(Object obj, Errors errors) {
    	FileUpload fileUpload = new FileUpload();
    	int index=0;
    	
	    	for (@SuppressWarnings("unused") MultipartFile file : fileUpload.getFiles()) {
	    	if(fileUpload.getFiles()!=null){
	                if (fileUpload.getFiles().size() == 0) {
	                    errors.rejectValue("files["+index+"].file", "missing.file");
	                }
	            }
	            index++;
	        }
    }
}