package rsc.filemanager.service;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.List;
import rsc.filemanager.model.User;

public interface UserService {
 
    User findUserByUsername(String username);
    
    void saveUser(User user);
    
    void updateUsername(String oldUsername, String newUsername);
    
    void deleteUser(String username);
     
    List<User> findAllUsers();
    
    boolean isUsernameUnique(String username);     
}
