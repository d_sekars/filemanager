package rsc.filemanager.service;

/**
 * Adopt code from:
 * - http://websystique.com/spring-security/spring-security-4-hibernate-annotation-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rsc.filemanager.model.User;
import rsc.filemanager.model.UserAuthority;
 
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService{

	
    @Autowired
    private UserService userService;
     
    @Transactional(readOnly=true)
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        User user = userService.findUserByUsername(username);
        System.out.println("User : "+user);
        if(user==null){
            System.out.println("User not found");
            throw new UsernameNotFoundException("Username not found");
        }
            return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), 
                 user.getStatus().equals("active"), true, true, true, getUserAuthorities(user));
    }
 
     
    private List<GrantedAuthority> getUserAuthorities(User user){
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
         
        for(UserAuthority userAuthority : user.getUserAuthorities()){
            System.out.println("UserAuthorities : "+userAuthority);
            authorities.add(new SimpleGrantedAuthority("ROLE_"+userAuthority.getUserAuthority()));
            //authorities.add(new SimpleGrantedAuthority(userAuthority.getUserAuthority()));
        }
        
        System.out.print("authorities :"+authorities);
        return authorities;
    }
     
}