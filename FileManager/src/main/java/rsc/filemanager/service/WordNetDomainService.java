package rsc.filemanager.service;

import rsc.filemanager.model.WordNetDomain;

/**
 * 
 * @author Diah S R
 *
 */

public interface WordNetDomainService {

	public void saveDomain(WordNetDomain domain);
	
}
