package rsc.filemanager.service.impl;

/**
 * 
 * @author Diah S R
 *
 */

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rsc.filemanager.dao.AnnotationDao;
import rsc.filemanager.model.Annotation;
import rsc.filemanager.service.AnnotationService;

@Service("annotation")
@Transactional
public class AnnotationServiceImpl implements AnnotationService {
	
	@Autowired
	AnnotationDao dao;
	
	public List<Annotation> findAllAnnoByDocId(int docId) {
		return dao.findAllAnnoByDocId(docId);
	}
	
	public List<Annotation> findAllAnnoByCatDocId(int annoCatId, int docId) {
		return dao.findAllAnnoByCatDocId(annoCatId, docId);
	}
	
	public void save(Annotation annotation) {
		dao.save(annotation);
	}
	
	public int findBiggestId() {
    	return dao.findBiggestId();
    }
	
}
