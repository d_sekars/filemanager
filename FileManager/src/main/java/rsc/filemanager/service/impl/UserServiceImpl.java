package rsc.filemanager.service.impl;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rsc.filemanager.dao.UserDao;
import rsc.filemanager.model.User;
import rsc.filemanager.service.UserService;
 
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
 
    @Autowired
    private UserDao dao;
 
    public User findUserByUsername(String username) {
        return dao.findByUsername(username);
    }
    
    public void saveUser(User user) {
        dao.saveUser(user);
    }
    
    public void updateUsername(String oldUsername, String newUsername){
    	dao.updateUsername(oldUsername, newUsername);
    }
    
    public void deleteUser(String username) {
        dao.deleteUser(username);
    }
 
    public List<User> findAllUsers() {
        return dao.findAllUsers();
    }
 
    public boolean isUsernameUnique(String username) {
        User user = findUserByUsername(username);
        return ( user == null || ((username != null) && (user.getUsername() == username)));
    }
}
