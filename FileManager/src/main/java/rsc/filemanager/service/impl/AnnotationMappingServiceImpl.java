package rsc.filemanager.service.impl;

/**
 * 
 * @author Diah S R
 *
 */

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rsc.filemanager.dao.AnnotationMappingDao;
import rsc.filemanager.model.AnnotationMapping;
import rsc.filemanager.service.AnnotationMappingService;

@Service("annotationMapping")
@Transactional
public class AnnotationMappingServiceImpl implements AnnotationMappingService {
	
	@Autowired
	AnnotationMappingDao dao;
	
	public List<AnnotationMapping> findAllAnnoByDocId(int docId) {
		return dao.findAllAnnoByDocId(docId);
	}
	
	public List<AnnotationMapping> findAllAnnoByCatDocId(int annoCatId, int docId) {
		return dao.findAllAnnoByCatDocId(annoCatId, docId);
	}
	
	public AnnotationMapping findAnnoByAllId(int annoCatId, int docId, int docDicId) {
		return dao.findAnnoByAllId(annoCatId, docId, docDicId);
	}
	
	public AnnotationMapping findAnnoByAllId(AnnotationMapping am) {
		return dao.findAnnoByAllId(am.getAnnoCatId(), am.getDocId(), am.getDocDicId());
	}
	
	public void save(AnnotationMapping annotationMapping) {
		if(this.findAnnoByAllId(annotationMapping) != null) return;
		dao.save(annotationMapping);
	}
	
	public void deleteByDocId(int docId) {
		dao.deleteByDocId(docId);
	}
	
	public int findBiggestId() {
    	return dao.findBiggestId();
    }
	
}
