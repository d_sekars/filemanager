package rsc.filemanager.service.impl;

/**
 * @author Diah S R
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rsc.filemanager.dao.UserGrantedAuthDao;
import rsc.filemanager.model.UserGrantedAuth;
import rsc.filemanager.service.UserGrantedAuthService;

@Service("userGrantedAuth")
@Transactional
public class UserGrantedAuthServiceImpl implements UserGrantedAuthService {
	
	@Autowired
	UserGrantedAuthDao dao;
	
	public void save(UserGrantedAuth userGrantedAuth){
		dao.save(userGrantedAuth);
	}
	
	public void updateGrantedUser(String oldGrantedUser, String newGrantedUser) {
		dao.updateGrantedUser(oldGrantedUser, newGrantedUser);
	}
}
