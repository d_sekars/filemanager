package rsc.filemanager.service.impl;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rsc.filemanager.dao.UserDocumentDao;
import rsc.filemanager.model.UserDocument;
import rsc.filemanager.service.UserDocumentService;
 
@Service("userDocumentService")
@Transactional
public class UserDocumentServiceImpl implements UserDocumentService {
 
    @Autowired
    UserDocumentDao dao;
 
    public UserDocument findByDocId(int docId) {
        return dao.findByDocId(docId);
    }
    
    public UserDocument findByDocName(String username, String docName){
    	return dao.findByDocName(username, docName);
    }
    
    public int findBiggestDocId() {
    	return dao.findBiggestDocId();
    }
    
    public List<UserDocument> findAll() {
        return dao.findAll();
    }
 
    public List<UserDocument> findAllByUsername(String username) {
        return dao.findAllByUsername(username);
    }
    
    public int countDocuments(){
    	List<UserDocument> countAll = findAll();
    	return countAll.size();
    }
     
    public void saveDocument(UserDocument document){
        dao.save(document);
    }
    
    public void updateUser(String oldUser, String newUser) {
    	dao.updateUser(oldUser, newUser);
    }
    
    public void updateContent(int docId, byte[] docContent){
    	dao.updateContent(docId, docContent);
    }
 
    public void deleteByDocId(int docId){
        dao.deleteByDocId(docId);
    }
}
