package rsc.filemanager.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rsc.filemanager.dao.WordNetDomainDao;
import rsc.filemanager.model.WordNetDomain;
import rsc.filemanager.service.WordNetDomainService;

/**
 * 
 * @author Diah S R
 *
 */

@Service("wordNetDomainService")
@Transactional
public class WordNetDomainServiceImpl implements WordNetDomainService {
	
	@Autowired
    private WordNetDomainDao dao;

	public void saveDomain(WordNetDomain domain){
		dao.saveDomain(domain);
	}
	
}
