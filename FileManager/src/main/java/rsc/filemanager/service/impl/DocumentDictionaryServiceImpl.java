package rsc.filemanager.service.impl;

import java.util.ArrayList;

/**
 * @author Diah S R
 */

import java.util.List;
import java.util.Set;

/**
 * @author Diah S R
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rsc.filemanager.dao.DocumentDictionaryDao;
import rsc.filemanager.model.DocumentDictionary;
import rsc.filemanager.model.DocumentPosting;
import rsc.filemanager.service.DocumentDictionaryService;
import rsc.filemanager.service.DocumentPostingService;

@Service("documentDictionaryService")
@Transactional
public class DocumentDictionaryServiceImpl implements DocumentDictionaryService {
	
	@Autowired
	DocumentDictionaryDao dao;
	
	@Autowired
	DocumentPostingService documentPostingService;
	
	public DocumentDictionary findByToken(String token) {
		return dao.findByToken(token);
	}
	
	public List<DocumentDictionary> findByTokenLike(String token) {
		return dao.findByTokenLike(token);
	}
	
	public List<Integer> findIdByTokenLike(String token) {
		List<Integer> id = new ArrayList<Integer>();
		if(this.alreadyExists(token))
			for(DocumentDictionary dd: this.findByTokenLike(token))
				id.add(dd.getDictionaryId());
		return id;		
	}
	
	public int findIdByToken(String token) {
		DocumentDictionary dd = dao.findByToken(token);
		return dd.getDictionaryId();
	}
	
	public String findTokenById(int dcId){
		DocumentDictionary dd = dao.findById(dcId);
		return dd.getDictionaryToken();
	}
	
	public boolean alreadyExists(String token) {
		DocumentDictionary dd = dao.findByToken(token);
		if(dd != null) return true;
		return false;
	}
	
	public boolean alreadyExistsLike(String token) {
		List<DocumentDictionary> dd = dao.findByTokenLike(token);
		if(dd.size()>0) return true;
		return false;
	}
	
	public void saveDictionary(String token) {
		if (!this.alreadyExists(token)) {
			DocumentDictionary dictionary = new DocumentDictionary();
			dictionary.setDictionaryToken(token);
			dictionary.setDocFrequency(1);
			dao.saveDictionary(dictionary);
		} else this.increaseDocFrequency(token);
	}
	
	public void saveDictionary(int docId, String token) {
		List<Integer> dp = documentPostingService.findAllDcId(docId);
		if (this.alreadyExists(token)) {
			int dd = this.findIdByToken(token);
			if(dp.contains(dd)) return;
		}
		this.saveDictionary(token);
	}
	
	public void saveDictionaryList(String[] token) {
		for (String t: token) {
			this.saveDictionary(t);
		}
	}
	
	public void increaseDocFrequency(String token) {
		DocumentDictionary dd = dao.findByToken(token);
		dao.updateDocFrequency(token, dd.getDocFrequency() + 1);
	}
	
	public void decreaseDocFrequency(String token) {
		DocumentDictionary dd = dao.findByToken(token);
		if ( dd.getDocFrequency() > 0 ) {
			dao.updateDocFrequency(token, dd.getDocFrequency() - 1);
		}
	}
	
	public void decreaseListOfDocFrequency(List<Integer> dcId) {
		for (int id: dcId){
			DocumentDictionary dd = dao.findById(id);
			this.decreaseDocFrequency(dd.getDictionaryToken());
		}		
	}
	
	public void deleteToken(int dictionaryId) {
		dao.deleteToken(dictionaryId);
	}
	
	public List<DocumentDictionary> findAll(){
		return dao.findAll();
	}
	
	public int countDictionaryToken(){
		List<DocumentDictionary> countAll = findAll();
		return countAll.size();
	}
}
