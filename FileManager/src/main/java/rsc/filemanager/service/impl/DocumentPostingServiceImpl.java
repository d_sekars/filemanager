package rsc.filemanager.service.impl;

/**
 * 
 * @author Diah S R
 *
 */

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rsc.filemanager.dao.DocumentPostingDao;
import rsc.filemanager.model.DocumentPosting;
import rsc.filemanager.service.DocumentPostingService;

@Service("documentPosting")
@Transactional
public class DocumentPostingServiceImpl implements DocumentPostingService {
	
	@Autowired
	DocumentPostingDao dao;
	
	public List<DocumentPosting> findAllByDocId(int docId) {
		return dao.findByDocId(docId);
	}
	
	public List<Integer> findAllDcId(int docId) {
		List<DocumentPosting> docPost = dao.findByDocId(docId);
		List<Integer> dictionaryId = new ArrayList<Integer>();
		for (DocumentPosting dp: docPost) {
			dictionaryId.add(dp.getDictionaryId());
		}
		return dictionaryId;
	}
	
	public DocumentPosting findPostingByDocId(int docId, int dictioId){
		return dao.findPostingByDocId(docId, dictioId);
	}
	
	public void save(DocumentPosting documentPosting) {
		dao.save(documentPosting);
	}
	
	public void deleteByDocId(int docId) {
		dao.deleteByDocId(docId);
	}
}
