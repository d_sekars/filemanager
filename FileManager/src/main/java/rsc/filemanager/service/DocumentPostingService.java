package rsc.filemanager.service;

import java.util.List;

import rsc.filemanager.model.DocumentPosting;

/**
 * 
 * @author Diah S R
 *
 */

public interface DocumentPostingService {
	
	List<DocumentPosting> findAllByDocId(int docId);
	
	DocumentPosting findPostingByDocId(int docId, int dictioId);
	
	List<Integer> findAllDcId(int docId);

	void save(DocumentPosting documentPosting);
	
	void deleteByDocId(int docId);
}
