package rsc.filemanager.service;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.List;
import rsc.filemanager.model.UserDocument;
 
public interface UserDocumentService {
 
    UserDocument findByDocId(int docId);
    
    UserDocument findByDocName(String username, String docName);
    
    int findBiggestDocId();
 
    List<UserDocument> findAll();
     
    List<UserDocument> findAllByUsername(String username);
    
    /* Get number of document(s) available in database */
    int countDocuments();
     
    void saveDocument(UserDocument document);
    
    void updateUser(String oldUser, String newUser);
    
    /* Update document's content */
    void updateContent(int docId, byte[] docContent);
     
    void deleteByDocId(int docId);
}
