package rsc.filemanager.service;

/**
 * @author Diah S R
 */

import java.util.List;
import rsc.filemanager.model.DocumentDictionary;

public interface DocumentDictionaryService {
	
	DocumentDictionary findByToken(String token);
	
	List<DocumentDictionary> findByTokenLike(String token);
	
	List<Integer> findIdByTokenLike(String token);
	
	int findIdByToken(String token);
	
	String findTokenById(int dcId);
	
	int countDictionaryToken();
	
	boolean alreadyExists(String token);
	
	boolean alreadyExistsLike(String token);
    
    void saveDictionary(String token);
    
    void saveDictionary(int docId, String token);
    
    void saveDictionaryList(String[] token);
    
    void increaseDocFrequency(String token);
    
    void decreaseDocFrequency(String token);
    
    void decreaseListOfDocFrequency(List<Integer> dcId);
    
    void deleteToken(int dictionaryId);
     
    List<DocumentDictionary> findAll();
    
}
