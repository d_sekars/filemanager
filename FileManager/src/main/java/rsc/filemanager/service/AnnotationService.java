package rsc.filemanager.service;

import java.util.List;

import rsc.filemanager.model.Annotation;

/**
 * 
 * @author Diah S R
 *
 */

public interface AnnotationService {
	
	List<Annotation> findAllAnnoByDocId(int docId);
	
	List<Annotation> findAllAnnoByCatDocId(int annoCatId, int docId);
	
	void save(Annotation annotation);
	
	int findBiggestId();
}
