package rsc.filemanager.service;

import java.util.List;

import rsc.filemanager.model.AnnotationMapping;

/**
 * 
 * @author Diah S R
 *
 */

public interface AnnotationMappingService {
	
	List<AnnotationMapping> findAllAnnoByDocId(int docId);
	
	List<AnnotationMapping> findAllAnnoByCatDocId(int annoCatId, int docId);
	
	AnnotationMapping findAnnoByAllId(int annoCatId, int docId, int docDicId);
	
	AnnotationMapping findAnnoByAllId(AnnotationMapping am);
	
	void save(AnnotationMapping annotationMapping);
	
	void deleteByDocId(int docId);
	
	int findBiggestId();
}
