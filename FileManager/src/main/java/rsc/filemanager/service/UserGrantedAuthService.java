package rsc.filemanager.service;

/**
 * @author Diah S R
 */

import rsc.filemanager.model.UserGrantedAuth;

public interface UserGrantedAuthService {

	void save(UserGrantedAuth userGrantedAuth);
	
	void updateGrantedUser(String oldGrantedUser, String newGrantedUser);
}
