package rsc.filemanager.controller;

/**
 * Adopt code from:
 * - https://www.mkyong.com/spring-security/spring-security-custom-login-form-annotation-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.io.IOException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import rsc.filemanager.model.Principal;

@Controller
public class MainController {
	
	private Principal principal = new Principal();
	
	/** First page to display before login to the system */	
	@RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
	public ModelAndView loginPage(@RequestParam(value = "error",required = false) String error,
	@RequestParam(value = "logout",	required = false) String logout) {
		
		ModelAndView model = new ModelAndView();
		
		
		if (error != null) {
			model.addObject("error", "Invalid credentials provided.");
		}

		if (logout != null) {
			model.addObject("message", "Logged out from File Manager successfully.");
		}
		
		model.addObject("title", "Welcome to File Manager");
		model.setViewName("login");
		return model;
	}
	
	@RequestMapping(value = { "/home"}, method = RequestMethod.GET)
	public ModelAndView homePage() throws IOException {
		ModelAndView model = new ModelAndView();
		model.addObject("title", "File Manager");
		model.addObject("username", principal.getUsername());
		model.setViewName("home");		

		return model;
	}
	
}
