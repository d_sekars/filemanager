package rsc.filemanager.controller;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import rsc.filemanager.model.User;
import rsc.filemanager.model.UserGrantedAuth;
import rsc.filemanager.service.UserDocumentService;
import rsc.filemanager.service.UserGrantedAuthService;
import rsc.filemanager.service.UserService;

@Controller
public class UserController {	
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserDocumentService userDocumentService;
	
	@Autowired
	UserGrantedAuthService userGrantedAuthService;
     
	@Autowired
    MessageSource messageSource;
	
	private User user = new User();

	/* This method will list all existing users. */
    @RequestMapping(value = { "/userlist" }, method = RequestMethod.GET)
    public ModelAndView listUsersPage() {
    	ModelAndView model = new ModelAndView();
    	List<User> users = userService.findAllUsers();
		model.addObject("users", users);
		model.setViewName("userlist");
		return model;
	}
    
    /* This method will provide the medium to add a new user. */
    @RequestMapping(value = { "/signup" }, method = RequestMethod.GET)
    public ModelAndView signUpPage() {
		ModelAndView model = new ModelAndView();
		model.addObject("user", user);
		model.addObject("edit", false);
		model.setViewName("registration");
		return model;
	}
    
    /* This method will be called on form submission, handling POST request for
     * saving user in database. It also validates the user input */
    @RequestMapping(value = { "/signup" }, method = RequestMethod.POST)
    public ModelAndView saveUser(@Valid User user, BindingResult result) {
    	ModelAndView model = new ModelAndView();
 
        if (result.hasErrors()) {
        	model.setViewName("registration");
    		return model;
        }
 
        /* Preferred way to achieve uniqueness of field [username] should be implementing custom @Unique annotation 
         * and applying it on field [username] of Model class [User].
         * 
         * Below mentioned peace of code [if block] is to demonstrate that you can fill custom errors outside the validation
         * framework as well while still using internationalized messages. */
        if(!userService.isUsernameUnique(user.getUsername())){
            FieldError usernameError = new FieldError("user","username",messageSource.getMessage("non.unique.username", new String[]{user.getUsername()}, Locale.getDefault()));
            result.addError(usernameError);
            model.setViewName("registration");
            return model;
        }
         
        user.setStatus("active");
        userService.saveUser(user);
        
        /* Set user's granted authority, default granted authority is USER */
        UserGrantedAuth userGrantedAuth = new UserGrantedAuth();
        userGrantedAuth.setGrantedUser(user.getUsername());
        userGrantedAuth.setGrantedAuth("USER");
        userGrantedAuthService.save(userGrantedAuth);
         
        model.addObject("user", user);
        model.addObject("success", "User " + user.getUsername() + " registered successfully");
        model.setViewName("registrationsuccess");
        return model;
    }
    
    /* This method will provide the medium to update an existing user. */
    @RequestMapping(value = { "/edit-user-{username}" }, method = RequestMethod.GET)
    public ModelAndView editUserPage(@PathVariable String username) {
    	ModelAndView model = new ModelAndView();
        User user = userService.findUserByUsername(username);
        model.addObject("user", user);
        model.addObject("username", user.getUsername());
        model.addObject("edit", true);
        model.setViewName("registration");
		return model;
    }
    
    /* This method will be called on form submission, handling POST request for
     * updating user in database. It also validates the user input */
    @RequestMapping(value = { "/edit-user-{username}" }, method = RequestMethod.POST)
    public ModelAndView updateUser(@Valid User user, BindingResult result,
            @PathVariable String username) {
    	ModelAndView model = new ModelAndView();
 
        if (result.hasErrors()) {
        	model.setViewName("registration");
    		return model;
        }
        
        userService.updateUsername(username, user.getUsername());
        userGrantedAuthService.updateGrantedUser(username, user.getUsername());
        userDocumentService.updateUser(username, user.getUsername());
 
        model.addObject("success", "User " + user.getUsername() + " updated successfully");
        model.addObject("username", user.getUsername());
        model.setViewName("registrationsuccess");
		return model;
    }
    
    /* This method will delete an user by it's username value. */
    @RequestMapping(value = { "/delete-user-{username}" }, method = RequestMethod.GET)
    public ModelAndView deleteUser(@PathVariable String username) {
    	ModelAndView model = new ModelAndView("redirect:/userlist");
        userService.deleteUser(username);
		return model;
    }
}