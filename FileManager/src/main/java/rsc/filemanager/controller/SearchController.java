package rsc.filemanager.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import rsc.filemanager.common.AnnotationProcess;
import rsc.filemanager.common.IrPreprocessing;
import rsc.filemanager.common.IrWeighting;
import rsc.filemanager.common.PDFBoxManager;
import rsc.filemanager.model.Annotation;
import rsc.filemanager.model.AnnotationMapping;
import rsc.filemanager.model.DocumentDictionary;
import rsc.filemanager.model.DocumentPosting;
import rsc.filemanager.model.SearchDocument;
import rsc.filemanager.model.SearchIR;
import rsc.filemanager.model.User;
import rsc.filemanager.model.UserDocument;
import rsc.filemanager.service.AnnotationMappingService;
import rsc.filemanager.service.AnnotationService;
import rsc.filemanager.service.DocumentDictionaryService;
import rsc.filemanager.service.DocumentPostingService;
import rsc.filemanager.service.UserDocumentService;
import rsc.filemanager.service.UserService;

/**
 * 
 * @author Diah Sekarsari R
 *
 */

@Controller
public class SearchController {
	
	@Autowired
    UserService userService;
	
	@Autowired
    DocumentDictionaryService documentDictionaryService;
	
	@Autowired
    DocumentPostingService documentPostingService;
	
	@Autowired
    UserDocumentService userDocumentService;
	
	@Autowired
	AnnotationMappingService annotationMappingService;
	
	private IrPreprocessing  irp = new IrPreprocessing();
	
	/* This method will direct user to search document page. */
	@RequestMapping(value = { "/search-document-{username}" }, method = RequestMethod.GET)
    public ModelAndView searchDocument(@PathVariable String username) {
		
		ModelAndView model = new ModelAndView();
		
        User user = userService.findUserByUsername(username);
        model.addObject("user", user);
        
        SearchDocument searchDocument = new SearchDocument();
        searchDocument.setAnnoReference("file");
        model.addObject("searchDocument", searchDocument);
        
        model.setViewName("searchDocument");
	    return model;
    }
	
	/* This method will handle search document process. */
	@RequestMapping(value = { "/search-document-{username}" }, method = RequestMethod.POST)
    public ModelAndView searchDocument(@Valid SearchDocument searchDocument,
    		BindingResult result, @PathVariable String username) throws IOException{
				
		User user = userService.findUserByUsername(username);
		
        if (result.hasErrors()) {
        	ModelAndView model = new ModelAndView();
            System.out.println("validation errors");
            model.addObject("user", user);             
            model.setViewName("searchDocument");
    	    return model;
        } else {     	
        	ModelAndView model = new ModelAndView("redirect:/search-document-" + username);   
        	
        	/* Put query into an object to display it in screen */
        	String query = searchDocument.getSearchQuery();
        	model.addObject("query", query);
        	
        	/* Split query into token(s) */
        	String[] queries = irp.tokenization(query);
        	List<SearchIR> searchResult = new ArrayList<SearchIR>();
        	
        	/* IR block
        	 * Calculate execution time 
        	 * */
    		long irStartTime = System.currentTimeMillis();
        	
    		searchResult = this.sortDescSearchIr(this.vsmWeighting(queries));
        	
        	long irEndTime = System.currentTimeMillis();
        	
        	model.addObject("irResult", searchResult);
        	model.addObject("irTime", (irStartTime - irEndTime));
        	
        	int irRow = searchResult.size();
        	System.out.println("IR Result: " + irRow);
        	            
            /* Annotation block
        	 * Calculate execution time 
        	 * */
            long annoStartTime = System.currentTimeMillis();
            
            if(searchDocument.getAnnoReference().equalsIgnoreCase("file")){
            	searchResult = this.sortDescSearchIr(this.annoSearch(username, queries));
            } else if(searchDocument.getAnnoReference().equalsIgnoreCase("db")){
            	searchResult = this.sortDescSearchIr(this.annoDbSearch(username, queries));
            }
        	
        	long annoEndTime = System.currentTimeMillis();
        	
        	model.addObject("annoResult", searchResult);
        	model.addObject("annoTime", (annoStartTime - annoEndTime));
            
        	int anRow = searchResult.size();
        	List<Integer> irRows = new ArrayList<Integer>();
            List<Integer> anRows = new ArrayList<Integer>();
            
            for(int i = 0; i < irRow; i++){
            	irRows.add(i, i);
            }
            for(int i = 0; i < anRow; i++){
            	anRows.add(i, i);
            }
            
            model.addObject("irRows", irRows);
            model.addObject("anRows", anRows);
            model.addObject("user", user);
            model.setViewName("searchDocument");
            
    	    return model;
        }
    }
	
	/* This method will handle download document process. */
	@RequestMapping(value = { "/download-annotation-{username}-{documentId}" }, method = RequestMethod.GET)
    public ModelAndView downloadAnnotation(@PathVariable String username, @PathVariable int documentId, 
    		HttpServletResponse response) throws IOException {
		ModelAndView model = new ModelAndView("redirect:/search-document-" + username);

		/* Read all files inside folder */
		PDFBoxManager pdfBox = new PDFBoxManager();
		File folder = new File(System.getProperty("catalina.home") + "\\uploads");
		List<File> listFile = pdfBox.listFilesForFolder(folder);
		
		UserDocument document = userDocumentService.findByDocId(documentId);
		
		for(File lf: listFile){
			if(document.getDocName().equals(lf.getName())){
				response.setContentType(document.getDocType());
		        response.setContentLength(Files.readAllBytes(lf.toPath()).length);
		        response.setHeader("Content-Disposition","attachment; filename=\"" + document.getDocName() +"\"");
		        
		        FileCopyUtils.copy(Files.readAllBytes(lf.toPath()), response.getOutputStream());
			}
		}
		
		User user = userService.findUserByUsername(username);
		model.addObject("user", user);
  	    return model;
    }
	
	private List<SearchIR> sortDescSearchIr(List<SearchIR> input){
		
		for(int i = 0; i < input.size(); i++){
			int bigger = i;
			for(int j = i + 1; j < input.size(); j++){
				if(input.get(bigger).getDocumentScore() < input.get(j).getDocumentScore()) bigger = j;
			}
			SearchIR temp = input.get(bigger);
			input.set(bigger, input.get(i));
			input.set(i, temp);
		}
		
		return input;
	}
	
	private List<SearchIR> annoDbSearch(String username, String[] queries){
		List<SearchIR> listSearchIr = new ArrayList<SearchIR>();
		
		List<UserDocument> lud = userDocumentService.findAllByUsername(username);
		
		List<Integer> dictioId = new ArrayList<Integer>();
		for(String q: queries){
			if(documentDictionaryService.alreadyExistsLike(q)) dictioId.addAll(documentDictionaryService.findIdByTokenLike(q));
		}
		
		for(UserDocument ud: lud){
			List<AnnotationMapping> lam = annotationMappingService.findAllAnnoByDocId(ud.getDocId());
			int score = 0;
			for(AnnotationMapping am: lam){
				if(dictioId.contains(am.getDocDicId())) score++;
			}
			if(score <= 0) continue;
			SearchIR searchIr = new SearchIR();
			searchIr.setDocumentContent(ud.getDocContent());
			searchIr.setDocumentId(ud.getDocId());
			searchIr.setDocumentName(ud.getDocName());
			searchIr.setDocumentScore(score);
			listSearchIr.add(searchIr);
		}
		
		return listSearchIr;
	}
	
	/* Method to get searching result based on annotation */
	private List<SearchIR> annoSearch(String username, String[] queries) throws IOException{
		List<SearchIR> listSearchIr = new ArrayList<SearchIR>();
		File folder = new File(System.getProperty("catalina.home") + "\\uploads");
		
		/* Read all files inside folder */
		PDFBoxManager pdfBox = new PDFBoxManager();
		List<File> listFile = pdfBox.listFilesForFolder(folder);
		
		AnnotationProcess anno = new AnnotationProcess();
		
		for(File lf: listFile){
			pdfBox.setFilePath(lf.getAbsolutePath());
			String pdfContent = pdfBox.ToText(1,1);
			String[] textArr = irp.tokenizationGetAll(pdfContent);
			
			Map<String, Integer> wordsMap = anno.wordsScoreMapping(lf, textArr);
			double score = 0;
			try {
				pdfBox.readDocumentInformation(lf, "annotation");
				String result = pdfBox.retrieveDocumentInformation(lf, "annotation");
				List<String> splitResult = Arrays.asList(result.split(";"));
				for(String q: queries) {
//					System.out.println("Query: " + q);
					for(String sr: splitResult) {
//						System.out.println("Annotation: " + sr);
						if(sr.contains(q) && wordsMap.containsKey(q)) score += wordsMap.get(q);
						else if(sr.contains(q)) score++;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try{
				UserDocument ud = userDocumentService.findByDocName(username, lf.getName());
				
				/* Add result into list */
				if(score > 0){
					SearchIR searchIr = new SearchIR();
		    		searchIr.setDocumentId(ud.getDocId());
		    		searchIr.setDocumentName(ud.getDocName());
		    		searchIr.setDocumentContent(ud.getDocContent());
		    		searchIr.setDocumentScore(score);
		    		listSearchIr.add(searchIr);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return listSearchIr;
	}
	
	/* Method to calculate Vector Space Model weighting */
	private List<SearchIR> vsmWeighting(String[] queries){
		/* Calculate N (number of document(s) inside the dictionary) */
    	int n = userDocumentService.countDocuments();
    	
    	/* List available document(s) */
    	List<UserDocument> listUD = userDocumentService.findAll();
    	
    	/* Mapping term frequency and document frequency */
    	Map<String, Double> mapDf = new HashMap<>();
    	Map<String, Double> mapTfD = new HashMap<>();
    	Map<String, Double> mapTfQ = new HashMap<>();
    	
    	/* Mapping term and dictionary identification */
    	Map<String, Integer> mapDictioId = new HashMap<>();
    	
    	/* Mapping term frequency (query) and document frequency */
    	for(String q: queries){
    		System.out.println("Query: " + q);
    		
    		/* Calculate frequency */
    		if(mapTfQ.size()>0 && mapTfQ.containsKey(q)) {
    			Double freq = mapTfQ.get(q);
    			mapTfQ.put(q, freq + (double) 1);
    		} else {
    			mapTfQ.put(q, (double) 1);
    		}
    			
    		List<DocumentDictionary> listDd = new ArrayList<DocumentDictionary>(); 
    		listDd.addAll(documentDictionaryService.findByTokenLike(q));
    		System.out.println("Dictionary: " + listDd.size());
    		if(listDd.size() > 0) {
    			for(DocumentDictionary dd: listDd) {
    				System.out.println("Dictionary: " + listDd.size() + " with frequency: " + dd.getDocFrequency());
    				
    				if(!mapTfQ.containsKey(dd.getDictionaryToken()))
    					mapTfQ.put(dd.getDictionaryToken(), (double) 1);
    				
    				mapDf.put(dd.getDictionaryToken(), (double) dd.getDocFrequency());
    				mapDictioId.put(dd.getDictionaryToken(), dd.getDictionaryId());
    			}
    		}else{
    			mapDf.put(q, (double) 0);
    			mapDictioId.put(q, 0);
    		}
    		
    	}        
    	
		List<SearchIR> listSearchIr = new ArrayList<SearchIR>();

    	/* Calculate Vector Space Model weighting for each document(s) */
    	for(UserDocument ud:listUD){
    		System.out.println("Document name: " + ud.getDocName());

    		for (Map.Entry<String, Integer> m : mapDictioId.entrySet()) {
    			System.out.println("Token (" + m.getValue() + "): " + m.getKey());
    			DocumentPosting dp = documentPostingService.findPostingByDocId(ud.getDocId(), m.getValue());
    			if(dp == null){
    				mapTfD.put(m.getKey(), (double) 0);
    			} else {
    				mapTfD.put(m.getKey(), (double) dp.getFrequency());
    			}
    		}
    		
    		/* VSM calculation */
    		IrWeighting irw = new IrWeighting();
    		double score = irw.vcmScore(n, mapTfQ, mapTfD, mapDf);
    		
    		if(score >= 0){
    			SearchIR searchIr = new SearchIR();
        		searchIr.setDocumentId(ud.getDocId());
        		searchIr.setDocumentName(ud.getDocName());
        		searchIr.setDocumentContent(ud.getDocContent());
        		searchIr.setDocumentScore(score);
        		listSearchIr.add(searchIr);
    		}
    	}
		
		return listSearchIr;
	}
	
	@ModelAttribute("annoReferenceList")
	public Map<String, String> getAnnoReferenceList(){
		Map<String, String> annoReferenceList = new HashMap<String, String>();
		annoReferenceList.put("file", "PDF File");
		annoReferenceList.put("db", "Database");
		return annoReferenceList;
	}

}
