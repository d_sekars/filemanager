package rsc.filemanager.controller;

/**
 * Adopt code from:
 * - http://www.journaldev.com/2573/spring-mvc-file-upload-example-single-multiple-files
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import rsc.filemanager.common.AnnotationProcess;
import rsc.filemanager.common.IrPreprocessing;
import rsc.filemanager.common.JavaArray;
import rsc.filemanager.common.PDFBoxManager;
import rsc.filemanager.model.AnnotationMapping;
import rsc.filemanager.model.DocumentDictionary;
import rsc.filemanager.model.DocumentPosting;
import rsc.filemanager.model.FileUpload;
import rsc.filemanager.model.User;
import rsc.filemanager.model.UserDocument;
import rsc.filemanager.service.AnnotationMappingService;
import rsc.filemanager.service.DocumentDictionaryService;
import rsc.filemanager.service.DocumentPostingService;
import rsc.filemanager.service.UserDocumentService;
import rsc.filemanager.service.UserService;
 
@Controller
public class DocumentController {
	
	@Autowired
    UserService userService;
	
	@Autowired
    UserDocumentService userDocumentService;
	
	@Autowired
	DocumentDictionaryService documentDictionaryService;
	
	@Autowired
	DocumentPostingService documentPostingService;
	
	@Autowired
	AnnotationMappingService annotationMappingService;
	
	private static final Logger logger = LoggerFactory
			.getLogger(DocumentController.class);
	private static AnnotationProcess anno = new AnnotationProcess();
	
	@RequestMapping(value = "/uploadfile", method = RequestMethod.GET)
	public ModelAndView uploadFilePage() {
		ModelAndView model = new ModelAndView();
	    model.setViewName("uploadfile");
	    return model;
	}
	
	@RequestMapping(value = "/uploadfile", method = RequestMethod.POST)
	public ModelAndView uploadFileSuccess(
		@ModelAttribute("filesToUpload") FileUpload filesToUpload) {
		ModelAndView model = new ModelAndView();
		List<MultipartFile> files = filesToUpload.getFiles();
		List<String> fileNames = new ArrayList<String>();
			
		if(null != files && files.size() > 0) {
			for (MultipartFile file : files) {

				String fileName = file.getOriginalFilename();
				fileNames.add(fileName);
				
				// Creating the directory to store file
                String rootPath = System.getProperty("catalina.home");
                File dir = new File(rootPath + File.separator + "uploads");
				if (!dir.exists())
					dir.mkdirs();
				
				// Create the file on server
				File serverFile = new File(dir.getAbsolutePath() + File.separator + fileName);
				try {
					file.transferTo(serverFile);
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				logger.info("Server File Location="
						+ serverFile.getAbsolutePath());

			}
		}
		
		model.addObject("files", fileNames);
		model.setViewName("uploadfilesuccess");
	    return model;
	}
	
	/* This method will direct user to document's upload page. */
	@RequestMapping(value = { "/add-document-{username}" }, method = RequestMethod.GET)
    public ModelAndView addDocuments(@PathVariable String username) {
		ModelAndView model = new ModelAndView();
        User user = userService.findUserByUsername(username);
        model.addObject("user", user);
 
        FileUpload fileUpload = new FileUpload();
        model.addObject("fileUpload", fileUpload);
 
        List<UserDocument> documents = userDocumentService.findAllByUsername(username);
        model.addObject("documents", documents);
        
        model.setViewName("manageDocuments");
	    return model;
    }
	
	/* This method will handle document's upload process. */
	@RequestMapping(value = { "/add-document-{username}" }, method = RequestMethod.POST)
    public ModelAndView uploadDocument(@Valid FileUpload fileUpload, BindingResult result, 
    		@PathVariable String username) throws IOException{
		
        if (result.hasErrors()) {
        	ModelAndView model = new ModelAndView();
            System.out.println("validation errors");
            User user = userService.findUserByUsername(username);
            model.addObject("user", user);
 
            List<UserDocument> documents = userDocumentService.findAllByUsername(username);
            model.addObject("documents", documents);
             
            model.setViewName("manageDocuments");
    	    return model;
        } else {
        	ModelAndView model = new ModelAndView("redirect:/add-document-" + username);
             
            System.out.println("Fetching file");
             
            User user = userService.findUserByUsername(username);
            model.addObject("user", user);
            
            this.saveDocument(fileUpload, user);
    	    
            return model;
        }
    }
	
	private void saveDocument(FileUpload fileUpload, User user) throws IOException{
        
        UserDocument document = new UserDocument();
         
        MultipartFile multipartFile = fileUpload.getFile();
        
        if (!multipartFile.getContentType().contains("pdf")) return; /*filetype validation*/
        
         
        document.setDocUser(user.getUsername());
        document.setDocName(multipartFile.getOriginalFilename());
        document.setDocDesc(fileUpload.getDescription());
        document.setDocType(multipartFile.getContentType());
        document.setDocContent(multipartFile.getBytes());
        document.setDocAnnotationFlag((Integer) 0);
        
        try{
        	userDocumentService.saveDocument(document);
            int docId = userDocumentService.findBiggestDocId();
            saveDictionary(fileUpload, docId);
        } catch(Exception e) {
        	System.out.println("Save document error with document name " + multipartFile.getOriginalFilename());
        	e.printStackTrace();
        }
    }
	
	/* Method to handle pdf content preprocessing and insert the result to database 
	 * including posting list */
	private void saveDictionary(FileUpload fileUpload, int docId) 
			throws IOException{
		
		IrPreprocessing  irp = new IrPreprocessing();
		JavaArray jua = new JavaArray(); 
		MultipartFile multipartFile = fileUpload.getFile();
			
		/* Convert Multipartfile to File */
		File pdfFile = new File(System.getProperty("java.io.tmpdir") +
				System.getProperty("file.separator") + 
				multipartFile.getOriginalFilename());
		multipartFile.transferTo(pdfFile);
		
		/* Stemming + unique + frequency + sort ascending */
		String[][] tokenPosting = irp.IrPrep(pdfFile);
		
		/* Get token only */
		String[] token = jua.getBySecondIndex(tokenPosting, 0);
			
		/* Insert list of token into dictionary (DB) */
		documentDictionaryService.saveDictionaryList(token);   
		
		/* Insert posting list to DB */
		for (int i = 0; i < tokenPosting.length; i++) {
			DocumentPosting posting = new DocumentPosting();
			posting.setDictionaryId(documentDictionaryService.findIdByToken(tokenPosting[i][0]));
			posting.setDocumentId(docId);
			posting.setFrequency(Integer.parseInt(tokenPosting[i][1]));
			documentPostingService.save(posting);
		}
    }
	
	/* This method will handle download document process. */
	@RequestMapping(value = { "/download-document-{username}-{documentId}" }, method = RequestMethod.GET)
    public ModelAndView downloadDocument(@PathVariable String username, @PathVariable int documentId, 
    		HttpServletResponse response) throws IOException {
		ModelAndView model = new ModelAndView("redirect:/add-document-" + username);
        UserDocument document = userDocumentService.findByDocId(documentId);
        response.setContentType(document.getDocType());
        response.setContentLength(document.getDocContent().length);
        response.setHeader("Content-Disposition","attachment; filename=\"" + document.getDocName() +"\"");
  
        FileCopyUtils.copy(document.getDocContent(), response.getOutputStream());
  
	    return model;
    }
	
	/* This method will handle delete document process. */
	@RequestMapping(value = { "/delete-document-{username}-{documentId}" }, method = RequestMethod.GET)
    public ModelAndView deleteDocument(@PathVariable String username, @PathVariable int documentId) {
        userDocumentService.deleteByDocId(documentId);
        documentDictionaryService.decreaseListOfDocFrequency(documentPostingService.findAllDcId(documentId));
        documentPostingService.deleteByDocId(documentId);
        
        ModelAndView model = new ModelAndView("redirect:/add-document-" + username);
        return model;
    }
	
	/* This method will handle add annotation document process. */
	@RequestMapping(value = { "/add-annotation-document-{username}-{documentId}" }, method = RequestMethod.GET)
    public ModelAndView addAnnotationDocument(@PathVariable String username, @PathVariable int documentId) throws IOException {
		/* Calculate execution time */
		long annoStartTime = System.currentTimeMillis();
		
		this.annoProcess(documentId);
		
		long annoEndTime = System.currentTimeMillis();
		
		System.out.println("Annotation time: " + (annoStartTime - annoEndTime));
        ModelAndView model = new ModelAndView("redirect:/add-document-" + username);
        return model;
    }
	
	public void annoProcess(int docId) throws IOException{
		PDFBoxManager pdfBox = new PDFBoxManager();
		
		UserDocument ud = userDocumentService.findByDocId(docId);
		
		File file = new File(System.getProperty("catalina.home") + "\\uploads\\", ud.getDocName());
		FileOutputStream fileOuputStream = new FileOutputStream(file); 
		fileOuputStream.write(ud.getDocContent());
		fileOuputStream.close();
		
//		System.out.println(System.getProperty("catalina.home") + "\\uploads\\" + file.getName());
		Map<String,String> mapAnno= anno.annoProcessFullText(System.getProperty("catalina.home") + "\\uploads\\" + file.getName());
		
		try {
			pdfBox.writeCustomProperties(file, mapAnno);			
		} catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println("Annotation process finished (" + file.getName() + ")");
		
	}
	
	/* This method will handle migration process from document to database. */
	@RequestMapping(value = { "/migrate-annotation-document-{username}-{documentId}" }, method = RequestMethod.GET)
    public ModelAndView migrateAnnotationDocument(@PathVariable String username, @PathVariable int documentId) throws IOException {
		AnnotationMapping am = new AnnotationMapping();
		UserDocument ud = new UserDocument();
		
		if(annotationMappingService.findAllAnnoByDocId(documentId).size() > 0) 
			annotationMappingService.deleteByDocId(documentId);
		ud = userDocumentService.findByDocId(documentId);	
		
		PDFBoxManager pbm = new PDFBoxManager();
		DocumentDictionary dd = new DocumentDictionary();
		
		File file = new File(System.getProperty("catalina.home") + "\\uploads\\", ud.getDocName());
		pbm.readDocumentInformation(file, "annotation");
		String[] anno = pbm.retrieveDocumentInformation(file, "annotation").split(";");
		for(String sa: new HashSet<String>(Arrays.asList(anno))){
			documentDictionaryService.saveDictionary(documentId, sa);
			am.setAnnoCatId(1);
			am.setDocId(documentId);
			am.setDocDicId(documentDictionaryService.findIdByToken(sa));
			am.setAnnoDesc("");
			annotationMappingService.save(am);
			String wordnetAnno = pbm.retrieveDocumentInformation(file, "wordnet:"+sa);
			if(wordnetAnno!=null && !wordnetAnno.isEmpty()) {
				am.setAnnoCatId(3);
				am.setAnnoDesc(wordnetAnno);
				annotationMappingService.save(am);
			}
			String dbpediaAnno = pbm.retrieveDocumentInformation(file, "dbpedia:"+sa);
			if(dbpediaAnno!=null && !dbpediaAnno.isEmpty()) {
				am.setAnnoCatId(2);
				am.setAnnoDesc(dbpediaAnno);
				annotationMappingService.save(am);
			}
		}
		
        ModelAndView model = new ModelAndView("redirect:/add-document-" + username);
        return model;
    }
	
 
}