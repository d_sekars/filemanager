package rsc.filemanager.model;

public class SearchIR {

	int documentId;
	String documentName;
	byte[] documentContent;
	double documentScore;
	
	public int getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}
		
	public String getDocumentName() {
		return documentName;
	}
	
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	
	public byte[] getDocumentContent() {
		return documentContent;
	}
	
	public void setDocumentContent(byte[] documentContent) {
		this.documentContent = documentContent;
	}
	
	public double getDocumentScore() {
		return documentScore;
	}
	
	public void setDocumentScore(double documentScore) {
		this.documentScore = documentScore;
	}
	
	
	
}
