package rsc.filemanager.model;

/**
 * @author Diah S R
 */

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_granted_authorities")
@SuppressWarnings("serial")
public class UserGrantedAuth implements Serializable {

	@Id
    @Column(name="granted_user", nullable=false)
    private String grantedUser;
     
	@Id
    @Column(name="granted_auth", nullable=false)
    private String grantedAuth;
	
	public String getGrantedUser() {
        return grantedUser;
    }
 
    public void setGrantedUser(String grantedUser) {
        this.grantedUser = grantedUser;
    }
    
    public String getGrantedAuth() {
        return grantedAuth;
    }
 
    public void setGrantedAuth(String grantedAuth) {
        this.grantedAuth = grantedAuth;
    }
}
