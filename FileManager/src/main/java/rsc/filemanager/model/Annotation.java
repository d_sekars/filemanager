package rsc.filemanager.model;

/**
 * @author Diah S R
 */

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="annotation")
@SuppressWarnings("serial")
public class Annotation implements Serializable {
	
	@Id
	@Column(name="annotation_id", nullable=false)
    private Integer annoId; 
     
	@Column(name="annotation_category_id", nullable=false)
    private Integer annoCatId; 
	
	@Column(name="document_id", nullable=false)
    private Integer docId;
	
    @Column(name="document_dictionary_id", nullable=false)
    private Integer docDicId;
    
    @Column(name="annotation_description", length = 255)
    private String annoDesc;

	public Integer getAnnoId() {
		return annoId;
	}

	public void setAnnoId(Integer annoId) {
		this.annoId = annoId;
	}

	public Integer getAnnoCatId() {
		return annoCatId;
	}

	public void setAnnoCatId(Integer annoCatId) {
		this.annoCatId = annoCatId;
	}
	
	public Integer getDocId() {
		return docId;
	}

	public void setDocId(Integer docId) {
		this.docId = docId;
	}

	public Integer getDocDicId() {
		return docDicId;
	}

	public void setDocDicId(Integer docDicId) {
		this.docDicId = docDicId;
	}

	public String getAnnoDesc() {
		return annoDesc;
	}

	public void setAnnoDesc(String annoDesc) {
		this.annoDesc = annoDesc;
	}
    
    
}
