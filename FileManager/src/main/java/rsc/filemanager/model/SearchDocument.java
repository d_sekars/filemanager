package rsc.filemanager.model;

public class SearchDocument {
	
	String searchQuery;
	String annoReference;
	int searchResultNum;
	
	public String getAnnoReference() {
		return annoReference;
	}

	public void setAnnoReference(String annoReference) {
		this.annoReference = annoReference;
	}

	public String getSearchQuery() {
		return searchQuery;
	}

	public void setSearchQuery(String searchQuery) {
		this.searchQuery = searchQuery;
	}

	public int getSearchResultNum() {
		return searchResultNum;
	}

	public void setSearchResultNum(int searchResultNum) {
		this.searchResultNum = searchResultNum;
	}
	
}
