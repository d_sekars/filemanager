package rsc.filemanager.model;

/**
 * @author Diah S R
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="document_dictionary")
public class DocumentDictionary {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "dc_id", nullable = false)
    private Integer dictionaryId;
	
	@Column(name = "dc_token", length = 100)
    private String dictionaryToken;
	
	@Column(name = "dc_frequency")
	private Integer docFrequency;
	
	public int getDictionaryId() {
        return dictionaryId;
    }
 
    public void setDictionaryId(int dictionaryId) {
        this.dictionaryId = dictionaryId;
    }
 
    public String getDictionaryToken() {
        return dictionaryToken;
    }
 
    public void setDictionaryToken(String dictionaryToken) {
        this.dictionaryToken = dictionaryToken;
    }
    
    public int getDocFrequency() {
        return docFrequency;
    }
 
    public void setDocFrequency(int docFrequency) {
        this.docFrequency = docFrequency;
    }

    @Override
    public String toString() {
        return "DocumentDictionary [ dictionaryId =" + dictionaryId + ", dictionaryToken =" + dictionaryToken +
        		", docFrequency =" + docFrequency + " ]";
    }
}
