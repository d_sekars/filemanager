package rsc.filemanager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Diah S R
 *
 */

@Entity
@Table(name="domain")
public class WordNetDomain {
	
	@Id
    @Column(name="domain_offset", unique=true, nullable=false)
    private Integer domainOffset;
	
	@Column(name="domain_pos", nullable=false)
    private String domainPos;
	
	@Column(name="domain_membership", nullable=false)
    private Integer domainMembership;
	
	public Integer getDomainOffset() {
        return domainOffset;
    }
 
    public void setDomainOffset(Integer domainOffset) {
        this.domainOffset = domainOffset;
    }
    
    public String getDomainPos() {
        return domainPos;
    }
 
    public void setDomainPos(String domainPos) {
        this.domainPos = domainPos;
    }
 
    public Integer getDomainMembership() {
        return domainMembership;
    }
 
    public void setDomainMembership(Integer domainMembership) {
        this.domainMembership = domainMembership;
    }
    
    @Override
    public String toString() {
        return "Domain [domainOffset=" + domainOffset + ", domainPos=" + domainPos + 
        		", domainMembership=" + domainMembership + "]";
    }

}
