package rsc.filemanager.model;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

//import java.util.HashSet;
//import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
 
@Entity
@Table(name="user_documents")
public class UserDocument {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="doc_id", nullable=false)
    private Integer docId; 
	
	@Column(name = "doc_user", length = 45, nullable = false)
	private String docUser;
     
    @Column(name="doc_name", length=200, nullable=false)
    private String docName;
     
    @Column(name="doc_desc", length=255)
    private String docDesc;
     
    @Column(name="doc_type", length=100, nullable=false)
    private String docType;
     
    @Lob @Basic(fetch = FetchType.LAZY)
    @Column(name="doc_content", nullable=false)
    private byte[] docContent;
    
    @Column(name="doc_annotation_flag", length=2)
    private Integer docAnnotationFlag;

	public Integer getDocId() {
        return docId;
    }
 
    public void setDocId(Integer docId) {
        this.docId = docId;
    }
    
	  public String getDocUser() {
	  return docUser;
	}
	
	public void setDocUser(String docUser) {
	  this.docUser = docUser;
	}
 
    public String getDocName() {
        return docName;
    }
 
    public void setDocName(String docName) {
        this.docName = docName;
    }
 
    public String getDocDesc() {
        return docDesc;
    }
 
    public void setDocDesc(String docDesc) {
        this.docDesc = docDesc;
    }
 
    public String getDocType() {
        return docType;
    }
 
    public void setDocType(String docType) {
        this.docType = docType;
    }
 
    public byte[] getDocContent() {
        return docContent;
    }
 
    public void setDocContent(byte[] docContent) {
        this.docContent = docContent;
    }
    
    public Integer getDocAnnotationFlag() {
		return docAnnotationFlag;
	}

	public void setDocAnnotationFlag(Integer docAnnotationFlag) {
		this.docAnnotationFlag = docAnnotationFlag;
	}
 
    @Override
    public String toString() {
        return "UserDocument [id=" + docId + ", name=" + docName + ", description="
                + docDesc + ", type=" + docType + ", annotation flag=" + docAnnotationFlag + "]";
    }
	
}
