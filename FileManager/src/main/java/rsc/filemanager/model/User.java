package rsc.filemanager.model;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
//import javax.persistence.OneToMany;
import javax.persistence.Table;
 
@Entity
@Table(name="users")
public class User {
 
	@Id
    @Column(name="username", unique=true, nullable=false)
    private String username;
     
    @Column(name="password", nullable=false)
    private String password;
 
    @Column(name="activation_status", nullable=false)
    private String status;
 
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "user_granted_authorities", 
             joinColumns = { @JoinColumn(name = "granted_user") }, 
             inverseJoinColumns = { @JoinColumn(name = "granted_auth") })
    private Set<UserAuthority> userAuthority = new HashSet<UserAuthority>(0);
    
//    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//    private Set<UserDocument> userDocuments = new HashSet<UserDocument>();
 
    public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
 
    public String getStatus() {
        return status;
    }
 
    public void setStatus(String status) {
        this.status = status;
    }
 
    public Set<UserAuthority> getUserAuthorities() {
        return userAuthority;
    }
 
    public void setUserAuthorities(Set<UserAuthority> userAuthority) {
        this.userAuthority = userAuthority;
    }
    
//    public Set<UserDocument> getUserDocuments() {
//        return userDocuments;
//    }
// 
//    public void setUserDocuments(Set<UserDocument> userDocuments) {
//        this.userDocuments = userDocuments;
//    }
 
    @Override
    public String toString() {
        return "User [username=" + username + ", password=" + password + ", status=" + status + "]";
    }
 
     
}