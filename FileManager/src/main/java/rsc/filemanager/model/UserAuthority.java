package rsc.filemanager.model;

/**
 * Adopt code from:
 * - http://websystique.com/spring-security/spring-security-4-hibernate-annotation-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name="user_authorities")
public class UserAuthority {
	
	@Id
	@Column(name="user_auth", nullable=false)
    private String userAuth;
    
    @Column(name="description", nullable=false)
    private String description;
     
    public String getUserAuthority() {
        return userAuth;
    }
 
    public void setUserAuthority(String userAuth) {
        this.userAuth = userAuth;
    }
 
    public String getDescription() {
        return description;
    }
 
    public void setDescription(String description) {
        this.description = description;
    }
 
    @Override
    public String toString() {
        return "UserAuthorities [userAuth=" + userAuth + ",  description=" + description  + "]";
    }
}
