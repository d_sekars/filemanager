package rsc.filemanager.model;

/**
 * Adopt code from:
 * - http://www.journaldev.com/2573/spring-mvc-file-upload-example-single-multiple-files
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public class FileUpload {
	
	private MultipartFile file;
	private List<MultipartFile> files;
	String description;
	
	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public List<MultipartFile> getFiles() {
		return files;
	}

	public void setFiles(List<MultipartFile> files) {
		this.files = files;
	}
	
	public String getDescription() {
        return description;
    }
 
    public void setDescription(String description) {
        this.description = description;
    }
	
}
