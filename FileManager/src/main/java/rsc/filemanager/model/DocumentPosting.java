package rsc.filemanager.model;

/**
 * @author Diah S R
 */

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="document_posting")
@SuppressWarnings("serial")
public class DocumentPosting implements Serializable {
	
	@Id
	@Column(name="document_id", nullable=false)
    private Integer documentId; 
     
	@Id
	@Column(name="dictionary_id", nullable=false)
    private Integer dictionaryId; 
	
    @Column(name="frequency", nullable=false)
    private Integer frequency;
    
    public Integer getDocumentId() {
        return documentId;
    }
 
    public void setDocumentId(int documentId) {
        this.documentId = documentId;
    }
    
    public Integer getDictionaryId() {
        return dictionaryId;
    }
 
    public void setDictionaryId(int dictionaryId) {
        this.dictionaryId = dictionaryId;
    }
    
    public Integer getFrequency() {
        return frequency;
    }
 
    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }
    
//    public UserDocument getUserDocument() {
//        return userDocument;
//    }
// 
//    public void setUserDocument(UserDocument userDocument) {
//        this.userDocument = userDocument;
//    }
    
//    public DocumentDictionary getDocumentDictionary() {
//        return documentDictionary;
//    }
// 
//    public void setDocumentDictionary(DocumentDictionary documentDictionary) {
//        this.documentDictionary = documentDictionary;
//    }
}
