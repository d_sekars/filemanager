package rsc.filemanager.common;

/**
 * Adopt code from:
 * - https://radixcode.com/pdfbox-example-code-how-to-extract-text-from-pdf-file-with-java/
 */

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.font.PDSimpleFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.text.PDFTextStripper;

import net.sf.extjwnl.data.Synset;

public class PDFBoxManager {

	private PDFParser parser;
	private PDFTextStripper pdfStripper;
	private PDDocument pdDoc ;
	private COSDocument cosDoc ;
	private String Text ;
	private String filePath;
	private String pdfAuthor;
    private String pdfTitle;
    private String pdfSubject;
    private String pdfCreator;
    private Calendar pdfCreationDate;
    private Calendar pdfModificationDate; 
    private String pdfKeywords;
    private File file;

	public PDFBoxManager() {
		
	}
	
	public String getFilePath(){
		return filePath;
	}
	   
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
    
    public String getPdfAuthor() {
		return pdfAuthor;
	}

	public void setPdfAuthor(String pdfAuthor) {
		this.pdfAuthor = pdfAuthor;
	}

	public String getPdfTitle() {
		return pdfTitle;
	}

	public void setPdfTitle(String pdfTitle) {
		this.pdfTitle = pdfTitle;
	}

	public String getPdfSubject() {
		return pdfSubject;
	}

	public void setPdfSubject(String pdfSubject) {
		this.pdfSubject = pdfSubject;
	}

	public String getPdfCreator() {
		return pdfCreator;
	}

	public void setPdfCreator(String pdfCreator) {
		this.pdfCreator = pdfCreator;
	}

	public Calendar getPdfCreationDate() {
		return pdfCreationDate;
	}

	public void setPdfCreationDate(Calendar pdfCreationDate) {
		this.pdfCreationDate = pdfCreationDate;
	}

	public Calendar getPdfModificationDate() {
		return pdfModificationDate;
	}

	public void setPdfModificationDate(Calendar pdfModificationDate) {
		this.pdfModificationDate = pdfModificationDate;
	}

	public String getPdfKeywords() {
		return pdfKeywords;
	}

	public void setPdfKeywords(String pdfKeywords) {
		this.pdfKeywords = pdfKeywords;
	}
	
	public String ToText() throws IOException {
		this.pdfStripper = null;
		this.pdDoc = null;
		this.cosDoc = null;
	       
		file = new File(filePath);
		parser = new PDFParser(new RandomAccessFile(file,"r")); // update for PDFBox V 2.0
	       
		parser.parse();
		cosDoc = parser.getDocument();
		pdfStripper = new PDFTextStripper();
		pdDoc = new PDDocument(cosDoc);
		pdDoc.getNumberOfPages();
		pdfStripper.setStartPage(1);
		pdfStripper.setEndPage(pdDoc.getNumberOfPages());
	       
		Text = pdfStripper.getText(pdDoc);
		pdDoc.close();
		return Text;
	}
	
	public String ToText(int from, int to) throws IOException {
		this.pdfStripper = null;
		this.pdDoc = null;
		this.cosDoc = null;
	       
		file = new File(filePath);
		parser = new PDFParser(new RandomAccessFile(file,"r")); // update for PDFBox V 2.0
	       
		parser.parse();
		cosDoc = parser.getDocument();
		pdfStripper = new PDFTextStripper();
		pdDoc = new PDDocument(cosDoc);
		pdDoc.getNumberOfPages();
		pdfStripper.setStartPage(from);
		pdfStripper.setEndPage(to);
	       
		Text = pdfStripper.getText(pdDoc);
		pdDoc.close();
		return Text;
	}
	
	public String readTextByFile(File pdfFile) throws IOException{
		pdDoc = PDDocument.load(pdfFile);
	    return new PDFTextStripper().getText(pdDoc);
	}
	
	public String readTextByFile(File pdfFile, int from, int to) throws IOException{
		pdDoc = PDDocument.load(pdfFile);
		pdfStripper.setStartPage(from);
		pdfStripper.setEndPage(to);
	       
		Text = pdfStripper.getText(pdDoc);
		pdDoc.close();
		return Text;
	}
	
	public byte[] addNewPageWithText(File file, String text) throws IOException{
		//Loading an existing document
	    PDDocument document = PDDocument.load(file);
	    int pageNumber = document.getNumberOfPages();
	    String fileName = file.getName();
	       
	    //Retrieving the pages of the document 
	    PDPage blankPage = new PDPage();
	    blankPage.setMediaBox(PDRectangle.A4);
	    document.addPage(blankPage);
	    PDPage page = document.getPage(pageNumber);
	    PDPageContentStream contentStream = new PDPageContentStream(document, page);
	      
	    //Begin the Content stream 
	    contentStream.beginText(); 
	       
	    //Setting the font to the Content stream  
	    PDSimpleFont font = PDType1Font.TIMES_ROMAN;
	    int fontSize = 12;
	    contentStream.setFont(font, fontSize);

	    //Setting the leading
	    contentStream.setLeading(14.5f);

	    //Setting the position for the line 
	    contentStream.newLineAtOffset(100, 700);
	    
	    //Split text into array
	    String[] txt = text.split("\\s+");
	    
	    //Manage new line(s)
	    float textWidth = 0;
	    float lineCapacity = PDRectangle.A4.getWidth() - 250;
	    for(String t:txt){
	    	float wordWidth = (font.getStringWidth(t) ) / 1000 * fontSize;
	    	
	    	textWidth = textWidth + wordWidth; 
	    	
	    	if(textWidth >= lineCapacity){
	    		contentStream.newLine();
	    		textWidth = 0;
	    	}
	    	
	    	//Adding text in the form of string 
		    contentStream.showText(t);
		    if(textWidth <= lineCapacity){
	    		contentStream.showText(" ");
	    	}
	    }

	    //Ending the content stream
	    contentStream.endText();

	    System.out.println("Content added");

	    //Closing the content stream
	    contentStream.close();
	      
	    PDStream pdStream = new PDStream(document);
	    byte[] content = pdStream.toByteArray();

	    //Saving the document
	    // Creating the directory to store file
        String rootPath = System.getProperty("catalina.home");
        File dir = new File(rootPath + File.separator + "uploads");
		if (!dir.exists())
			dir.mkdirs();
			
		// Store file on server
		document.save(new File(dir.getAbsolutePath() + File.separator + fileName.substring(0, fileName.length()-4) + "-ano.pdf"));

	    //Closing the document
	    document.close();
	      
	    return content;
	}
	
	public void readDocumentInformation() throws IOException{		
		//Loading an existing document
	    PDDocument document = PDDocument.load(new File(this.getFilePath()));

	    //Getting the PDDocumentInformation object
	    PDDocumentInformation pdd = document.getDocumentInformation();
	    
	    //Retrieving the info of a PDF document
	    System.out.println("Author of the document is :"+ pdd.getAuthor());
	    System.out.println("Title of the document is :"+ pdd.getTitle());
	    System.out.println("Subject of the document is :"+ pdd.getSubject());

	    System.out.println("Creator of the document is :"+ pdd.getCreator());
	    System.out.println("Creation date of the document is :"+ pdd.getCreationDate());
	    System.out.println("Modification date of the document is :"+ pdd.getModificationDate()); 
	    System.out.println("Keywords of the document are :"+ pdd.getKeywords()); 
	    
	    this.setPdfAuthor(pdd.getAuthor());
	    this.setPdfTitle(pdd.getTitle());
	    this.setPdfSubject(pdd.getSubject());
	    this.setPdfCreator(pdd.getCreator());
	    this.setPdfCreationDate(pdd.getCreationDate());
	    this.setPdfModificationDate(pdd.getModificationDate());
	    this.setPdfKeywords(pdd.getKeywords());
	       
	    //Closing the document 
	    document.close();
	}
	
	public void readDocumentInformation(File file) throws IOException{
		//Loading an existing document
	    PDDocument document = PDDocument.load(file);

	    //Getting the PDDocumentInformation object
	    PDDocumentInformation pdd = document.getDocumentInformation();
	    
	    //Retrieving the info of a PDF document
	    System.out.println("Author of the document is :"+ pdd.getAuthor());
	    System.out.println("Title of the document is :"+ pdd.getTitle());
	    System.out.println("Subject of the document is :"+ pdd.getSubject());

	    System.out.println("Creator of the document is :"+ pdd.getCreator());
	    System.out.println("Creation date of the document is :"+ pdd.getCreationDate());
	    System.out.println("Modification date of the document is :"+ pdd.getModificationDate()); 
	    System.out.println("Keywords of the document are :"+ pdd.getKeywords()); 
	    
	    this.setPdfAuthor(pdd.getAuthor());
	    this.setPdfTitle(pdd.getTitle());
	    this.setPdfSubject(pdd.getSubject());
	    this.setPdfCreator(pdd.getCreator());
	    this.setPdfCreationDate(pdd.getCreationDate());
	    this.setPdfModificationDate(pdd.getModificationDate());
	    this.setPdfKeywords(pdd.getKeywords());
	       
	    //Closing the document 
	    document.close();
	}
	
	public void readDocumentInformation(File file, String property){
	    try {
	    	//Loading an existing document
		    PDDocument document = PDDocument.load(file);

		    //Getting the PDDocumentInformation object
		    PDDocumentInformation pdd = document.getDocumentInformation();
		    
		    //Retrieving the info of a PDF document
		    String value = pdd.getCustomMetadataValue(property);
		    if(value!=null && !value.isEmpty())
		    	System.out.println("Custom property " + property + " with value: " + value);
		       
		    //Closing the document 
			document.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String retrieveDocumentInformation(File file, String property) throws IOException{
		//Loading an existing document
	    PDDocument document = PDDocument.load(file);

	    //Getting the PDDocumentInformation object
	    PDDocumentInformation pdd = document.getDocumentInformation();
	    
	    //Retrieving the info of a PDF document
	    String result = pdd.getCustomMetadataValue(property);
	       
	    //Closing the document 
	    document.close();
	    
	    return result;
	}
	
	public boolean findDocumentInformation(File file, String property) throws Exception{
		//Loading an existing document
	    PDDocument document = PDDocument.load(file);

	    //Getting the PDDocumentInformation object
	    PDDocumentInformation pdd = document.getDocumentInformation();
	    
	    //Retrieving the info of a PDF document
	    String value = pdd.getCustomMetadataValue(property);
	       
	    //Closing the document 
	    document.close();
	    
	    if(value!=null && !value.isEmpty()) return true;
	    
	    return false;
	}
	
	public void writeCustomProperties(File file, String propertiesName , String properties) throws IOException{
		this.readDocumentInformation(file);
		String fileName = file.getName();

		//Loading an existing document
	    PDDocument document = PDDocument.load(file);

	    //Getting the PDDocumentInformation object
	    PDDocumentInformation pdd = document.getDocumentInformation();
	    
//	    pdd.setSubject("this is subject");
//	    pdd.setKeywords("keywords:description");
	    pdd.setCustomMetadataValue(propertiesName, properties);
	    
	    //Saving the document
	    // Creating the directory to store file
        String rootPath = System.getProperty("catalina.home");
        File dir = new File(rootPath + File.separator + "uploads");
		if (!dir.exists())
			dir.mkdirs();
	    
	    // Store file on server
	 	document.save(new File(dir.getAbsolutePath() + File.separator + fileName.substring(0, fileName.length()-4) + "-ano.pdf"));

	 	//Closing the document
	 	document.close();
	}
	
	public void writeCustomProperties(File file, Map<String, String> map) throws IOException{
		this.readDocumentInformation(file);
		String fileName = file.getName();

		//Loading an existing document
	    PDDocument document = PDDocument.load(file);

	    //Getting the PDDocumentInformation object
	    PDDocumentInformation pdd = document.getDocumentInformation();
	    
	    for (Map.Entry<String, String> m : map.entrySet()) {
	    	pdd.setCustomMetadataValue(m.getKey(), m.getValue());
	    }
	    
	    //Saving the document
	    // Creating the directory to store file
//        String rootPath = System.getProperty("catalina.home");
	    String rootPath = file.getParent();
//        File dir = new File(rootPath + File.separator + "uploads");
        File dir = new File(rootPath);
		if (!dir.exists()) dir.mkdirs();
	    
	    // Store file on server
//	 	document.save(new File(dir.getAbsolutePath() + File.separator + fileName.substring(0, fileName.length()-4) + "-ano.pdf"));
	 	document.save(new File(dir.getAbsolutePath() + File.separator + fileName));

	 	//Closing the document
	 	document.close();
	}
	
	/* List of files inside a folder */
	public List<File> listFilesForFolder(File folder) {
		List<File> listFile = new ArrayList<File>();
	    for (File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	            listFilesForFolder(fileEntry);
	        } else {
	            System.out.println(fileEntry.getName());
	            listFile.add(fileEntry);
	        }
	    }
	    return listFile;
	}
}