package rsc.filemanager.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import net.sf.extjwnl.data.Synset;

/**
 * 
 * @author Diah S R
 *
 */

public class JenaDBpedia {
	
	private static final String sparqlService = "http://dbpedia.org/sparql";
	private static IrPreprocessing  irp = new IrPreprocessing();
	
	/* Method to get person label based on input */
	public void getPersonLabelByInput(String input) {
		String query = "PREFIX pr:<http://xmlns.com/foaf/0.1/> " +
						"PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
						"SELECT DISTINCT ?s ?label " +
						"WHERE { " +
							"?s rdfs:label ?label . " +
							"?s a pr:Person . " +
							"FILTER (lang(?label) = 'en') . " +
							"?label <bif:contains> \"\'" + input + "\'\" }";
		this.runQuery(query);
	}
	
	/* Method to get label based on input */
	public void getLabelByInput(String input) {
		String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
						"SELECT DISTINCT ?s ?label " +
						"WHERE { " +
							"?s rdfs:label ?label . " +
							"FILTER (lang(?label) = 'en') . " +
							"?label <bif:contains> \"\'" + input + "\'\" } " +
						"LIMIT 10";
		this.runQuery(query);
	}	
	
	/*  */
	public String searchByMultipleInputQuery(String input) {
		String[] in = irp.tokenization(input);
		String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
				"PREFIX o: <http://dbpedia.org/ontology/> " +
				"PREFIX p: <http://dbpedia.org/property/> " +
				"SELECT DISTINCT ?wordName ?word ?type ?description " +
				"WHERE { ";
				
		for(int i = 0; i < in.length; i++){
			if(i > 0) query = query + "UNION";
			query = query +
					"{ " +
					"?word o:wikiPageDisambiguates ?type . " +
					"?type o:abstract ?description ." +
					"?word rdfs:label ?wordName . " +
					"?wordName <bif:contains> \"" + in[i] + "\" . " +
					"FILTER (lang(?wordName) = \"en\") " +
					"FILTER (lang(?description) = \"en\") " +
					"} ";
			
		}
		query = query + "}";
		return query;
	}
	
	public String searchBySingleInputQueryWithWikiPageDisambiguates(String input) {
		String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
				"PREFIX o: <http://dbpedia.org/ontology/> " +
				"PREFIX p: <http://dbpedia.org/property/> " +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/>" +
				"SELECT DISTINCT ?wordName ?word ?type ?typeTopic ?description " +
				"WHERE { " +
				"?word o:wikiPageDisambiguates ?type . " +
				"?type o:abstract ?description . " +
				"?type foaf:isPrimaryTopicOf ?typeTopic . " +
				"?word rdfs:label ?wordName . " +
				"?wordName <bif:contains> \"" + input + "\" . " +
				"FILTER (lang(?wordName) = \"en\") " +
				"FILTER (lang(?description) = \"en\") " +
				"} ";
		return query;
	}
	
	public String searchExactResultBySingleInputQueryWithWikiPageDisambiguates(String input) {
		String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
				"PREFIX o: <http://dbpedia.org/ontology/> " +
				"PREFIX p: <http://dbpedia.org/property/> " +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/>" +
				"SELECT DISTINCT ?wordName ?word ?type ?typeTopic ?description " +
				"WHERE { " +
				"?word o:wikiPageDisambiguates ?type ; rdfs:label ?wordName ; ?word rdfs:label \"" + input + "\"@en . " +
				"?type o:abstract ?description ; ?type foaf:isPrimaryTopicOf ?typeTopic . " +
				"FILTER (lang(?wordName) = \"en\") " +
				"FILTER (lang(?description) = \"en\") " +
				"} ";
		return query;
	}
	
	public String searchSingleInputQueryWithWikiPageDisambiguates(String input) {
		return "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
				"PREFIX o: <http://dbpedia.org/ontology/> " +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/> " +
				"SELECT DISTINCT ?wordName ?word ?typeTopic ?description " +
				"WHERE { " +
					"?keyword o:wikiPageDisambiguates ?word ; rdfs:label \"" + input + "\"@en . " +
					"?word rdfs:label ?wordName ; o:abstract ?description ; foaf:isPrimaryTopicOf ?typeTopic . " +
					"FILTER (lang(?wordName) = \"en\") . " +
					"FILTER (lang(?description) = \"en\") " +
				"}";
	}
	
	public String getDisambiguationResultAndCategory(String resource) {
		return "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/> " +
				"PREFIX dct:<http://purl.org/dc/terms/> " +
				"SELECT DISTINCT ?wordName ?typeTopic " +
				"WHERE { " +
					"?word foaf:isPrimaryTopicOf ?typeTopic ; rdfs:label ?wordName . " + 
					"{ ?word rdfs:resource <" + resource + "> } " +
						"UNION " +
					"{ " + 
						"?cat rdfs:label ?wordName . " + 
						"?member dct:subject ?cat . " +
						"FILTER (?member = <" + resource + ">) " +
					"} . " +
					"FILTER (lang(?wordName) = \"en\") " + 
				"}";
	}
	
	public String searchBySingleInputQueryWithoutWikiPageDisambiguates(String input) {
		String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
				"PREFIX o: <http://dbpedia.org/ontology/> " +
				"PREFIX p: <http://dbpedia.org/property/> " +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/>" +
				"SELECT DISTINCT ?wordName ?word ?typeTopic ?description " +
				"WHERE { " +
				"?word o:abstract ?description . " +
				"?word foaf:isPrimaryTopicOf ?typeTopic . " +
				"?word rdfs:label ?wordName . " +
				"?wordName <bif:contains> \"" + input + "\" . " +
				"FILTER (lang(?wordName) = \"en\") " +
				"FILTER (lang(?description) = \"en\") " +
				"} ";
		return query;
	}
	
	public String searchExactResultBySingleInputQueryWithoutWikiPageDisambiguates(String input) {
		String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
				"PREFIX o: <http://dbpedia.org/ontology/> " +
				"PREFIX p: <http://dbpedia.org/property/> " +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/>" +
				"SELECT DISTINCT ?wordName ?word ?typeTopic ?description " +
				"WHERE { " +
				"?word o:abstract ?description ; foaf:isPrimaryTopicOf ?typeTopic ; rdfs:label ?wordName ; rdfs:label \"" + input + "\"@en . " +
				"FILTER (lang(?wordName) = \"en\") " +
				"FILTER (lang(?description) = \"en\") " +
				"} ";
		return query;
	}
	
	public String searchSingleInputQuery(String input) {
		return "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
				"PREFIX foaf: <http://xmlns.com/foaf/0.1/> " +
				"PREFIX dct:<http://purl.org/dc/terms/> " +
				"PREFIX o: <http://dbpedia.org/ontology/> " +
				"SELECT DISTINCT ?wordName ?typeTopic " +
				"WHERE { " + 
					"?word foaf:isPrimaryTopicOf ?typeTopic ; rdfs:label ?wordName . " +
					"{ ?word rdfs:label \"" + input + "\"@en ; o:abstract ?description } " +
						"UNION " +
					"{ ?cat rdfs:label ?wordName . ?member rdfs:label \"" + input + "\"@en ; dct:subject ?cat } . " +
					"FILTER (lang(?wordName) = \"en\") " +
				"}";
	}
	
	/*  */
	public void searchByInputQueryTest(String input){
		this.runQuery(this.searchByMultipleInputQuery(input));
	}
	
	/* Method to get query result */
	public void runQuery(String queryString) {
		Query query = QueryFactory.create(queryString);        
        QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlService, query);
        
        try {
        	ResultSet results = qe.execSelect();
        	
        	while(results.hasNext()){
        		QuerySolution solution = results.nextSolution();
        		System.out.println(solution);
            }
        } finally { qe.close(); }
	}
	
	/* Method to search result based on input */
	public void searchByInput(String input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		List<String> listResult = new ArrayList<String>();
		List<String> in = Arrays.asList(irp.tokenization(input));
		
		for(String i: in){
			int score = 0;
			List<String> tempResult = new ArrayList<String>();
			Query query = QueryFactory.create(this.searchBySingleInputQueryWithWikiPageDisambiguates(i));        
	        QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlService, query);
	        
	        try {
	        	ResultSet results = qe.execSelect();
	        	System.out.println(results.getRowNumber());
	        	
	        	for(; results.hasNext(); ){
	        		int tempScore = 0;
	        		QuerySolution qs = results.nextSolution();
	        		String[] tempDesc = irp.tokenization(qs.getLiteral("description").toString());
	        		String tempStr = qs.getLiteral("wordName").toString().substring(0, 
							qs.getLiteral("wordName").toString().length()-3);
	        		
	        		String[] tempArrStr = tempStr.split("\\s+");
	        		tempStr = "";
	        		for(int x=0; x < tempArrStr.length; x++){
	        			if(x == tempArrStr.length-1 && tempArrStr[x].equals("disambiguation)")) break;
	        			tempStr = tempStr + tempArrStr[x];
	        		}
	        		
	        		for(String td: tempDesc){
	        			if(in.contains(td)){
	        				tempScore++;
	        			}
	        		}
	        		
	        		if(score < tempScore){
	        			score = tempScore;
	        			tempResult.remove(tempResult);
	        			tempResult.add(tempStr);
	        		} else if(score == tempScore){
	        			tempResult.add(tempStr);
	        		}
	            }
	        } finally { qe.close(); }
	        
	        listResult.addAll(tempResult);
		}
		
		for(String rs: listResult){
			System.out.println(rs);
		}
        
        long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
	}
	
	/* Method to search result based on input */
	public Map<String, String> searchByInput(List<String> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		Map<String, String> mapDbp = this.definitionHeuristicDBpedia(input);
        
        long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		return mapDbp;
	}
	public Map<String, String> searchByInput(Map<String, Integer> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		Map<String, String> mapDbp = this.definitionHeuristicDBpedia(input);
        
        long endTime = System.currentTimeMillis();
		System.out.println("DBpedia time consumed: " + (endTime - startTime) + " milliseconds.");
		
		return mapDbp;
	}
	
	/* Method to search result based on input */
	public Map<String, String> defHeuristicDBpedia(Map<String, Integer> input){
		Map<String, String> mapResult = new HashMap<String, String>();
		
		for(Map.Entry<String, Integer> m : input.entrySet()) {
			int score = 0;
			Map<String, String> tempMap = new HashMap<String, String>();
			Query query = QueryFactory.create(this.searchBySingleInputQueryWithWikiPageDisambiguates(m.getKey()));        
	        QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlService, query);
	        System.out.println(m.getKey());
	        
	        try {
	        	ResultSet results = qe.execSelect();
	        	
	        	for(; results.hasNext(); ){
	        		int tempScore = 0;
	        		QuerySolution qs = results.nextSolution();
	        		String uri = qs.getResource("?typeTopic").getURI().toString();
	        		String desc = qs.getLiteral("description").toString();
	        		String[] tempDesc = irp.tokenization(desc);
	        		String tempStr = qs.getLiteral("wordName").toString().substring(0, 
							qs.getLiteral("wordName").toString().length()-3);
	        		
	        		String[] tempArrStr = tempStr.split("\\s+");
	        		tempStr = "";
	        		for(int x=0; x < tempArrStr.length; x++){
	        			if(x == tempArrStr.length-1 && tempArrStr[x].equals("(disambiguation)")) break;
	        			else if(x == 0) tempStr = tempStr + tempArrStr[x];
	        			else tempStr = tempStr + " " + tempArrStr[x];
	        		}
	        		
	        		for(String td: tempDesc){
	        			if(input.containsValue(td)){
	        				tempScore += input.get(td);
	        			}
	        		}
	        		
	        		if(score < tempScore){
	        			score = tempScore;
	        			tempMap.remove(tempMap.keySet());
	        			tempMap.put(tempStr.toLowerCase(), uri);
	        		} else if(score == tempScore){
	        			tempMap.remove(tempMap.keySet());
	        		}
	            }
	        	mapResult.putAll(tempMap);
	        	qe.close();
	        } catch(Exception e) {
	        	System.out.println(e);
	        	qe.close(); 
	        	continue;
	        }
		}
		
		return mapResult;
	}
	
	public Map<String, String> definitionHeuristicDBpedia(List<String> input){
		Map<String, String> mapResult = new HashMap<String, String>();
		Set<String> setInput = new TreeSet<String>();
		
		for(String inp: input){
			String[] temp = irp.stopwordsRemoval(irp.tokenization(inp));
			for(String t: temp) setInput.add(t);
		}
		
		for(String in: setInput){
	        if(in.equals("middleware")); 
	        	System.out.println(in);
	        
	        try {
	        	Query query = QueryFactory.create(this.searchBySingleInputQueryWithWikiPageDisambiguates(in));        
		        QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlService, query);
	        	Map<String, String> tempMap = this.getResults(qe.execSelect(), setInput);
	        	 System.out.println(tempMap.size());
	        	if(!tempMap.isEmpty()) {
	        		mapResult.putAll(tempMap);
	        		qe.close();
	        	}
	        	else {
	        		query = QueryFactory.create(this.searchBySingleInputQueryWithoutWikiPageDisambiguates(in));        
			        qe = QueryExecutionFactory.sparqlService(sparqlService, query);
			        tempMap = this.getResults(qe.execSelect(), setInput);
			        System.out.println(tempMap.size());
			        if(!tempMap.isEmpty()) {
		        		mapResult.putAll(tempMap);
		        		qe.close();
		        	}
	        	}
	        } catch(Exception e) {
	        	e.printStackTrace();
	        	continue;
	        }
		}
		
		return mapResult;
	}
	public Map<String, String> definitionHeuristicDBpedia(Map<String, Integer> input){
		Map<String, String> mapResult = new HashMap<String, String>();
		
		for(Map.Entry<String, Integer> m : input.entrySet()) {
			        
	        try {
	        	try {
	        		Double.parseDouble(m.getKey());
	        		continue;
	        	} catch(Exception e) {
	        		
	        	}
	        	String[] tempString = m.getKey().split(", ");
	        	if(m.getValue() == 3) tempString = m.getKey().split(" ");
	        	Set<String> setTemp = new HashSet<String>(Arrays.asList(tempString));
	        	for(int i=0;i<tempString.length-1;i++){
	        		setTemp.add(tempString[i]+" "+tempString[i+1]);
	        	}
	        	setTemp.add(m.getKey());
	        	for(String s: setTemp) {
	        		String temp = s.replace("and ", "").replaceAll("[^A-Za-z0-9'\\/\\(\\)\\-\\s]", "").trim();
	        		if(s.length() > 1) temp = s.substring(0,1).toUpperCase() + s.substring(1);
	        		Query query = QueryFactory.create(this.searchSingleInputQuery(temp));        
			        QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlService, query);
		        	Map<String, String> tempMap = this.getDirectResult(qe.execSelect(), input);
		        	
		        	if(tempMap.isEmpty() && s.contains("-")) {
		        		query = QueryFactory.create(this.searchSingleInputQuery(temp.replace("-", "")));        
				        qe = QueryExecutionFactory.sparqlService(sparqlService, query);
				        tempMap = this.getDirectResult(qe.execSelect(), input);
		        	} else if(tempMap.isEmpty() && s.contains("/")) {
		        		String[] tmp = s.split("/");
		        		for(String t: tmp){
		        			t = t.substring(0,1).toUpperCase() + t.substring(1);
		        			query = QueryFactory.create(this.searchSingleInputQuery(t.trim()));        
					        qe = QueryExecutionFactory.sparqlService(sparqlService, query);
					        tempMap = this.getDirectResult(qe.execSelect(), input);
		        		}
		        	}
		        	
		        	if(!tempMap.isEmpty()) {
		        		mapResult.putAll(tempMap);
		        		qe.close();
		        	} else if (input.containsKey(s) && input.get(s)>1) { /* if word score > 1 */
		        		query = QueryFactory.create(this.searchSingleInputQueryWithWikiPageDisambiguates(temp));        
				        qe = QueryExecutionFactory.sparqlService(sparqlService, query);
				        tempMap = this.getDefinitionHeuristicResultAndCategory(temp, qe.execSelect(), input);
				        if(!tempMap.isEmpty()) {
			        		mapResult.putAll(tempMap);
			        		qe.close();
			        	}
		        	}
	        	}
	        } catch(Exception e) {
	        	e.printStackTrace();
	        	continue;
	        }
		}
		
		return mapResult;
	}
	
	private Map<String, String> getResults(ResultSet results, Set<String> input){  	
		int score = 0;
		Map<String, String> tempMap = new HashMap<String, String>();
    	for(; results.hasNext(); ){
    		int tempScore = 0;
    		QuerySolution qs = results.nextSolution();
    		String uri = qs.getResource("?typeTopic").getURI().toString();
    		String desc = qs.getLiteral("description").toString();
    		String[] tempDesc = irp.tokenization(desc);
    		String tempStr = qs.getLiteral("wordName").toString().substring(0, 
					qs.getLiteral("wordName").toString().length()-3);
    		
    		String[] tempArrStr = tempStr.split("\\s+");
    		tempStr = "";
    		for(int x=0; x < tempArrStr.length; x++){
    			if(x == tempArrStr.length-1 && tempArrStr[x].equals("(disambiguation)")) break;
    			else if(x == 0) tempStr = tempStr + tempArrStr[x];
    			else tempStr = tempStr + " " + tempArrStr[x];
    		}
    		
    		for(String td: tempDesc){
    			if(input.contains(td)){
    				tempScore++;
    			}
    		}
    		
    		if(score < tempScore){
    			score = tempScore;
    			tempMap.remove(tempMap.keySet());
    			tempMap.put(tempStr.toLowerCase(), uri);
    		} else if(score == tempScore){
    			tempMap.remove(tempMap.keySet());
    		}
        }
    	return tempMap;
	}
	
	private Map<String, String> getDefinitionHeuristicResultAndCategory(String word, ResultSet results, Map<String, Integer> input){  	
		int score = 0;
		Map<String, String> tempMap = new HashMap<String, String>();
    	for(; results.hasNext(); ){
    		int tempScore = 0;
    		QuerySolution qs = results.nextSolution();
    		String uri = qs.getResource("?typeTopic").getURI().toString();
    		String resource = qs.getResource("?word").getURI().toString();
    		String desc = qs.getLiteral("description").toString();
    		String[] tempDesc = irp.tokenizationWithoutNum(desc);
    		String tempStr = qs.getLiteral("wordName").toString().substring(0, 
					qs.getLiteral("wordName").toString().length()-3);
    		
    		String[] tempArrStr = tempStr.split("\\s+");
    		tempStr = "";
    		for(int x=0; x < tempArrStr.length; x++){
    			if(x == tempArrStr.length-1 && tempArrStr[x].equals("(disambiguation)")) break;
    			else if(x == 0) tempStr = tempStr + tempArrStr[x];
    			else tempStr = tempStr + " " + tempArrStr[x];
    		}
    		
    		for(String td: tempDesc){
    			if(!word.contains(td) && input.containsKey(td)){
    				tempScore += input.get(td);
    			}
    		}
    		
    		if(score < tempScore){
    			score = tempScore;
    			tempMap.remove(tempMap.keySet());
    			
    			/* Getting selected result and its category */
    			Query query = QueryFactory.create(this.getDisambiguationResultAndCategory(uri));        
		        QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlService, query);
	        	Map<String, String> tempCategory = this.getDirectResult(qe.execSelect(), input);
	        	if(!tempCategory.isEmpty()) {
	        		tempMap.putAll(tempCategory);
	        		qe.close();
	        	}
    			tempMap.put(tempStr.toLowerCase(), uri);
    		} else if(score == tempScore){
    			tempMap.remove(tempMap.keySet());
    		}
        }
    	return tempMap;
	}
	
	private Set<Map<String, String>> getResult(ResultSet results){  	
		Set<Map<String, String>> result = new HashSet<Map<String, String>>();
    	for(; results.hasNext(); ){
    		QuerySolution qs = results.nextSolution();
    		String uri = qs.getResource("?typeTopic").getURI().toString();
    		String resource = qs.getResource("?word").getURI().toString();
    		String desc = qs.getLiteral("description").toString();
    		String[] tempDesc = irp.tokenizationWithoutNum(desc);
    		String tempStr = qs.getLiteral("wordName").toString().substring(0, 
					qs.getLiteral("wordName").toString().length()-3);
    		
    		String[] tempArrStr = tempStr.split("\\s+");
    		tempStr = "";
    		for(int x=0; x < tempArrStr.length; x++){
    			if(x == tempArrStr.length-1 && tempArrStr[x].equals("(disambiguation)")) break;
    			else if(x == 0) tempStr = tempStr + tempArrStr[x];
    			else tempStr = tempStr + " " + tempArrStr[x];
    		}
    		
    		Map<String, String> temp = new HashMap<String, String>();
    		temp.put("uri", uri);
    		temp.put("resource", resource);
    		temp.put("desc", desc);
    		temp.put("word", tempStr.trim());
    		result.add(temp);
        }
    	return result;
	}
	
	private Set<String> getResultsUri(ResultSet results){  	
		Set<String> result = new HashSet<String>();
    	for(Map<String, String> m: this.getResult(results)) {
    		result.add(m.get("uri"));
    	}
    	return result;
	}
	
	private Map<String, String> getDirectResult(ResultSet results, Map<String, Integer> input){  	
		Map<String, String> tempMap = new HashMap<String, String>();
    	for(; results.hasNext(); ){
    		QuerySolution qs = results.nextSolution();
    		String uri = qs.getResource("?typeTopic").getURI().toString();
    		String tempStr = qs.getLiteral("wordName").toString().substring(0, 
					qs.getLiteral("wordName").toString().length()-3);
    		
    		String[] tempArrStr = tempStr.split("\\s+");
    		tempStr = "";
    		for(int x=0; x < tempArrStr.length; x++){
    			if(x == tempArrStr.length-1 && tempArrStr[x].equals("(disambiguation)")) break;
    			else if(x == 0) tempStr = tempStr + tempArrStr[x];
    			else tempStr = tempStr + " " + tempArrStr[x];
    		}
    		
    		tempMap.put(tempStr.toLowerCase(), uri);
        }
    	return tempMap;
	}
	
	/* Method to search result based on input */
	public Map<String, String> glossHeuristicDBpedia(List<String> input){
		Map<String, String> mapResult = new HashMap<String, String>();
		Set<String> setInput = new TreeSet<String>();
		
		for(String inp: input){
			String[] temp = irp.stopwordsRemoval(irp.tokenization(inp));
			for(String t: temp) setInput.add(t);
		}
		
		for(String in: setInput){
			int score = 0;
			Map<String, String> tempMap = new HashMap<String, String>();
			Query query = QueryFactory.create(this.searchBySingleInputQueryWithWikiPageDisambiguates(in));        
	        QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlService, query);
	        System.out.println(in);
	        
	        try {
	        	ResultSet results = qe.execSelect();
	        	
	        	for(; results.hasNext(); ){
	        		int tempScore = 0;
	        		QuerySolution qs = results.nextSolution();
	        		String uri = qs.getResource("?typeTopic").getURI().toString();
	        		String desc = qs.getLiteral("description").toString();
	        		String[] tempDesc = irp.tokenization(desc);
	        		String tempStr = qs.getLiteral("wordName").toString().substring(0, 
							qs.getLiteral("wordName").toString().length()-3);
	        		
	        		String[] tempArrStr = tempStr.split("\\s+");
	        		tempStr = "";
	        		for(int x=0; x < tempArrStr.length; x++){
	        			if(x == tempArrStr.length-1 && tempArrStr[x].equals("(disambiguation)")) break;
	        			else if(x == 0) tempStr = tempStr + tempArrStr[x];
	        			else tempStr = tempStr + " " + tempArrStr[x];
	        		}
	        		
	        		for(String td: tempDesc){
	        			if(input.contains(td)){
	        				tempScore++;
	        			}
	        		}
	        		
	        		if(score < tempScore){
	        			score = tempScore;
	        			tempMap.remove(tempMap);
	        			tempMap.put(tempStr.toLowerCase(), uri);
	        		} else if(score == tempScore){
	        			tempMap.remove(tempMap);
	        		}
	            }
	        	mapResult.putAll(tempMap);
	        	qe.close();
	        } catch(Exception e) {
	        	System.out.println(e);
	        	qe.close(); 
	        	continue;
	        }
		}
		
		return mapResult;
	}
	
	public Set<String> validateAnnotation(Map<String, String> map, Set<String> key, String pdfContent){
		Set<String> matchedAll = new HashSet<String>();
		Set<String> keyUri = new HashSet<String>();
		Set<String> annoUri = new HashSet<String>();
		Set<String> annoText = new HashSet<String>();
		int dbpedia = 0;
		
		for(Map.Entry<String,String> m: map.entrySet()) {
			if(!m.getKey().substring(0, 7).contains("dbpedia")) continue;
			annoUri.add(m.getValue());
			annoText.add(m.getKey().substring(8));
			/* Get keyword's categories */
			for(String k: key) {
				if(k.contains(m.getKey().substring(8))) {
					keyUri.add(m.getValue());
					Query query = QueryFactory.create(this.getDisambiguationResultAndCategory(m.getValue()));        
		        	QueryExecution qe = QueryExecutionFactory.sparqlService(sparqlService, query);
	        		keyUri.addAll(this.getResultsUri(qe.execSelect()));
				}
			}
			dbpedia++;
		}
		
		int related = 0;
		for(String a: annoText) {
			String[] split = a.split(" ");
			for(String s: split) 
				if(pdfContent.contains(s)) {
					matchedAll.add(a);
					related++;
					break;
				}
		}
		
		if(dbpedia>0) {
			System.out.println("Number of dbpedia annotation related to text: " + 
				related + " of " + dbpedia + " (" + ((double)related/(double)dbpedia)*100 + "%)");
		} else
			System.out.println("No dbpedia annotation found.");
		
		int keyNum = keyUri.size();
		keyUri.retainAll(annoUri);
		for(String k: keyUri) {
			System.out.println(k);
			for(Map.Entry<String,String> m: map.entrySet()) {
				if(m.getValue().contains(k)) {
					matchedAll.add(m.getKey());
					break;
				}
			}
		}
		
		if(keyNum>0) {
			System.out.println("Number of dbpedia annotation related to keywords: " + 
				keyUri.size() + " of " + keyNum + " (" + ((double)keyUri.size()/(double)keyNum)*100 + "%)");
		} else
			System.out.println("No dbpedia annotation related to keywords found.");
		
		return matchedAll;
	}
}
