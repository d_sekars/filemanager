package rsc.filemanager.common;

/**
 * 
 * @author Diah S R
 *
 */

public class JavaString {
	
	/* Method to cut string after character */
	public String getStrAfterSign(String str, char sign) {		 
		if (countChar(str, sign)){
			str = str.substring(str.indexOf(sign)+1);
		}
		return str;
	}
	
	/* Method to insert space after capital letter
	 * source: http://stackoverflow.com/questions/4886091/insert-space-after-capital-letter */
	public String insertSpaceBeforeCapital(String str) {
		return str = str.replaceAll("(\\p{Ll})(\\p{Lu})","$1 $2");
	}
	
	/* Method to replace given character into space */
	public String replaceCharToSpace(String str, String ch) {
		return str = str.replace(ch, " ");
	}
	
	/* Method to replace space into given character */
	public String replaceSpaceToChar(String str, String ch) {
		return str = str.replace("_", ch);
	}
	
	/* Method to count given character inside string */
	public boolean countChar(String str, char character) {
		char[] ch = str.toCharArray();
		int count = 0;
		
		for (char c: ch){
			if (c == character){
				count++;
			}
		}
		
		if (count == 1) { return true; }
		else { return false; }
	}
}
