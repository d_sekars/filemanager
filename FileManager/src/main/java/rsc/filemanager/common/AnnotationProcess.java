package rsc.filemanager.common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class AnnotationProcess {
	
	private static ExtJwnl ej = new ExtJwnl();
	private static JenaOwl jo = new JenaOwl();
	private static JenaDBpedia jd = new JenaDBpedia();
	private static JavaArray ja = new JavaArray();
	private static PDFBoxManager pbm = new PDFBoxManager();
	private static IrPreprocessing  irp = new IrPreprocessing();
	
	public AnnotationProcess(){
//		jo = new JenaOwl("classpath:../../src/main/resources/ComputingClassificationSystem.owl",
//							"http://www.semanticweb.org/ontologies/2012/acm/ccs");
//		jo = new JenaOwl("F:/git/FileManager/src/main/resources/ComputingClassificationSystem.owl", 
//							"http://www.semanticweb.org/ontologies/2012/acm/ccs");
		try{
			jo = new JenaOwl(System.getProperty("catalina.home") + "/ontology/ComputingClassificationSystem.owl", 
					"http://www.semanticweb.org/ontologies/2012/acm/ccs");
		} catch(Exception e){
//			jo = new JenaOwl(System.getProperty("catalina.home") + "/../src/main/resources/ComputingClassificationSystem.owl", 
//					"http://www.semanticweb.org/ontologies/2012/acm/ccs");
//			jo = new JenaOwl(System.getProperty("catalina.home") + "/../../../ComputingClassificationSystem.owl", 
//					"http://www.semanticweb.org/ontologies/2012/acm/ccs");
			jo = new JenaOwl("C:/Users/admin/OneDrive/Thesis Draft/Ontologi/ComputingClassificationSystem.owl", 
					"http://www.semanticweb.org/ontologies/2012/acm/ccs");
		}
	}
	
	/* Annotation */
	public void combineAllAnnotation(File file, List<String> token){		
		Set<String> setAno = new TreeSet<>();
		
		/* Get annotation from Ontology: 
		 * - Specification Marks
		 * */
		setAno.addAll(this.specificationMarksOnto(token));
		List<String> listAno = new ArrayList<String>(setAno);
		List<String> listAll = new ArrayList<String>(setAno);
		listAll.addAll(token);
		
		/* 
		 * Get annotation from WordNet:
		 * - Specification Marks
		 * - Hypernym/Hyponym Heuristic
		 * - Definition Heuristic
		 * - Gloss Hypernym/Hyponym Heuristic
		 * */
		Map<String, String> mapWordNet = new TreeMap<String, String>();
		mapWordNet.putAll(this.annotationWordNet("all", listAll));
		
		for(Map.Entry<String, String> m : mapWordNet.entrySet()) {
			listAno.add(m.getKey());
		}
		
		/* Get annotation from DBpedia:
		 * - Definition Heuristic
		 * */
		Map<String, String> mapDBpedia = new TreeMap<String, String>();
		mapDBpedia.putAll(this.annotationDBpedia(listAno));
		
		String annoResult = "";
			
		/* Print result: Ontologi */
		for(String l: listAno){
			System.out.println(l);
			if(!annoResult.equals("")) annoResult = annoResult + ";";
			annoResult = annoResult + "l";
		}
		
		Map<String, String> mapResult = new TreeMap<String, String>();
		
		/* Print result: WordNet */
		for(Map.Entry<String, String> m : mapWordNet.entrySet()) {
			System.out.println(m.getKey() + ": " + m.getValue());
			mapResult.put("wordnet:" + m.getKey(), m.getValue());
			annoResult = annoResult + m.getKey();
		}
		/* Print result: DBpedia */
		for(Map.Entry<String, String> m : mapDBpedia.entrySet()) {
			System.out.println(m.getKey() + ": " + m.getValue());
			mapResult.put("dbpedia:" + m.getKey(), m.getValue());
			annoResult = annoResult + m.getKey();
		}
		
		mapResult.put("annotation", annoResult);
		try {
			pbm.writeCustomProperties(file, mapResult);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/* Get annotation from Ontology */
	public Set<String> specificationMarksOnto(List<String> listInput){
		
		Map<String, Set<String[]>> mapStringLevel = new HashMap<>(); 

		/* Get related individual */
		for(String li: listInput){			
			Set<String[]> arrayLevel = jo.listAllLevel(li);	
			mapStringLevel.put(li, arrayLevel);
		}
		
		return this.checkSpecificationMarksOnto(listInput, mapStringLevel);
		
	}
	
	public Map<String, Integer> specificationMarksOnto(Map<String, Integer> input){
		
		Map<String, Set<String[]>> mapStringLevel = new HashMap<>(); 

		/* Get related individual */
		for (Map.Entry<String, Integer> m: input.entrySet()) {
			if(m.getKey().trim() == null || m.getKey().trim().isEmpty() || m.getKey().trim().equals("")) continue;
			Set<String[]> arrayLevel = jo.listAllLevel(m.getKey());
			mapStringLevel.put(m.getKey(), arrayLevel);
		}
		
		return this.checkSpecificationMarksOnto(input, mapStringLevel);
		
	}
	
	/* Specification Marks Score */
	public Set<String> checkSpecificationMarksOnto(List<String> listInput, Map<String, Set<String[]>> mapLevel){
		Set<String> result = new TreeSet<>();
		
		for(String li: listInput){
			int score = 0;
			Set<String[]> tempLevel = mapLevel.get(li);			
			Set<String[]> temp = new HashSet<String[]>();
			
			for(String[] tl: tempLevel){
				int tempScore = 0;
				
				for(String term: tl){
					if(listInput.contains(term)){
						tempScore++;
					}
				}
				
				if(score < tempScore){
					score = tempScore;
					temp.removeAll(temp);
					temp.add(tl);
				} else {
					temp.removeAll(temp);
				}
			}
			
			for(String[] level: temp){
				for(String term: level){
					if(term.equals(li)) break;
					result.add(term.toLowerCase());
				}
			}
				
		}
		
		return result;
	}
	
	public Map<String, Integer> checkSpecificationMarksOnto(Map<String, Integer> input, Map<String, Set<String[]>> mapLevel){
		Map<String, Integer> result = new HashMap<>();
		
		for (Map.Entry<String, Integer> m: input.entrySet()) {
//			if(m.getValue()<3) continue;
			if(m.getKey().trim() == null || m.getKey().trim().isEmpty() || m.getKey().trim().equals("")) continue;
			int score = 0;
			Set<String[]> tempLevel = mapLevel.get(m.getKey());
			Map<String[], Integer> tempMap = new HashMap<>();
			
			System.out.println();
			System.out.println("Word: " + m.getKey());
			System.out.println();
			for(String[] tl: tempLevel){
				int tempScore = 0;
				int level = 0;
				for(String term: tl){
					term = term.toLowerCase();
					System.out.println();
					System.out.print("Level " + level + ": " + term);
					Set<String> inSet = new HashSet<String>(input.keySet());
					Set<String> setSplit = new HashSet<String>(Arrays.asList(term.replaceAll("[^A-Za-z0-9'\\s]", "").split(" ")));
					for(String s: inSet) 
						if(s != null && !s.isEmpty() && !s.trim().equals("") 
							&& (setSplit.contains(s) || setSplit.contains(s+"s") || setSplit.contains(s+"es"))) {
							tempScore += input.get(s);
							System.out.print(" +" + input.get(s));
							if(term.equals(s) || term.equals(s+"s") || term.equals(s+"es")) {
								tempScore++;
								System.out.print(" +" + 1);
							}
						}
					level++;
				}
				
				System.out.println();
				System.out.println("Score = " + tempScore);
				System.out.println();
				
				if(score < tempScore){
					score = tempScore;
					tempMap.clear();
					tempMap.put(tl, m.getValue());
				} else if (score == tempScore) tempMap.clear();
			}
			
			for (Map.Entry<String[], Integer> temp: tempMap.entrySet()) {
				int count = 0;
				String[] str = temp.getKey();
				for(int i=str.length-1; i>=0; i--){
					System.out.println(str[i] + " x " + m.getKey());
					if(!str[i].equals(m.getKey())  && !str[i].equals(m.getKey()+"s") && !str[i].equals(m.getKey()+"es") 
						&& count == 0) continue;
					System.out.println(str[i]);
					count++;
					if((result.containsKey(str[i]) && result.get(str[i])<temp.getValue()) 
						|| !result.containsKey(temp.getKey()[i])) 
						result.put(str[i].toLowerCase(), temp.getValue());
					if(count > 2) break;
				}
			}
				
		}
		
		return result;
	}
	
	/* 
	 * Get annotation from WordNet:
	 * - Specification Marks
	 * - Hypernym/Hyponym Heuristic
	 * - Definition Heuristic
	 * - Gloss Hypernym/Hyponym Heuristic
	 * */
	public Map<String, String> annotationWordNet(String anno, List<String> input) {
		Map<String, String> mapWordNet = new TreeMap<String, String>();
		mapWordNet.putAll(ej.refineResult(anno, input));		
		return mapWordNet;
	}
	public Map<String, String> annotationWordNet(String anno, Map<String, Integer> input) {
		Map<String, String> mapWordNet = new TreeMap<String, String>();
		mapWordNet.putAll(ej.refineResult(anno, input));		
		return mapWordNet;
	}
	
	/* Get annotation from DBpedia:
	 * - Definition Heuristic
	 * */
	public Map<String, String> annotationDBpedia(List<String> input) {
		Map<String, String> mapDBpedia = new TreeMap<String, String>();
		mapDBpedia.putAll(jd.searchByInput(input));
		return mapDBpedia;
	}
	public Map<String, String> annotationDBpedia(Map<String, Integer> input) {
		Map<String, String> mapDBpedia = new TreeMap<String, String>();
		mapDBpedia.putAll(jd.searchByInput(input));
		return mapDBpedia;
	}
	
	/* Search based on annotation */
	public void annoSearch(File folder, String input){
		
		String[] queries = irp.tokenization(input);
		
		/* Read all files inside folder */
		List<File> listFile = pbm.listFilesForFolder(folder);
		
		for(File lf: listFile){
			double score = 0;
			try {
				pbm.readDocumentInformation(lf, "annotation");
				String result = pbm.retrieveDocumentInformation(lf, "annotation");
				List<String> listString = Arrays.asList(result.split(";"));
				for(String q:queries){
					if(listString.contains(q)) score++;
					else listString.add(q);
				}
				score = score/listString.size();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Score for " + lf.getName() + ": " + score);
		}
	}
	
	/* Get keywords */	
	public Set<String> getKeywords(String filePath) throws IOException{
		File file = new File(filePath);
		pbm.setFilePath(filePath);
		String pdfContent = pbm.ToText(1,1);
		
		/* Print title */
		System.out.println();	
		System.out.println("Keywords of document named: " + file.getName());
		System.out.println();
		
		String[] textArr = irp.tokenizationGetAll(pdfContent);
		
		Set<String> result = new HashSet<>();
		 
		 pbm.setFilePath(file.getPath());
		 pbm.readDocumentInformation();
		 String key = pbm.getPdfKeywords();
		 
		 int flag = 0;
		 Set<String> setTemp = new HashSet<String>();
		 for(int i = 1; i<textArr.length; i++){
			 setTemp.add(textArr[i].replaceAll("[^A-Za-z0-9']", ""));
			 if(textArr[i].contains("abstract")) flag = 1;
			 else if(textArr[i].equals("i.") || (textArr[i].equals("1") && (flag == 3)) 
					|| textArr[i].equals("introduction") || textArr[i].equals("ç")) break;
			 else if((textArr[i - 1].equals("index") && textArr[i].contains("terms"))
					|| (textArr[i - 1].equals("key") && textArr[i].contains("words"))
					|| textArr[i].contains("keywords")) flag = 2;
			else if(flag == 2) {
				String temp = "";
				if(textArr[i-1].length() > 6 && textArr[i-1].contains("terms"))
					temp = textArr[i-1].substring(6) + " ";
				else if(textArr[i-1].length() > 9 && textArr[i-1].contains("keywords"))
					temp = textArr[i-1].substring(9) + " ";
				if(textArr[i].equals("-") || textArr[i].equals(":")) i++;
				key = temp + textArr[i];
				flag = 3;
			}
			else if(flag == 3) {
				key = key + " " + textArr[i];
				if(key.substring(key.length()-1).equals("-") || key.charAt(key.length()-1) == '-'
					|| key.substring(key.length()-1).equals("/") || key.charAt(key.length()-1) == '/') {
					key = key + textArr[i+1];
					i++;
				}
				if(key.substring(key.length()-2, key.length()-1).equals(".")
					|| key.charAt(key.length()-1) == '.') break;
			}
		 }
		 
		 if(key != null) {
			String[] split = key.split(",");
			if(split.length < key.split(";").length) split = key.split(";");
		 	for(String s: split){
		 		if(s.length() > 1 && (s.substring(s.length()-1).equals("\\)") || s.charAt(s.length()-1) == ')')) {
		 			String[] sp = s.split("\\(");
		 			result.add(sp[1].substring(0, sp[1].length()-1));
		 			s = sp[0];
		 		}
		 		s = s.replaceAll("[^A-Za-z0-9'\\/\\(\\)\\-\\s]", "").trim();
			 	result.add(s);
		 	}
		 }
		 
		 return result;
	}
	
	/* Getting PDF text score based on word's position */	
	public Map<String, Integer> wordsScoreMapping(File file, String[] textArr) throws IOException{
		 Map<String, Integer> result = new TreeMap<>();
		 
		 pbm.setFilePath(file.getPath());
		 pbm.readDocumentInformation();
		 String key = pbm.getPdfKeywords();
		 
		 int flag = 0;
		 Set<String> setTemp = new HashSet<String>();
		 for(int i = 1; i<textArr.length; i++){
			 setTemp.add(textArr[i].replaceAll("[^A-Za-z0-9']", ""));
			 if(textArr[i].contains("abstract")) flag = 1;
			 else if(textArr[i].equals("i.") || (textArr[i].equals("1") && (flag == 3)) 
					|| textArr[i].equals("introduction") || textArr[i].equals("ç")) break;
			 else if((textArr[i - 1].equals("index") && textArr[i].contains("terms"))
					|| (textArr[i - 1].equals("key") && textArr[i].contains("words"))
					|| textArr[i].contains("keywords")) {
				 	if(flag != 1) {
				 		for(String str: setTemp)
				 			result.put(str, 2);
				 		flag = 1;
				 	}
				 	if(key == null || key.isEmpty()) flag = 2;
			}
			else if(flag == 1) {
				if(textArr[i].substring(textArr[i].length()-1).equals("-")) {
					result.put(textArr[i].substring(0, textArr[i].length()-2).replaceAll("[^A-Za-z0-9']", "")
						+ textArr[i+1], 2);
					i++;
					continue;
				}
				result.put(textArr[i].replaceAll("[^A-Za-z0-9']", ""), 2);
			}
			else if(flag == 2) {
				String temp = "";
				if(textArr[i-1].length() > 6 && textArr[i-1].contains("terms"))
					temp = textArr[i-1].substring(6) + " ";
				else if(textArr[i-1].length() > 9 && textArr[i-1].contains("keywords"))
					temp = textArr[i-1].substring(9) + " ";
				if(textArr[i].equals("-") || textArr[i].equals(":")) i++;
				if(result.containsKey(textArr[i]) && result.get(textArr[i]) == 1) {
					result.remove(textArr[i]);
				}
				key = temp + textArr[i];
				flag = 3;
			}
			else if(flag == 3) {
				if(result.containsKey(textArr[i].replaceAll("[^A-Za-z0-9'\\/\\(\\)\\-\\s]", "")) 
					&& result.get(textArr[i].replaceAll("[^A-Za-z0-9'\\/\\(\\)\\-\\s]", "")) == 1) {
					System.out.println("Remove: " + textArr[i].replaceAll("[^A-Za-z0-9'\\/\\(\\)\\-\\s]", ""));
					result.remove(textArr[i].replaceAll("[^A-Za-z0-9'\\/\\(\\)\\-\\s]", ""));
				}
				key = key + " " + textArr[i];
				if(key.substring(key.length()-1).equals("-") || key.charAt(key.length()-1) == '-'
					|| key.substring(key.length()-1).equals("/") || key.charAt(key.length()-1) == '/') {
					key = key + textArr[i+1];
					i++;
				}
				if(key.substring(key.length()-2, key.length()-1).equals(".")
					|| key.charAt(key.length()-1) == '.') break;
			}
		 }
		 
		 if(key != null) {
			String[] split = key.split(",");
			if(split.length < key.split(";").length) split = key.split(";");
		 	for(String s: split){
		 		if(s.length() > 1 && (s.substring(s.length()-1).equals("\\)") || s.charAt(s.length()-1) == ')')) {
		 			String[] sp = s.split("\\(");
		 			result.put(sp[1].substring(0, sp[1].length()-1), 3);
		 			s = sp[0];
		 		}
		 		s = s.replaceAll("[^A-Za-z0-9'\\/\\(\\)\\-\\s]", "").trim();
			 	result.put(s, 3);
		 	}
		 }
		 
		 return result;
	}
	
	public Map<String,String> annoProcessFullText(String filePath) throws IOException{
		/**
		 * Whole anno process
		 */
		File pdfFile = new File(filePath);
		pbm.setFilePath(filePath);
		String pdfContent = pbm.ToText(1,1);
		
		/* Print title */
		System.out.println();	
		System.out.println("Annotation process for: " + pdfFile.getName());
		System.out.println();	
		
		long annoStartTime = System.currentTimeMillis();
		
		String[] textArr = irp.tokenizationGetAll(pdfContent);
		
		for(String s: textArr) System.out.println(s);
		
		Map<String, Integer> wordsScore = new TreeMap<>();
		String annoText = "";
		
		Map<String, Integer> wordsScoreMap = this.wordsScoreMapping(pdfFile, textArr);
		wordsScore.putAll(wordsScoreMap);
		for(Map.Entry<String, Integer> m : wordsScoreMap.entrySet()) {
			if(m.getValue() != 3) continue;
			else if(!annoText.equals("")) annoText = annoText + ";";
			annoText = annoText + m.getKey();
		}
		
		if(wordsScore.size() < 1)
			for(String i: textArr) wordsScore.put(i.replaceAll("[^A-Za-z0-9'\\s]", "").trim(), 1);
		
		for(Map.Entry<String, Integer> m : wordsScore.entrySet()) {
			System.out.println(m.getKey() + ": " + m.getValue());
		}
		
		/* Annotation from Ontology: 
		 * - Specification Marks
		 * */
		Map<String, Integer> mapOnto = this.specificationMarksOnto(wordsScore);
		System.out.println("Annotation onto: " + mapOnto.size());
		
		/* Add ontology result to annotation text */
		for(Map.Entry<String, Integer> m : mapOnto.entrySet()) {
			if(!annoText.equals("")) annoText = annoText + ";";
			annoText = annoText + m.getKey();
			if((wordsScore.containsKey(m.getKey()) && wordsScore.get(m.getKey()) < 2)
				|| !wordsScore.containsKey(m.getKey())) wordsScore.put(m.getKey(), 2);
		}
		System.out.println("Annotation + onto: " + annoText);
		
		Map<String, String> mapAnno = new TreeMap<String, String>();
		
		/* Get annotation from DBpedia:
		 * - Definition Heuristic
		 * */
		Map<String, String> mapDBpedia = new TreeMap<String, String>();
		mapDBpedia.putAll(this.annotationDBpedia(wordsScore));
		
		/* Add DBpedia result to annotation text and annotation map */
		for(Map.Entry<String, String> m : mapDBpedia.entrySet()) {
			System.out.println(m.getKey());
			String[] temp = m.getKey().split(":", 2);
			annoText = annoText + ";" + temp[0];
			mapAnno.put("dbpedia:" + m.getKey(), m.getValue());
			if((wordsScore.containsKey(m.getKey()) && wordsScore.get(m.getKey()) < 2) 
				|| !wordsScore.containsKey(m.getKey())) wordsScore.put(m.getKey(), 2);
		}
		
		/* 
		 * Get annotation from WordNet:
		 * - Specification Marks
		 * - Hypernym/Hyponym Heuristic
		 * - Definition Heuristic
		 * - Gloss Hypernym/Hyponym Heuristic
		 * */
		Map<String, String> mapWordNet = new TreeMap<String, String>();
		mapWordNet.putAll(this.annotationWordNet("all", wordsScore));
		System.out.println("Annotation wordnet: " + mapWordNet.size());
		
		/* Add WordNet result to annotation text and annotation map */
		for(Map.Entry<String, String> m : mapWordNet.entrySet()) {
			annoText = annoText + ";" + m.getKey();
			mapAnno.put("wordnet:" + m.getKey(), m.getValue());
		}
		System.out.println("Annotation + wordnet: " + annoText);
		
		/* Add annotation to  */
		System.out.println("Annotation final: " + annoText);
		mapAnno.put("annotation", annoText);
		
		long annoEndTime = System.currentTimeMillis();
		
//		System.out.println("Annotation time: " + (annoStartTime - annoEndTime));
		
		return mapAnno;
	}
	
	public void annoProcess(String filePath) throws IOException{
		/**
		 * Whole anno process
		 */
		File pdfFile = new File(filePath);
		pbm.setFilePath(filePath);
		String pdfContent = pbm.ToText(1,1);
		
		/* Print title */
		System.out.println();	
		System.out.println("Annotation process for: " + pdfFile.getName());
		System.out.println();	
		
		String[] textArr = irp.tokenizationGetAll(pdfContent);
		
		long annoStartTime = System.currentTimeMillis();
		
		Map<String, Integer> wordsScore = new TreeMap<>();
		String annoText = "";
		
		Map<String, Integer> wordsScoreMap = this.wordsScoreMapping(pdfFile, textArr);
		wordsScore.putAll(wordsScoreMap);
		for(Map.Entry<String, Integer> m : wordsScoreMap.entrySet()) {
			if(m.getValue() != 3) continue;
			else if(!annoText.equals("")) annoText = annoText + ";";
			annoText = annoText + m.getKey();
		}
		for(Map.Entry<String, Integer> m : wordsScore.entrySet()) {
			System.out.println(m.getKey() + ": " + m.getValue());
		}
		
		/* Annotation from Ontology: 
		 * - Specification Marks
		 * */
		Map<String, Integer> mapOnto = this.specificationMarksOnto(wordsScore);
		System.out.println("Annotation onto ("+ pdfFile.getName() +"): " + mapOnto.size());
		
		/* Add ontology result to annotation text */
		for(Map.Entry<String, Integer> m : mapOnto.entrySet()) {
			System.out.println("Annotation onto: "+ m.getKey());
			if(!annoText.equals("")) annoText = annoText + ";";
			annoText = annoText + m.getKey();
			if((wordsScore.containsKey(m.getKey()) && wordsScore.get(m.getKey()) < 2)
				|| !wordsScore.containsKey(m.getKey())) wordsScore.put(m.getKey(), 2);
		}
		System.out.println("Annotation + onto ("+ pdfFile.getName() +"): " + annoText);
		
		Map<String, String> mapAnno = new TreeMap<String, String>();
		
		/* Get annotation from DBpedia:
		 * - Definition Heuristic
		 * */
		Map<String, String> mapDBpedia = new TreeMap<String, String>();
		mapDBpedia.putAll(this.annotationDBpedia(wordsScore));
		
		/* Add DBpedia result to annotation text and annotation map */
		for(Map.Entry<String, String> m : mapDBpedia.entrySet()) {
			System.out.println("Annotation dbpedia: " + m.getKey());
			String[] temp = m.getKey().split(":", 2);
			annoText = annoText + ";" + temp[0];
			mapAnno.put("dbpedia:" + m.getKey(), m.getValue());
			if((wordsScore.containsKey(m.getKey()) && wordsScore.get(m.getKey()) < 2) 
				|| !wordsScore.containsKey(m.getKey())) wordsScore.put(m.getKey(), 2);
		}
		
		/* 
		 * Get annotation from WordNet:
		 * - Specification Marks
		 * - Hypernym/Hyponym Heuristic
		 * - Definition Heuristic
		 * - Gloss Hypernym/Hyponym Heuristic
		 * */
		Map<String, String> mapWordNet = new TreeMap<String, String>();
		mapWordNet.putAll(this.annotationWordNet("all", wordsScore));
		System.out.println("Annotation wordnet ("+ pdfFile.getName() +"): " + mapWordNet.size());
		
		/* Add WordNet result to annotation text and annotation map */
		for(Map.Entry<String, String> m : mapWordNet.entrySet()) {
			System.out.println("Annotation wordnet: " + m.getKey());
			annoText = annoText + ";" + m.getKey();
			mapAnno.put("wordnet:" + m.getKey(), m.getValue());
		}
		System.out.println("Annotation + wordnet ("+ pdfFile.getName() +"): " + annoText);
		
		/* Add annotation to  */
		System.out.println("Annotation final: " + annoText);
		mapAnno.put("annotation", annoText);
		
		try {
			pbm.writeCustomProperties(pdfFile, mapAnno);			
		} catch(Exception e){
			e.printStackTrace();
		}
		
		long annoEndTime = System.currentTimeMillis();
		
		System.out.println("Annotation time ("+ pdfFile.getName() +"): " + (annoStartTime - annoEndTime));
	}
	
}
