package rsc.filemanager.common.demo;

/**
 * @author Diah S R
 * 
 */

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import rsc.filemanager.common.IrPreprocessing;
import rsc.filemanager.common.IrWeighting;
import rsc.filemanager.common.JavaArray;
import rsc.filemanager.common.JavaMap;
import rsc.filemanager.common.PDFBoxManager;

public class IrDemo {
	
	private static IrPreprocessing  irp = new IrPreprocessing();
	private static IrWeighting irw = new IrWeighting();
	private static PDFBoxManager pbm = new PDFBoxManager();
	private static JavaArray ja = new JavaArray();
	private static JavaMap jm = new JavaMap();
	private static String[][] words;
	
	/* Method to display IR Preprocessing demo */
	public static void irPrepDemo() throws IOException {	
		
		String pdfContent;
		
		/* Read PDF content by type file */
		File pdfFile = new File("F:\\Tomcat\\apache-tomcat-8.0.15\\uploads\\Social-Aware Edge Caching in Fog Radio Access Networks.pdf");
		pdfContent = pbm.readTextByFile(pdfFile);
		
		/* Print PDF content */
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println("*********************** PDF Content **********************");
		System.out.println();
		System.out.println(pdfContent);
		System.out.println();
		System.out.println("**********************************************************");
		System.out.println();
		System.out.println();
		
		/* Tokenization process */
		String[] token = irp.tokenization(pdfContent);
		
		System.out.println("*************** Tokenization and Stemming ****************");
		System.out.println();
		for (int i = 0; i < token.length; i++) {
			/* Print tokenization result */
			System.out.print("#" + i + " | Before stemming: " + token[i]);
			System.out.print(" | ");
			
			/* Stemming process */
			token[i] = irp.porterStemmer(token[i]);
			
			/* Print stemming result */
			System.out.println("After stemming: " + token[i]);
		}  
		System.out.println();
		System.out.println("**********************************************************");
		System.out.println();
		System.out.println();
		
		/* Unique token + sorting + remove stop words + count frequency */
		words = irp.uniqueCountFreq(token);
		System.out.println("********************** Unique token **********************");
		System.out.println();
		for (int i = 0; i < words.length; i++) {
			System.out.println(words[i][0] + ": " + words[i][1]);
		}
		System.out.println();
		System.out.println("**********************************************************");
		System.out.println();
		System.out.println();
		
	} 
	
	/* Method to display IR Weighting demo */
	public static void irWeightVSMDemo(){	
		
		/* VSM Weighting Demo
		 * Reference (book): Introduction to Information Retrieval page 124 Example 6.4 */
		
		/* number of documents in collection */
		int n = 1000000;
		
		/* query from user */
		String[] query = {"best", "car", "insurance"};
		
		/* list of dictionary term respectively with the query and its document frequency */
		String[][] dictionary = {{"auto", "5000"}, {"best", "50000"}, {"car", "10000"}, 
				{"insurance", "1000"}};
		
		/* list of dictionary term available in respective document and query along
		 * with its frequency inside the document */
		String[][] doc = {{"auto", "1"}, {"car", "1"}, {"insurance", "2"}};
		Map<String, Double> docTf = jm.mapTermArray(doc);
		
		/* get only doc's term */
		String[] docTerm = ja.getBySecondIndex(doc, 0);
		
		/* get unique concatenate array */
		String[] term = ja.concatArray(query, docTerm);
		
		/* map document's term frequency based on concatenated result */
		for (int i = 0; i < term.length; i++) {
			if (docTf.get(term[i]) == null){
				docTf.put(term[i], 0.0);
			} else {
				docTf.put(term[i], docTf.get(term[i]));
			}
		}
		
		/* map query term's frequency based on concatenated result */
		Map<String, Double> queryTf = new HashMap<>();
		for (int i = 0; i < term.length; i++) {
			if (Arrays.asList(query).contains(term[i])){
				queryTf.put(term[i], 1.0);
			} else {
				queryTf.put(term[i], 0.0);
			}
		}
		
		/* map dictionary */
		Map<String, Double> dc = jm.mapTermArray(dictionary);
		
		/* test VSM calculation */
		irw.vcmScore(n, queryTf, docTf, dc);		
		
		System.out.println();
		System.out.println("**********************************************************");
		System.out.println();
		System.out.println();
	}
	
	public static void main(String[] args) throws IOException {
		/* Run demo */
//		irPrepDemo();
//		irWeightVSMDemo();
//		psm.runDemo();
		
//		System.out.print("distributed -> ");
//		irp.porterStemmerTartarus("distributed");
//		System.out.print("gently -> ");
//		irp.porterStemmerTartarus("gently");
//		System.out.print("freely -> ");
//		irp.porterStemmerTartarus("freely");
//		System.out.print("family -> ");
//		irp.porterStemmerTartarus("family");
//		System.out.print("business -> ");
//		irp.porterStemmerTartarus("business");
		
		System.out.println("input: www.ieee.org, key-word");
		System.out.println("result:");
		String[] str = irp.tokenization("www.ieee.org key-word");
		for(String s: str) System.out.println(s);
	}
}
