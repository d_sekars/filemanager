package rsc.filemanager.common.demo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import rsc.filemanager.common.AnnotationProcess;
import rsc.filemanager.common.IrPreprocessing;
import rsc.filemanager.common.JavaArray;
import rsc.filemanager.common.JenaOwl;
import rsc.filemanager.common.PDFBoxManager;

public class AnnotationDemo {
	
	private static AnnotationProcess ano = new AnnotationProcess();
	private static JenaOwl jo = new JenaOwl();
	private static IrPreprocessing  irp = new IrPreprocessing();
	private static PDFBoxManager pbm = new PDFBoxManager();
	private static JavaArray ja = new JavaArray();	
	private static String[][] words;

	public static void main(String[] args) throws IOException {
		
		/* Variable to store PDF content */
		String pdfContent;
		
		/* Read PDF content by type file */
//		File pdfFile = new File("F:\\How I Love to study\\Thesis\\Code\\Documents\\PDF\\pdf-sample.pdf");
//		File pdfFile = new File("F:\\Tomcat\\apache-tomcat-8.0.15\\uploads\\Social-Aware Edge Caching in Fog Radio Access Networks.pdf");
		File pdfFile = new File("D:\\annotation\\A Self-Adaptive Multi-Agent System Approach for Collaborative Mobile Learning.pdf");
//		pbm.setFilePath("F:\\Tomcat\\apache-tomcat-8.0.15\\uploads\\Social-Aware Edge Caching in Fog Radio Access Networks.pdf");
//		pbm.setFilePath("D:\\Tomcat\\apache-tomcat-8.0.45\\uploads\\Social-Aware Edge Caching in Fog Radio Access Networks.pdf");
		pbm.setFilePath("D:\\annotation\\A Self-Adaptive Multi-Agent System Approach for Collaborative Mobile Learning.pdf");
//		pbm.setFilePath("D:\\Tomcat\\apache-tomcat-8.0.45\\uploads\\Handoffs Management in Follow-the-Sun Software Projects A Case Study.pdf");
//		pdfContent = pbm.readTextByFile(pdfFile);
		pdfContent = pbm.ToText(1,1);
//		System.out.println(pdfContent);
		
		/* Print PDF content */
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println("*********************** PDF Content **********************");
//		System.out.println();
//		System.out.println(pdfContent);
//		System.out.println();
//		System.out.println("**********************************************************");
//		System.out.println();
//		System.out.println();
		
		/* Tokenization process */
//		String[] token = irp.tokenization(pdfContent);
		
//		System.out.println("*************** Tokenization and Stemming ****************");
//		System.out.println();
//		for (int i = 0; i < token.length; i++) {
//			/* Print tokenization result */
//			System.out.print("#" + i + " | Before stemming: " + token[i]);
//			System.out.print(" | ");
//			
//			/* Stemming process */
//			token[i] = irp.porterStemmer(token[i]);
//			
//			/* Print stemming result */
//			System.out.println("After stemming: " + token[i]);
//		}  
//		System.out.println();
//		System.out.println("**********************************************************");
//		System.out.println();
//		System.out.println();
		
		/* Unique token + sorting + remove stop words + count frequency */
//		words = irp.uniqueCountFreq(token);
//		System.out.println("********************** Unique token **********************");
//		System.out.println();
//		for (int i = 0; i < words.length; i++) {
//			System.out.println(words[i][0] + ": " + words[i][1]);
//		}
//		System.out.println();
//		System.out.println("**********************************************************");
//		System.out.println();
//		System.out.println();
//		
//		/* Put words into list */
//		List<String> listWords = new ArrayList<String>();
//		for(String[] w: words){
//			listWords.add(w[0]);
//		}
		
		/* Annotation Process */
//		ano.combineAllAnnotation(pdfFile, listWords);
		
		/* Search based on annotation */
//		File folder = new File(System.getProperty("catalina.home") + "\\uploads");
//		ano.annoSearch(folder, "web trouble display");
		
		/* Print PDF content */
//		File pdfFile = new File(System.getProperty("catalina.home") +
//				"\\uploads\\A Fog Based Middleware for Automated Compliance With OECD Privacy Principles in Internet of Healthcare Things.pdf");
//				"\\uploads\\Design of an off-grid energy kiosk in rural Zambia.pdf");
//		pbm.setFilePath(pdfFile.getPath());
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println("*********************** PDF Content **********************");
//		System.out.println();
//		System.out.println(pdfContent);
//		System.out.println();
//		System.out.println("**********************************************************");
//		System.out.println();
//		System.out.println();
//		System.out.println();
		
//		Map<String, Integer> map = ano.wordsScore(pdfFile);
//		map.putAll(ano.specificationMarksOnto(map));
//		Map<String, String> mapResult = ano.annotationWordNet("all", map);
//		for(Map.Entry<String, String> m : mapResult.entrySet()) {
//			System.out.println(m.getKey() + ": " + m.getValue());
//		}
		
		ano.annoProcess(pdfFile.getAbsolutePath());
		
		/* Read all files inside folder */
//		File folder = new File("D:\\annotation");
//		List<File> listFile = pbm.listFilesForFolder(folder);
		List<File> listFile = new ArrayList<File>();
		listFile.add(pdfFile);
		int flag = 0;
		
//		for(File lf: listFile){
//			if (lf.getName().contains("Analysis and Implementation Hardware-Software of Rijndael Encryption")) flag = 1;
//			if (flag == 0) continue;
//			if (lf.getName().contains("Wireless Powered Communication Networks Assisted by Backscatter Communication"))
//				annoProcess(lf.getAbsolutePath());
//			ano.annoProcess(lf.getAbsolutePath());
//			if (lf.getName().contains("A case study on the misclassification of software performance issues in an issue tracking system.pdf")) 
//			f'lag = 0;
//			break;
//		}
		
	}
	
	public static void annoProcess(String filePath) throws IOException{
		/**
		 * Whole anno process
		 */
		File pdfFile = new File(filePath);
		pbm.setFilePath(filePath);
//		String pdfContent = pbm.ToText(1,1);
		String pdfContent = "Abstract - Computer hardware is the physical parts or components of a computer, "
				+ "such as the monitor, keyboard, computer data storage, hard disk drive (HDD), "
				+ "graphic cards, sound cards, memory (RAM), motherboard, and so on, "
				+ "all of which are tangible physical objects. Index Terms - Computer hardware, hardware.";
		
		/* Print title until abstract */
		String[] textArr = irp.tokenizationGetAll(pdfContent);
		
		long annoStartTime = System.currentTimeMillis();
		
		Map<String, Integer> wordsScore = new TreeMap<>();
		String annoText = "";
		
		Map<String, Integer> wordsScoreMap = ano.wordsScoreMapping(pdfFile, textArr);
		wordsScore.putAll(wordsScoreMap);
		for(Map.Entry<String, Integer> m : wordsScore.entrySet()) {
			System.out.println(m.getKey() + ": " + m.getValue());
		}
		
		/* Annotation from Ontology: 
		 * - Specification Marks
		 * */
		Map<String, Integer> mapOnto = ano.specificationMarksOnto(wordsScore);
		System.out.println("Annotation onto ("+ pdfFile.getName() +"): " + mapOnto.size());
		
		/* Add ontology result to annotation text */
		for(Map.Entry<String, Integer> m : mapOnto.entrySet()) {
			if(!annoText.equals("")) annoText = annoText + ";";
			annoText = annoText + m.getKey();
			if((wordsScore.containsKey(m.getKey()) && wordsScore.get(m.getKey()) < 2)
				|| !wordsScore.containsKey(m.getKey())) wordsScore.put(m.getKey(), 2);
		}
		System.out.println("Annotation + onto ("+ pdfFile.getName() +"): " + annoText);
		
		Map<String, String> mapAnno = new TreeMap<String, String>();
		
		/* Get annotation from DBpedia:
		 * - Definition Heuristic
		 * */
		Map<String, String> mapDBpedia = new TreeMap<String, String>();
		mapDBpedia.putAll(ano.annotationDBpedia(wordsScore));
		
		/* Add DBpedia result to annotation text and annotation map */
		for(Map.Entry<String, String> m : mapDBpedia.entrySet()) {
			System.out.println(m.getKey());
			String[] temp = m.getKey().split(":", 2);
			annoText = annoText + ";" + temp[0];
			mapAnno.put("dbpedia:" + m.getKey(), m.getValue());
			if((wordsScore.containsKey(m.getKey()) && wordsScore.get(m.getKey()) < 2) 
				|| !wordsScore.containsKey(m.getKey())) wordsScore.put(m.getKey(), 2);
		}
		
		/* 
		 * Get annotation from WordNet:
		 * - Specification Marks
		 * - Hypernym/Hyponym Heuristic
		 * - Definition Heuristic
		 * - Gloss Hypernym/Hyponym Heuristic
		 * */
		Map<String, String> mapWordNet = new TreeMap<String, String>();
		mapWordNet.putAll(ano.annotationWordNet("all", wordsScore));
		System.out.println("Annotation wordnet ("+ pdfFile.getName() +"): " + mapWordNet.size());
		
		/* Add WordNet result to annotation text and annotation map */
		for(Map.Entry<String, String> m : mapWordNet.entrySet()) {
			annoText = annoText + ";" + m.getKey();
			mapAnno.put("wordnet:" + m.getKey(), m.getValue());
		}
		System.out.println("Annotation + wordnet ("+ pdfFile.getName() +"): " + annoText);
		
		/* Add annotation to  */
		System.out.println("Annotation final: " + annoText);
		mapAnno.put("annotation", annoText);
		
		for(Map.Entry<String, String> m : mapAnno.entrySet()) {
			System.out.println(m.getKey() + ": " + m.getValue());
		}
		
		try {
			pbm.writeCustomProperties(pdfFile, mapAnno);			
		} catch(Exception e){
			e.printStackTrace();
		}
		
		long annoEndTime = System.currentTimeMillis();
		
		System.out.println("Annotation time ("+ pdfFile.getName() +"): " + (annoStartTime - annoEndTime));
	}

}
