package rsc.filemanager.common.demo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rsc.filemanager.common.JavaString;
import rsc.filemanager.common.JenaDBpedia;
import rsc.filemanager.common.JenaOwl;

/**
 * @author Diah S R
 */

public class JenaDemo {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
//		String file = "F:/git/FileManager/src/main/resources/ComputingClassificationSystem.owl",
		String file = "C:/Users/admin/OneDrive/Thesis Draft/Ontologi/ComputingClassificationSystem20170910.owl",
			inputQueryString2 = "Ultra-large-scale systems",
			rdf = "http://www.w3.org/1999/02/22-rdf-syntax-ns",
			owl = "http://www.w3.org/2002/07/owl",
			xsd = "http://www.w3.org/2001/XMLSchema",
			rdfs = "http://www.w3.org/2000/01/rdf-schema",
			uri = "http://www.semanticweb.org/ontologies/2012/acm/ccs",
			queryString =
				"SELECT DISTINCT ?s ?p ?o " +
				"WHERE { ?s ?p ?o } " +
				"LIMIT 10",
			queryString1 =
				"PREFIX owl: <" + owl + "#> " +
				"SELECT DISTINCT ?s " +
				"WHERE { ?s a owl:NamedIndividual } " +
				"ORDER BY ?s " +
				"LIMIT 10",
			queryString2 =
				"PREFIX rdfs:<" + rdfs + "#> " +
				"PREFIX : <" + uri + "#> " +
				"SELECT ?label " +
				"WHERE { " + 
					"?s :hasSuperior ?o. " +
					"?o rdfs:label ?label. " +
					"?s rdfs:label \"" + inputQueryString2 + "\"@en " +
				"}";
		
		JenaOwl jenaOwl = new JenaOwl(file, uri);
		JavaString javaString = new JavaString();
		JenaDBpedia jenaDbp = new JenaDBpedia();
		
//		jenaOwl.runQuery(queryString);
//		jenaOwl.runQuery(queryString1);
//		jenaOwl.runQuery(queryString2);
//		jenaOwl.printLabel(queryString2);
//		jenaOwl.printURI(queryString2);
		
//		List<String> listLabel = jenaOwl.listIndividualByProperty("Peer-to-peer architectures", "hasSuperior");
//		List<String> listLabel = jenaOwl.listIndividualByProperty("Ultra-large-scale systems", "hasSuperior");
//		List<String> listLabel = jenaOwl.listIndividualByProperty("Software and its engineering", "hasSuperior");
		Set<String[]> setLevel = jenaOwl.listAllLevel("hardware");
		for(String[] level: setLevel){
			for(String l: level) {
				System.out.println(l);
			}
			System.out.println();
		}
		
//		jenaOwl.individualSize();
//		jenaOwl.listInstances(uri, "Term");
//		jenaOwl.getPropertyValue();
		
//		System.out.println(
//			javaString.insertSpaceBeforeCapital(
//				javaString.getStrAfterSign("http://www.w3.org/2000/01/rdf-schema#subClassOf", '#')
//		));
//		System.out.println(
//			javaString.replaceCharToSpace(
//				javaString.getStrAfterSign("Analog_and_mixed-signal_circuit_optimization", '#'), "_"
//		));
		
//		jenaDbp.getLabelByInput("Hardware");
//		jenaDbp.getLabelByInput("Analog circuit");
//		jenaDbp.searchByInputQueryTest("plant, tree, perennial, leaf, nature, earth, flora, wood, branch, forest, land");
//		jenaDbp.searchByInput("plant, tree, perennial, leaves, nature, earth, flora, wood, branch, forest, land");
//		jenaDbp.searchByInput("plant tree");
//		jenaDbp.searchByInput("Analog circuit");
		
		Set<String> set = new HashSet<String>();
//		list.add("plant, tree, perennial, leaves, nature, earth, flora, wood, branch, forest, land");
//		list.add("middleware");
//		jenaDbp.defHeuristicDBpedia(list);
		
//		set = jenaOwl.searchIndividualLikely("artificial");
//		for(String l: set){
//			System.out.println(l);
//		}
		
//		set = jenaOwl.searchIndividualLikely("hardware");
//		for(String l: set){
//			System.out.println(l);
//		}
	}
}
