package rsc.filemanager.common.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rsc.filemanager.common.Node;

/**
 * Source: http://programtalk.com/java/java-tree-implementation/
 */

public class NodeDemo {
	 
	public static void main(String[] args) {
		Node<String> treeRootNodeString = new Node<String>(null);
		treeRootNodeString.setNodeName("root");
		
		// add child to root node 
		Node<String> child1= treeRootNodeString.addChild(treeRootNodeString, "child-1");
		// add child to the child node created above
		Node<String> child11= treeRootNodeString.addChild(child1, "child-11");
		treeRootNodeString.addChild(child1, "child-12");
		   
		treeRootNodeString.addChild(child11, "child-111");
		treeRootNodeString.addChild(child11, "child-112");
		
		// add child to root node
		Node<String> child2 = treeRootNodeString.addChild(treeRootNodeString, "child-2");
		// add child to the child node created above
		treeRootNodeString.addChild(child2, "child-21");
		   
		treeRootNodeString.printTree(treeRootNodeString, " ");
		treeRootNodeString.findNode(treeRootNodeString, "child-21");	
		
		Node<Long> treeRootNodeLong = new Node<Long>(null);
		treeRootNodeLong.setNodeName((long) 0);
		
		// add child to root node 
		treeRootNodeLong.addChild(treeRootNodeLong, (long) 1);
		// add child to the child node created above
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 1), (long) 10);
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 1), (long) 11);
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 1), (long) 12);
		
		// add child to root node 
		treeRootNodeLong.addChild(treeRootNodeLong, (long) 2);
		// add child to the child node created above
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 2), (long) 20);
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 2), (long) 21);
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 2), (long) 22);
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 2), (long) 22);
		
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 20), (long) 201);
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 20), (long) 202);
		
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 0), (long) 3);
		treeRootNodeLong.addChild(treeRootNodeLong.findNode(treeRootNodeLong, (long) 3), (long) 22);
		
		treeRootNodeLong.printTree(treeRootNodeLong, " ");
		treeRootNodeLong.findNode(treeRootNodeLong, (long) 11);
		List<Node<Long>> listChild = treeRootNodeLong.findNode(treeRootNodeLong, (long) 1).getChildren();
		System.out.println("Child of 1:");
		for (Node<Long> node: listChild) {
			System.out.println(node.getNodeName());
		}
		
		if(treeRootNodeLong.isDecendant(treeRootNodeLong, (long) 0, (long) 22)){
			System.out.println("22 is descendant of 0");
			System.out.println(treeRootNodeLong.countDecendant(treeRootNodeLong, (long) 0, (long) 22));
		}
		
		System.out.println(treeRootNodeLong.isDecendant(treeRootNodeLong, (long) 0, (long) 4));
		
		Node<Long> treeRootNodeLong1 = new Node<Long>(null);
		treeRootNodeLong1.setNodeName((long) 0);
		
		// add child to root node 
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 0, (long) 1);
		// add child to the child node created above
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 1, (long) 10);
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 1, (long) 11);
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 1, (long) 12);
		
		// add child to root node 
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 0, (long) 2);
		// add child to the child node created above
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 2, (long) 20);
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 2, (long) 21);
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 2, (long) 22);
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 2, (long) 22);
		
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 20, (long) 201);
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 20, (long) 202);
		
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 0, (long) 3);
		treeRootNodeLong1.addChildren(treeRootNodeLong1, (long) 3, (long) 22);
		
		System.out.println(
			"Parent 202: " +
			treeRootNodeLong1.findNode(treeRootNodeLong1, (long) 202).getParent().getNodeName()
		);
		
		List<Node<Long>> listPath = new ArrayList<>();
		Map<Long, Integer> mapPath = new HashMap<>();
		listPath = treeRootNodeLong1.findHypernymPath(listPath, treeRootNodeLong1.findNode(treeRootNodeLong1, (long) 202));
		mapPath = treeRootNodeLong1.mapHypernymPathLevel(treeRootNodeLong1.findNode(treeRootNodeLong1, (long) 202));
		
		for (Node<Long> node: listPath) {
			System.out.print(node.getNodeName() + " ");
		}
		System.out.println();
		
		System.out.println("Level 202: " + mapPath.get((long) 202));
		
		treeRootNodeLong1.printTree(treeRootNodeLong1, " ");
	}
}