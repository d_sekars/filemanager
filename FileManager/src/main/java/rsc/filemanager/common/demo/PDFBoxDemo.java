package rsc.filemanager.common.demo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import rsc.filemanager.common.IrPreprocessing;
import rsc.filemanager.common.PDFBoxManager;

public class PDFBoxDemo {

	private static PDFBoxManager pdfBox = new PDFBoxManager();
	private static IrPreprocessing  irp = new IrPreprocessing();
	
	public static void main(String[] args) throws IOException {
		/* Preparing PDF document */
		String pdfFile = "F:\\How I Love to study\\Thesis\\Code\\Documents\\PDF\\pdf-sample.pdf";
		File folder = new File(System.getProperty("catalina.home") + "\\uploads");
		File file = new File(pdfFile);
		
		String text = "This is an example of adding text to a page in the pdf document. "
				+ "We can add as many lines as we want like this using the showText() method of the ContentStream class.";
		
		String ano = "acrobat;adobe;analog and mixed-signal circuits;analysis and design of emerging devices and systems;application;application layer protocols;application specific instruction set processors;application specific integrated circuits;application specific processors;application-specific vlsi designs;architectures;bio-embedded electronics;biographies;biology-related information processing;carbon based electronics;circuit substrates;colour;common;communication hardware, interfaces and storage;compact;compact delay models;computer systems organization;computing classification system;computing standards, rfcs and guidelines;contextual software domains;control path algorithms;correct;create;data conversion;design reuse and communication-based design;device;display;displays and imagers;distribute;distributed architectures;distributed memory;distributed systems organizing principles;document;document types;download;easy;electronic;electronic blackboards;electronic design automation;embedded and cyber-physical systems;embedded software;embedded systems;emerging architectures;emerging languages and compilers;emerging simulation;emerging technologies;emerging tools and methodologies;encounter;energy distribution;exact;fast;fiber distributed data interface (fddi);file;file systems management;flexible and printable circuits;font;formal specifications;format;free;general and reference;general conference proceedings;general literature;graphic;hardware;hardware test;hardware-software codesign;high-level and register-transfer level synthesis;ideal;incompatible;integrated circuits;lost;model-driven software engineering;multithreading;network algorithms;network file system (nfs) protocol;network properties;network protocols;network resources allocation;network security;network structure;network types;networks;neural systems;oam protocols;on-chip resource management;open;operate;overcome;overlay and other logical network structures;page;pdf;peer-to-peer architectures;platform;platform power issues;platform-based design;portable;power and energy;power conversion;power estimation and optimization;preserve;print;printed circuit boards;printers;problem;protocol correctness;protocol testing and verification;read;read-only memory;real-time languages;real-time operating systems;real-time system architecture;real-time system specification;real-time systems;recipient;reconfigurable logic and fpgas;reconfigurable logic applications;reference works;regardless;resource binding and sharing;semiconductor memory;sensor applications and deployments;sensor devices and platforms;share;small;software;software and its engineering;software architectures;software infrastructure;software organization and properties;software system models;software system structures;source;surveys and overviews;system;testing with distributed and parallel systems;time;time synchronization protocols;timing analysis;ultra-large-scale systems;universal;version;very large scale integration design;virtual worlds software;virtual worlds training simulations;web;web protocol security;world wide web (network structure);adjust;application;application program;applications programme;correct;create;data file;device;display;distribute;document;file;make;papers;preserve;save;set;source;system;universal;universal joint;video display;written document";
		String query = "about acrobat";
		
		/* Read all files inside folder */
		List<File> listFile = pdfBox.listFilesForFolder(folder);
		
		for(File lf: listFile){
			pdfBox.readDocumentInformation(lf, "annotation");
			String[] anno = pdfBox.retrieveDocumentInformation(lf, "annotation").split(";");
			for(String a: anno){
				if(!a.contains("middleware")) continue;
				pdfBox.readDocumentInformation(lf, "wordnet:"+a);
				pdfBox.readDocumentInformation(lf, "dbpedia:"+a);
			}
//			pdfBox.setFilePath(lf.getPath());
//			String txt = pdfBox.ToText(1, 1);
//			System.out.println(txt);
//			System.out.println();
		}
		
		/* Add new page with given text */
//		pdfBox.addNewPageWithText(file, text);
		
		/* Add custom properties */
//		Map<String, String> mapCustomProperties = new HashMap<String, String>();
//		mapCustomProperties.put("annotation", ano);
//		mapCustomProperties.put("annotation:adjust", "alter or regulate so as to achieve accuracy or conform to a standard");
//		mapCustomProperties.put("annotation:application", "a program that gives a computer instructions that provide the user with tools to accomplish a task");
//		mapCustomProperties.put("annotation:application program", "a program that gives a computer instructions that provide the user with tools to accomplish a task");
//		mapCustomProperties.put("annotation:applications programme", "a program that gives a computer instructions that provide the user with tools to accomplish a task");
//		mapCustomProperties.put("annotation:correct", "alter or regulate so as to achieve accuracy or conform to a standard");
//		mapCustomProperties.put("annotation:create", "make or cause to be or to become");
//		mapCustomProperties.put("annotation:data file", "a set of related records (either written or electronic) kept together");
//		mapCustomProperties.put("annotation:device", "an instrumentality invented for a particular purpose");
//		mapCustomProperties.put("annotation:display", "an electronic device that represents information in visual form");
//		mapCustomProperties.put("annotation:distribute", "make available");
//		mapCustomProperties.put("annotation:document", "writing that provides information (especially information of an official nature)");
//		mapCustomProperties.put("annotation:file", "a set of related records (either written or electronic) kept together");
//		mapCustomProperties.put("annotation:make", "make or cause to be or to become");
//		mapCustomProperties.put("annotation:papers", "writing that provides information (especially information of an official nature)");
//		mapCustomProperties.put("annotation:preserve", "to keep up and reserve for personal or special use");
//		mapCustomProperties.put("annotation:save", "to keep up and reserve for personal or special use");
//		mapCustomProperties.put("annotation:set", "alter or regulate so as to achieve accuracy or conform to a standard");
//		mapCustomProperties.put("annotation:source", "a document (or organization) from which information is obtained");
//		mapCustomProperties.put("annotation:system", "instrumentality that combines interrelated interacting artifacts designed to work as a coherent entity");
//		mapCustomProperties.put("annotation:universal", "coupling that connects two rotating shafts allowing freedom of movement in all directions");
//		mapCustomProperties.put("annotation:universal joint", "coupling that connects two rotating shafts allowing freedom of movement in all directions");
//		mapCustomProperties.put("annotation:video display", "an electronic device that represents information in visual form");
//		mapCustomProperties.put("annotation:written document", "writing that provides information (especially information of an official nature)");
//		
//		pdfBox.writeCustomProperties(file, mapCustomProperties);
	}

	
}
