package rsc.filemanager.common.demo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import rsc.filemanager.common.IrPreprocessing;
import rsc.filemanager.common.JavaArray;
import rsc.filemanager.common.PDFBoxManager;

public class ReadCustomPropDemo {
	
	private static PDFBoxManager pbm = new PDFBoxManager();
	private static IrPreprocessing  irp = new IrPreprocessing();
	private static JavaArray ja = new JavaArray();

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		pbm.setFilePath("D:\\Tomcat\\apache-tomcat-8.0.45\\uploads\\Cultural Identity Restoration and Purposive Website Design A Hermeneutic Study of the Chickasaw and Klamath Tribes.pdf");
//		pbm.setFilePath("D:\\annotation\\uploads\\Wireless Powered Communication Networks Assisted by Backscatter Communication.pdf");
//		pbm.setFilePath("D:\\uploads\\A Bit of Code How the Stack Overflow Community Creates Quality Postings.pdf");
		String pdfContent = pbm.ToText(1,1);
		
		/* Print title until abstract */
//		String[] textArr = irp.tokenizationWithoutNum(pdfContent);
//		ja.uniqueArrayAsc(textArr);
//		int flag = 0;
//		int count = 0;
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println("*********************** PDF Content **********************");
//		System.out.println();
//		for(int i=0; i<textArr.length; i++){
//			count++;
//			System.out.print(textArr[i]);
//			if(textArr[i].contains("abstract")) flag = 1;
//			else if((textArr[i].contains("index") && textArr[i+1].contains("terms"))
//				|| (textArr[i].contains("key") && textArr[i+1].contains("words"))
//				|| textArr[i].contains("keywords")) flag = 2;
//			else if(textArr[i+1].contains("introduction") && (flag==1 || flag==2)) break;
//			System.out.print(" ");
//			if(count==10){
//				count = 0;
//				System.out.println();
//			}
//		}
//		System.out.println();
//		System.out.println();
//		System.out.println("**********************************************************");
//		System.out.println();
//		System.out.println();
//		System.out.println();
		
		File folder = new File("D:\\Tomcat\\apache-tomcat-8.0.45\\uploads");
//		File folder = new File("D:\\uploads");
		File pdfFile = new File(pbm.getFilePath());
		
		/* Read all files inside folder */
//		List<File> listFile = new ArrayList<File>();
//		listFile.add(pdfFile);
		List<File> listFile = pbm.listFilesForFolder(folder);
		
		for(File lf: listFile){
			System.out.println("Annotation for " + lf.getName());
			pbm.readDocumentInformation(lf, "annotation");
			String[] anno = pbm.retrieveDocumentInformation(lf, "annotation").split(";");
			for(String a: anno){
				pbm.readDocumentInformation(lf, "wordnet:"+a);
				pbm.readDocumentInformation(lf, "dbpedia:"+a);
			}
			System.out.println();
		}
		
	}

}
