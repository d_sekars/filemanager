package rsc.filemanager.common.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RandomDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		String str = "a:b:c";
//		String[] split = str.split(":", 3);
//		for(String s: split){
//			System.out.println(s);
//		}
		
		sort();
		
//		String test = "a, b, c, d, and e";
//		System.out.println(test.replace(" ", "_").replaceAll("\\p{P}", ""));
//		System.out.println(test.replace(", ", "_").replace("and ", ""));
//		String[] strTest = test.split(", ");
//		for(String s: strTest) System.out.println(s.replace("and ", ""));
		
//		String test = "a-b, c, d, and e";
//		test.replace(",", " ");
//		String[] strTest = test.split(" ");
//		for(String s: strTest) System.out.println(s);
		
//		Integer num = 10, number = 0;
//		System.out.println(num.toString());
//		number = Integer.parseInt(num.toString());
//		System.out.println(number);
		
//		String a = "a b c d e f";
//		String[] split = a.split(" ");
//		for(int i = 0; i < split.length - 1; i++){
//			System.out.println(split[i] + " " + split[i+1]);
//		}
	}
	
	public static void sort(){
		List<Integer> values = new ArrayList<Integer>();
		values.add(5);
		values.add(3);
		values.add(1);
		values.add(2);
		values.add(4);
		values.add(4);
		values.add(4);
		values.add(5);
		values.add(2);
		values.add(1);
		for(int i = 0; i < values.size(); i++){
			int smaller = i;
			for(int j = i + 1; j < values.size(); j++){
				if(values.get(smaller) > values.get(j)) smaller = j;
			}
			int temp = values.get(smaller);
			values.set(smaller, values.get(i));
            values.set(i, temp);
		}
		for(int v: values){
			System.out.print(v + " ");
		}
	}

}
