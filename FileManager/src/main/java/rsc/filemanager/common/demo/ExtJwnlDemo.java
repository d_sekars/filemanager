 package rsc.filemanager.common.demo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import rsc.filemanager.common.ExtJwnl;
import rsc.filemanager.common.dbConnect;

/**
 * 
 * @author Diah S R
 *
 */

public class ExtJwnlDemo {
	
	private static ExtJwnl extJwnl = new ExtJwnl();
	private static dbConnect dbc = new dbConnect();

	public static void main(String[] args) throws SQLException {
		
//		extJwnl.getHypernyms("house");
//		extJwnl.getHypernyms("leaf");
//		extJwnl.getHypernyms("set aside");
		
//		extJwnl.getHypernymNodeTree("house");
//		extJwnl.getHypernymNodeTree("plant tree perennial leaf");
//		extJwnl.getHypernymNodeTree("leaf");
		
//		extJwnl.getHypernymHyponymNodeTree("plant tree perennial leaf");
//		extJwnl.getHypernymHyponymNodeTree("plant tree perennial leaf nature earth flora wood branch forest land");
//		extJwnl.getHypernymHyponymNodeTree("tree");
		
//		extJwnl.getSpecificationMarks("set aside");
//		extJwnl.getSpecificationMarks("house");
//		extJwnl.getSpecificationMarks("plant tree perennial leaf");
//		extJwnl.getSpecificationMarks("plant tree perennial leaf nature earth flora wood branch forest land");
		String in = "plant, tree, perennial, leaf, nature, earth, flora, wood, branch, forest, land";
		List<String> ls = Arrays.asList(in.split("\\s*,\\s*"));
//		List<String> ls = new ArrayList<String>();
//		ls.add("plant");
//		ls.add("tree");
//		ls.add("perennial");
//		ls.add("leaf");
//		ls.add("nature");
//		ls.add("earth");
//		ls.add("flora");
//		ls.add("wood");
//		ls.add("forest");
//		ls.add("land");
//		extJwnl.specificationMarksWordNet(ls);
//		extJwnl.getMappedWordAllLevelSynset(ls);
//		extJwnl.testSynset("plant");
		
//		extJwnl.getHypernymHeuristic("plant tree perennial leaf nature earth flora wood branch forest land");
		
//		extJwnl.getDefinitionHeuristic("plant tree perennial leaf nature earth flora wood branch forest land");
		
//		extJwnl.getGlossHypernymHeuristic("plant tree perennial leaf nature earth flora wood branch forest land");
		
//		extJwnl.getDomainHeuristic("plant");
//		extJwnl.getDomainHeuristic("genotype");
//		extJwnl.getDomainHeuristic("bank");
//		extJwnl.getDomainHeuristic("chromosome");
//		extJwnl.getDomainHeuristic("pretty mess");
//		extJwnl.getDomainHeuristic("house");
//		extJwnl.getDomainHeuristic("light");
//		extJwnl.getDomainHeuristic("software engineering");
//		extJwnl.getDomainHeuristic("hardware, software engineering, embedded software, redundancy");
//		extJwnl.getDomainHeuristic("government politics");
//		extJwnl.getDomainHeuristic("There are a number of ways in which the chromosome structure can change, which will detrimentally change the genotype and phenotype of the organism.");
//		extJwnl.getDomainHeuristic("music");
//		extJwnl.getDomainHeuristic("music art photography");
//		extJwnl.getDomainHeuristic("photography");
		
//		extJwnl.getAllSynset();
		
//		extJwnl.testSynset("bear");
//		extJwnl.testSynset("blue");
//		extJwnl.testSynset("angrily");
//		extJwnl.testSynset("worse");
//		extJwnl.testSynset("book");
//		extJwnl.testSynset("booked");
//		extJwnl.testSynset("set aside");
//		extJwnl.testSynset("house");
//		extJwnl.testSynset("cat");
//		extJwnl.testSynset("dog");
		extJwnl.testSynset("keyboard");
		
//		System.out.println(extJwnl.getSynsetByString("algorithm").size());
		
//		extJwnl.testHypernyms("set aside");
//		extJwnl.testHypernyms("leaf");
//		extJwnl.testHypernyms("grow");
//		extJwnl.testHypernyms("house");
//		extJwnl.testHypernyms("plant");
		
//		extJwnl.testHyponyms("plant");
		
//		extJwnl.tryGetAsymmetricRelation("dog", "cat");
		
//		extJwnl.tryGetSymmetricRelation("funny", "droll");
		
//		Map<String, Double> cv = new HashMap<>(), sv = new HashMap<>();
//		cv.put("Biology", 0.00102837);
//		cv.put("Ecology", 0.00402855);
//		cv.put("Botany", 0.00000320408);
//		cv.put("Zoology", 0.0000177959);
//		cv.put("Anatomy", 0.000012959);
//		cv.put("Physiology", 0.000226531);
//		cv.put("Chemistry", 0.000179857);		
//		cv.put("Geology", 0.0000166327);
//		cv.put("Meteorology", 0.00371308);
//		sv.put("Biology", 0.047627);
//		sv.put("Ecology", 0.084778);
//		sv.put("Bowling", 0.019687);
//		sv.put("Archaeology", 0.016451);
//		sv.put("Sociology", 0.014251);
//		sv.put("Alimentation", 0.006510);
//		sv.put("Linguistics", 0.005297);
//		
//		double numerator = 0, denominator = 0, cvLength = 0, svLength = 0, result = 0;
//		for (Map.Entry<String, Double> m : cv.entrySet()) {
//			if (sv.containsKey(m.getKey())) {
//				numerator = numerator + (m.getValue() * sv.get(m.getKey()));
//			}
//			cvLength = cvLength + Math.pow(m.getValue(),2);
//		}
//		for (Map.Entry<String, Double> m : sv.entrySet()) {
//			svLength = svLength + Math.pow(m.getValue(),2);
//		}
//		cvLength = Math.sqrt(cvLength);
//		svLength = Math.sqrt(svLength);
//		denominator = cvLength * svLength;
//		result = numerator / denominator;
//		System.out.println(numerator + "/" + denominator + " = " + result);
		
		
		/*Getting domain*/
//		Set<String> domain = extJwnl.getDomain("Computer Science");
//		for (String d: domain) {
//			System.out.println("Domain: " + d);
//			String[] split = d.split("#");
//			for(int i = 2; i < split.length-2; i++){
//				System.out.println(split[i]);
//				domain.addAll(extJwnl.getDomain(split[i]));
//			}
//		}
//		
//		Map<String, Set<String>> domainFull = new HashMap<String, Set<String>>();
//		for (String d: domain) {
//			System.out.println("Domain expanded: " + d);
//			String[] split = d.split("#");
//			for(int i = 2; i < split.length-2; i++){
//				domainFull.putAll(extJwnl.getDomainMemberHyponym(split[i]));
//			}
//		}
//		for (Map.Entry<String, Set<String>> m : domainFull.entrySet()) {
//			if (!m.getKey().toLowerCase().contains("computer")) {
//				System.out.println(m.getKey());
//				System.out.println("skipped");
//				continue;
//			}
//			System.out.println(m.getKey() + ": ");
//			System.out.println(m.getValue().size() + " member.");
			
			/* Split domain */
//			String[] split = m.getKey().split("#");
//			
//			int duplicate = 0;
//			
//			for (String member: m.getValue()) {
//				System.out.println("Member: " + member);
				
				/* Split domain member */
//				String[] splitMember = member.split("#");
//				Set<String> hypernym = extJwnl.getDirectHypernym(splitMember[1].charAt(0), splitMember[0]);
//				Set<String> hyponym = extJwnl.getDirectHyponym(splitMember[1].charAt(0), splitMember[0]);
//				
//				for(String hyp: hypernym) {
//					System.out.println("Hypernym: " + hyp);
//					String[] splitHyper = hyp.split("#");
//					
					/* Insert hypernym of member */
//					System.out.println("insert into direct_hypernym_mapping "
//							+ "values (" +  splitHyper[0] + ",\"" + splitHyper[1] + "\"," 
//							+ splitMember[0] + ",\"" + splitMember[1] + "\")");
//					if(!dbc.alreadyExists("select * from direct_hypernym_mapping where hypernym_offset = " + splitHyper[0]
//							+ " and hypernym_pos = \"" + splitHyper[1] 
//							+"\" and hyponym_offset = " + splitMember[0]
//							+ " and hyponym_pos = \"" + splitMember[1] + "\""))
//					dbc.runUpdate("insert into direct_hypernym_mapping "
//							+ "values (" +  splitHyper[0] + ",\"" + splitHyper[1] + "\"," 
//							+ splitMember[0] + ",\"" + splitMember[1] + "\")");
//					
//					Set<String> setHyper = extJwnl.getDirectHypernym(splitHyper[1].charAt(0), splitHyper[0]);
//					for(String hyper: setHyper) {
//						System.out.println("Hypernym of direct hypernym: " + hyper);
//						String[] splitHypernym = hyper.split("#");
						
						/* Insert hypernym of direct hypernym */
//						System.out.println("insert into direct_hypernym_mapping "
//								+ "values (" +  splitHypernym[0] + ",\"" + splitHypernym[1] + "\"," 
//								+ splitHyper[0] + ",\"" + splitHyper[1] + "\")");
//						if(!dbc.alreadyExists("select * from direct_hypernym_mapping where hypernym_offset = " 
//								+ splitHypernym[0] + " and hypernym_pos = \"" + splitHypernym[1] 
//								+"\" and hyponym_offset = " + splitHyper[0]
//								+ " and hyponym_pos = \"" + splitHyper[1] + "\""))
//						dbc.runUpdate("insert into direct_hypernym_mapping "
//								+ "values (" +  splitHypernym[0] + ",\"" + splitHypernym[1] + "\"," 
//								+ splitHyper[0] + ",\"" + splitHyper[1] + "\")");
//					}
//				}
//				
//				for(String ho: hyponym) {
//					System.out.println("Hyponym: " + ho);
//					String[] splitHypo = ho.split("#");
//					
					/* Insert hyponym of member */
//					System.out.println("insert into direct_hypernym_mapping "
//							+ "values (" +  splitMember[0] + ",\"" + splitMember[1] + "\"," 
//							+ splitHypo[0] + ",\"" + splitHypo[1] + "\")");
//					if(!dbc.alreadyExists("select * from direct_hypernym_mapping where hypernym_offset = " 
//							+ splitMember[0] + " and hypernym_pos = \"" + splitMember[1] 
//							+"\" and hyponym_offset = " + splitHypo[0]
//							+ " and hyponym_pos = \"" + splitHypo[1] + "\""))
//					dbc.runUpdate("insert into direct_hypernym_mapping "
//							+ "values (" +  splitMember[0] + ",\"" + splitMember[1] + "\"," 
//							+ splitHypo[0] + ",\"" + splitHypo[1] + "\")");
					
//					Set<String> setHypo = extJwnl.getDirectHyponym(splitHypo[1].charAt(0), splitHypo[0]);
//					for(String hypo: setHypo) {
//						System.out.println("Hyponym of direct hyponym: " + hypo);
//						String[] splitHyponym = hypo.split("#");
//						
						/* Insert hyponym of direct hyponym */
//						System.out.println("insert into direct_hypernym_mapping "
//								+ "values (" +  splitHypo[0] + ",\"" + splitHypo[1] + "\"," 
//								+ splitHyponym[0] + ",\"" + splitHyponym[1] + "\")");
//						if(!dbc.alreadyExists("select * from direct_hypernym_mapping where hypernym_offset = " 
//								+ splitHypo[0] + " and hypernym_pos = \"" + splitHypo[1] 
//								+"\" and hyponym_offset = " + splitHyponym[0]
//								+ " and hyponym_pos = \"" + splitHyponym[1] + "\""))
//						dbc.runUpdate("insert into direct_hypernym_mapping "
//								+ "values (" +  splitHypo[0] + ",\"" + splitHypo[1] + "\"," 
//								+ splitHyponym[0] + ",\"" + splitHyponym[1] + "\")");
//					}
//				}
				
				/* Insert domain and its member's mapping */
//				System.out.println("insert into domain_mapping "
//						+ "values (" +  split[0] + ",\"" + split[1] + "\"," 
//						+ splitMember[0] + ",\"" + splitMember[1] + "\")");
//				if(!dbc.alreadyExists("select * from domain_mapping where domain_offset = " + split[0]
//						+ " and domain_pos = \"" + split[1] +"\" and domain_member_offset = " + splitMember[0]
//						+ " and domain_member_pos = \"" + splitMember[1] + "\""))
//				dbc.runUpdate("insert into domain_mapping "
//						+ "values (" +  split[0] + ",\"" + split[1] + "\"," 
//						+ splitMember[0] + ",\"" + splitMember[1] + "\")");
				
				/* Insert synset offset and its description */
//				System.out.println("insert into synset "
//						+ "values (" +  splitMember[0] + ",\"" + splitMember[1]
//						+ "\",\"" + splitMember[splitMember.length-1] + "\")");
//				if(!dbc.alreadyExists("select * from synset where synset_offset = " + splitMember[0]
//						+ " and synset_pos = \"" + splitMember[1] +"\""))
//					dbc.runUpdate("insert into synset values (" +splitMember[0] + ",\"" + splitMember[1] + "\",\"" 
//						+ splitMember[splitMember.length-1].replace("\"", "\\\"") + "\")");
//				else duplicate++;
				
				/* Insert word sense */
//				for (int i = 2; i < splitMember.length - 1; i++) {
//					System.out.println("insert into wordsense "
//							+ "values (" +  splitMember[0] + ",\"" + splitMember[1]
//							+ "\",\"" + splitMember[i] + "\")");
//					if(!dbc.alreadyExists("select * from wordsense where wordsense_offset = " + splitMember[0]
//							+ " and wordsense_pos = \"" + splitMember[1] + "\""
//							+ " and wordsense_lemma = \"" + splitMember[i] + "\""))
//						dbc.runUpdate("insert into wordsense values ("+  splitMember[0]
//								+",\"" + splitMember[1] + "\""
//								+",\"" + splitMember[i].replace("\"", "\\\"") + "\")");
////						
//				}
//			}
//			
//		System.out.println(duplicate);
	}
		
}
