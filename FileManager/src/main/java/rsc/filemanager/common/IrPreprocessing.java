package rsc.filemanager.common;

import java.io.File;

/**
 * @author Diah S R
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import rsc.filemanager.common.PDFBoxManager;
import rsc.filemanager.common.PorterStemmer;
import rsc.filemanager.common.PorterStemmerModified;

public class IrPreprocessing {
	
	private static JavaArray ja = new JavaArray();	
	private static PDFBoxManager pbm = new PDFBoxManager();
	private static AnnotationProcess ap = new AnnotationProcess();
	
	public String[][] IrPrep(File pdfFile) throws IOException {
		pbm.setFilePath(pdfFile.getAbsolutePath());
		String pdfContent = pbm.ToText(1, 1);
		String[] token = this.tokenization(pdfContent); /* Split PDF content into token */
		Map<String, Integer> map = ap.wordsScoreMapping(pdfFile, this.tokenizationGetAll(pdfContent));
		if(map.size() > 0) { 
			Set<String> setToken = new HashSet<String>();
			for(Map.Entry<String, Integer> m : map.entrySet()) {
				System.out.println(m.getValue() + ": " + m.getKey());
				setToken.add(m.getKey());
			}
			token = setToken.toArray(new String[setToken.size()]);
		}
		token = this.multipleStem(token); /* Stemming process */
		String[][] tokenPosting = this.uniqueCountFreq(token); /* Unique + frequency + sort */
		return tokenPosting;
	}
	
	/* Method to read PDF content by file path */
	public String pdfReadByFilePath(String pdfPath) throws IOException{
	    pbm.setFilePath(pdfPath);
	    return pbm.ToText();
	}
	
	/* Method to transforms whole content into tokens */
	public String[] tokenization(String txt){
		txt = txt
				.toLowerCase()
				.replaceAll("[^A-Za-z0-9'\\s]", "");
		String[] token = this.stopwordsRemoval(txt.split("\\s+")); /* \\s+ groups all white spaces */
		return token;
	}
	
	public String[] tokenizationWithoutNum(String txt){
		txt = txt
				.toLowerCase()
				.replaceAll("[^A-Za-z'\\s]", "");
		String[] token = this.stopwordsRemoval(txt.split("\\s+")); /* \\s+ groups all white spaces */
		token = ja.uniqueArrayAsc(token);
		return token;
	}
	
	public String[] tokenizationGetAll(String txt){
		txt = txt
				.toLowerCase();
		String[] token = this.stopwordsRemoval(txt.split("\\s+")); /* \\s+ groups all white spaces */
		return token;
	}
	
	/* Method to transforms a word into its origin form */
	public String porterStemmer(String txt){
		PorterStemmerModified s = new PorterStemmerModified();
		txt = s.stemResult(txt);
		return txt;
	}
	
	public String porterStemmerTartarus(String txt){
		PorterStemmer s = new PorterStemmer();
		s.add(txt.toCharArray(), txt.length());
		s.stem();
//		System.out.println(s.toString());
		return s.toString();
	}
	
	/* Method to handle multiple words stemming process */
	public String[] multipleStem(String[] txt){
		for (int i = 0; i < txt.length; i++) {
			txt[i] = porterStemmerTartarus(txt[i]);
		}
		return txt;
	}
	
	/* Method to remove stopwords */
	public String[] stopwordsRemoval(String[] words){
		
		/* Resource (book): Introduction to Information Retrieval page 26 with addition*/
		List<String> stopWords = 
				Arrays.asList("a", "at", "able", "about", "all", "also", "although", "always", "an", "another", 
						"above", "any", "anyone", "anywhere", "again", "ahead", "aim",
						"along", "await", "ask", "asume", "away", "after",
						"am", "among", "and", "are", "as", "at", "be", "become", "but", "both", "before",
						"been", "being", "because", "became", "bring", "between",
						"by", "can", "can't", "did", "do", "does", "doesn't",
						"come", "came", "called",
						"doesnt", "done", "don't", "dont", "due", "each", "even", 
						"for", "from", "get", "go", "given", "gives",
						"has", "have", "he", "her", "here", "hers", "him", "how",
						"let", "like", "leave",
						"his", "i", "if", "i'm", "im", "in", "into", "is", "it", "its",  
						"itself", "made", "make", "may", "me", "might", "many",
						"mine", "more", "most", "need", "not",
						"no", "nor", "now", "of", "on", "once", 
						"only", "or", "other", "our", "out", 
						"rather", "regardless", "so", "sometime", "should",
						"sometimes", "such", "than", "this",
						"that", "the", "their", "them", "there", "they", "those", "these",
						"the", "to", "us", "use", "until", "up", "very",  "we", "when", "what",
						"was", "way", "were", "who", "while", "where", "whose", "whether",
						"will", "willing", "with", "wish", "wink", "would", "wouldn't", 
						"wouldnt", "well", "which", "within", "without",
						"yet", "you", "your", "yours");
		
		ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(words)); 
		
		for(String word : words) {
			if(stopWords.contains(word.replaceAll("[^A-Za-z0-9'\\s]", ""))){
				arrayList.remove(word);
			}
		}
		
		words = new String[arrayList.size()];
		words = arrayList.toArray(words);
		return words;
	}
	
	/* Method to:
	 * - Sort array
	 * - Remove stop words
	 * - Get unique words n its frequency */
	public String[][] uniqueCountFreq(String[] words){
		
		String tmp = "";
		int i = 0, count = 0;
		
		Arrays.sort(words);
		words = stopwordsRemoval(words);
		
		String[][] uniqueArr = new String[ja.uniqueArrayAsc(words).length][2];
		
		for (int x = 0; x < words.length; x++) {
			if(!tmp.equals(words[x])){
				if (!tmp.equals("")) { i++; }
				tmp = words[x];
				count = 1;
				uniqueArr[i][0] = words[x];
			} else { count++; }
			uniqueArr[i][1] = Integer.toString(count);
		}
		return uniqueArr;
	}
	
}
