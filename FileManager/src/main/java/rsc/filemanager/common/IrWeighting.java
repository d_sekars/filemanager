package rsc.filemanager.common;

/**
 * 
 * @author Diah S R
 *
 */

import java.util.HashMap;
import java.util.Map;

public class IrWeighting {
	
	private static JavaMap jm = new JavaMap();
	
	/* Method to count IR weighting score (Vector Space Model) 
	 * Reference (book): Introduction to Information Retrieval page 124 Example 6.4 */
	public double vcmScore(int n, Map<String, Double> query, Map<String, Double> term, 
			Map<String, Double> dictionary){	
		Map<String, Double> mapScore = new HashMap<>(), wtdMapScore = new HashMap<>();
		double score = 0;
		
		/* calculate idf score */
		System.out.println("idf score:");
		mapScore = idfScore(n, dictionary);
		jm.printMap(mapScore);
		System.out.println();
		
		/* calculate W(t,q): tf * idf with tf = term frequency */
		System.out.println("W(t,q):");
		mapScore = wtqScore(mapScore, query);
		jm.printMap(mapScore);
		System.out.println();
		
		/* calculate W(t,d) */
		System.out.println("W(t,d):");
		wtdMapScore = wtdScore(term);
		jm.printMap(wtdMapScore);
		System.out.println();
		
		/* calculate dot product W(t,q) and W(t,d) */
		System.out.println("W(t,q) * W(t,d):");
		mapScore = jm.mapsDotProduct(mapScore, wtdMapScore);
		jm.printMap(mapScore);
		System.out.println();
		
		/* calculate final VSM score */
		System.out.println("VSM final score between query and respective document:");
		if(!mapScore.isEmpty()) score = jm.mapAddAll(mapScore);
		else score = -1; /* not displaying it when no query terms appears in document */ 
		System.out.println("Score = " + score);
		System.out.println();
		
		return score;
	}
	
	/* Method to calculate idf weighting for each term */
	public Map<String, Double> idfScore(int n, Map<String, Double> map){
		Map<String, Double> score = new HashMap<>();
		for (Map.Entry<String, Double> m : map.entrySet()) {
			if(m.getValue() == 0) score.put(m.getKey(), (double) 0);
			else score.put(m.getKey(), Math.log10(n/m.getValue()));
		}
		return score;
	}
	
	/* Method to calculate W(t,q) for VSM: W(t,q) = tf * idf 
	 * Reference (book): Introduction to Information Retrieval page 124 Example 6.4 */
	public Map<String, Double> wtqScore(Map<String, Double> idf, Map<String, Double> term){
		Map<String, Double> score = new HashMap<>();
		for (Map.Entry<String, Double> t : term.entrySet()) {
			score.put(t.getKey(), idf.get(t.getKey()) * t.getValue());
		}
		return score;
	}

	/* Method to calculate W(t,d) for VSM using Euclidean normalization 
	 * Reference (book): Introduction to Information Retrieval page 124 Example 6.4 */
	public Map<String, Double> wtdScore(Map<String, Double> term){
		Map<String, Double> score = new HashMap<>();
		double div = 0;
		
		/* calculate divider */
		for (Map.Entry<String, Double> t : term.entrySet()) {
			div = div + Math.pow(t.getValue(), 2);
		}
		div = Math.sqrt(div);
		
		/* calculate each term's score and put in map score */
		for (Map.Entry<String, Double> t : term.entrySet()) {
			if(div == 0) score.put(t.getKey(), (double) 0); 
			else score.put(t.getKey(), t.getValue()/div);
		}
		return score;
	}
	
}
