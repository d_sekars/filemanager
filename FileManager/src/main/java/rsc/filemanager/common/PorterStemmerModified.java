package rsc.filemanager.common;

import java.util.Arrays;
import java.util.List;

/**
 * Adopt codes and algorithm from http://tartarus.org/~martin/PorterStemmer/java.txt
 * Original codes remains in rsc.filemanager.common.PorterStemmer.java
 * 
 * Some changes made in this class to cater some issues.
 * 
 * Issues from last code:
 * - Some rules triggers error, 
 *   e.g Step 2: freely -> freeli
 *       Step 5: individual -> individu
 *               advance -> adv
 *               etc
 * - Needs additional rules, e.g. words ends with suffix "s": files -> file
 * - Rules that needs m() method calculation, 
 *   didn't attach the calculation inside the validation
 * - Needs further research to get best approach to cater the rules
 * 
 * Edited by Diah S R
 */

public class PorterStemmerModified {
	
	private String word;
	
	/* global variable setter */
	public void setWord(String word){
		this.word = word;
	}
	
	/* global variable getter */
	public String getWord(){
		return word;
	}
	
	/* Code from Porter Stemmer.
	 * cons(i) is true <=> b[i] is a consonant. 
	 */

	private final boolean cons(int i) {
		switch (word.charAt(i)) {
			case 'a': case 'e': case 'i': case 'o': case 'u': return false;
			case 'y': return (i==0) ? true : !cons(i-1);
			default: return true;
		}
	}
	
	/* Code from Porter Stemmer.
	 * m() measures the number of consonant sequences between 0 and j. if c is
	 * a consonant sequence and v a vowel sequence, and <..> indicates arbitrary
	 * presence,
	 * 
	 * <c><v>       gives 0
	 * <c>vc<v>     gives 1
	 * <c>vcvc<v>   gives 2
	 * <c>vcvcvc<v> gives 3
	 * ....
	 */

	private final int m(String endChars) {
		int n = 0;
	    int i = 0;
	    int endLength = endChars.length();
	    String txt = "";
	    if ((word.length() < (endLength + 1))){
	    	return n;
	    }
	    txt = word.substring(0, word.length() - endLength);
	    while(true) {
	    	if (i == txt.length()) return n;
	    	if (! cons(i)) break;
	    	i++;
	    }
	    i++;
	    while(true) {
	    	while(true) {
	    		if (i == txt.length()) return n;
	    		if (cons(i)) break;
	    		i++;
	    	}
	    	i++;
	    	n++;
	    	while(true){
	    		if (i == txt.length()) return n;
	    		if (! cons(i)) break;
	    		i++;
	    	}
	    	i++;
	     }
	 }
	
	/* Code from Porter Stemmer.
	 * cvc(i) is true <=> i-2,i-1,i has the form consonant - vowel - consonant
	 * and also if the second c is not w,x or y. this is used when trying to
	 * restore an e at the end of a short word. e.g.
	 * 
	 * cav(e), lov(e), hop(e), crim(e), but
	 * snow, box, tray.
	 */

	 private final boolean cvc(int i) {
		 i = word.length() - i;
		 if (i < 2 || !cons(i) || cons(i-1) || !cons(i-2)) return false; {  
			 int ch = word.charAt(i);
			 if (ch == 'w' || ch == 'x' || ch == 'y') return false;
		 }
		 return true;
	 }
	 
	 /* Modify doublec() method from Porter Stemmer. */
	 private final boolean endsWithDoubleConsonant(int checkPosition) {
		 if (word.substring(word.length() - checkPosition)
					.equals(word.substring(word.length() - checkPosition - 1
							, word.length() - checkPosition))) {
			 return true;
		 }
		 return false;
	 }
	 
	 /* Addition: calculating m value based on endsWith() input */
	 private boolean calcM(String endChars, int limit){
		 if (endsWith(endChars) && (m(endChars) > limit)) { return true; }
		 return false;
	 }
	 
	 /* Modify ends() method from Porter Stemmer. */
	 private final boolean endsWith(String endChars){
		 if ((word.length() < (endChars.length() + 1)) ||
				 !(word.substring(word.length() - endChars.length()).equals(endChars))) {
			 return false;
		 }
		 return true;
	 }
	 
	/* Modify setto() method from Porter Stemmer. */
	private final void setTo(int cut, String add) {
		word = word.substring(0, word.length() - cut) + add;
	}
	
	/* Adopt step 1 from Porter Stemmer with changes */
	private void stepOne() {
		if (endsWith("sses")) { setTo(2, "");
		} else if (endsWith("ies") || endsWith("ied")) { setTo(3, "y");
		} else if (endsWith("eed")) {
			List<String> list = Arrays.asList("freed");
			if (list.contains(word) || m("eed") > 0) { setTo(1, ""); }
		} else if (calcM("ed", 0) || calcM("ing", 0)) {
			List<String> list = Arrays.asList("distributed");
			if (endsWith("ed")) { 
				/* Addition: 
				 * - validation to cater 4 letters or less words 
				 * - e.g. distributed -> distribute */
				if (list.contains(word) || (word.length() <= 4)) {
					setTo(1, "");
				} else { setTo(2, ""); }
			} else if (endsWith("ing")) { setTo(3, ""); }
			
			if (endsWith("at") || endsWith("bl") || endsWith("iz") || (m("") == 1 && cvc(1))) { 
				word = word + "e";
			} else if (endsWithDoubleConsonant(1)) {
				if(word.substring(word.length() - 2, word.length() - 1).equals("l") ||
						word.substring(word.length() - 2, word.length() - 1).equals("s") ||
						word.substring(word.length() - 2, word.length() - 1).equals("z")) {
					/* do nothing */
				} else { setTo(1, ""); }
			}
		} 	
	} /* End of step 1 */
		
	/* Step 2: set last char y with i. 
	 * Caused error for some word e.g. freely become freeli, skip step 2 */
	
	/* Adopt step 3 from Porter Stemmer with changes */
	private void stepThree() {
		if (calcM("tional", 0) || calcM("ally", 0) || calcM("ely",0)) { 
			List<String> list = Arrays.asList("international");
			if(!list.contains(word)) setTo(2, "");
		} else if (calcM("izer", 0)) { setTo(1, "");
		} else if (calcM("bly", 0)) { setTo(1, "e");
		} else if (endsWith("ly")) {
			/* step 3 ently, ously, ely & additional other ly */
			List<String> listCase1 = Arrays.asList("gently");
			List<String> listCase2 = Arrays.asList("freely");
			List<String> listCase3 = Arrays.asList("family");
			if (listCase1.contains(word)) { setTo(1, "e");
			} else if (!listCase3.contains(word) &&
				(calcM("ly", 0) || listCase2.contains(word))) { setTo(2, ""); }
		} else if (calcM("ization", 0)) { setTo(5, "e");
		} else if (calcM("ation", 0) || calcM("ivity", 0)) {
			List<String> list = Arrays.asList("application");
			if (!list.contains(word)) { setTo(3, "e");
			}
		} else if (calcM("ator", 0)) { setTo(2, "e");
		} else if (calcM("alism", 0) || calcM("ality", 0) || calcM("ful", 0)) { setTo(3, "");
		} else if (calcM("ness", 0)) {
			/* step 3 iveness, fulness, ousness
			 * move step 4 ness here because ness suppose to remove first
			 * before ful, e.g. forgetfulness */
			List<String> listCase = Arrays.asList("business");
			if (!listCase.contains(word)) {
				setTo(4, "");
			}
		} else if (calcM("ivity", 0)) { setTo(3, "e");
		} else if (calcM("bility", 0)) { setTo(5, "le");
		} 
	} /* End of step 3 */
	
	/* Adopt step 4 from Porter Stemmer with changes */
	private void stepFour(){
		if (calcM("ical", 0)) { setTo(2, "");
		} else if (calcM("alize", 0) || calcM("icity", 0) || calcM("ful", 0)) { setTo(3, "");
		} else if (calcM("s", 0)) { 
			/* Addition: for s, e.g. files */
			List<String> list = Arrays.asList("always", "regardless", "business", "crisis");
			if (!list.contains(word)) { setTo(1, "");
			} 
		}
	} /* End of step 4 */
	
	/* Adopt step 5 from Porter Stemmer with changes */
	private void stepFive(){
		if (endsWith("er")) { 
			List<String> list = Arrays.asList("encounter", "other", "cover");
			if (!list.contains(word)) {
				setTo(2, "");
				if (endsWith("ag")) { 
					/* Addition: e.g. manager -> manag -> manage */
					word = word + "e"; 
				}
			} 
		} else if (endsWith("ion")) { 
			List<String> listCase1 = Arrays.asList("distribution");
			List<String> listCase2 = Arrays.asList("version", "application");
			if (listCase1.contains(word)) { setTo(3, "e"); 
			} else if(!listCase2.contains(word)) { setTo(3, ""); }
			
		}
	} /* End of step 5 */
	
	/* Skip step 6 */
	
	private void runStemmer() {		
		stepOne();
		stepThree();
		stepFour();
		stepFive();
	}
	
	public String stemResult(String txt) {
		setWord(txt);
		runStemmer();
		return word;
	}
	
	public void porterStemmerDemo(String demo){
		String result = stemResult(demo);
		System.out.println("Before: " + demo + " | After: " + result);
	}
	
	public void runDemo(){
		System.out.println("Demo Porter Stemmer:");
		porterStemmerDemo("princesses");
		porterStemmerDemo("business");
		porterStemmerDemo("fairies");
		porterStemmerDemo("greed");
		porterStemmerDemo("deed");
		porterStemmerDemo("agreed");
		porterStemmerDemo("freed");
		porterStemmerDemo("created");
		porterStemmerDemo("enabling");
		porterStemmerDemo("organized");
		porterStemmerDemo("stopped");
		porterStemmerDemo("buzzing");
		porterStemmerDemo("filled");
		porterStemmerDemo("accessed");
		porterStemmerDemo("loved");
		porterStemmerDemo("boxed");
		porterStemmerDemo("affectional");
		porterStemmerDemo("memorizer");
		porterStemmerDemo("avoidably");
		porterStemmerDemo("actually");
		porterStemmerDemo("gently");
		porterStemmerDemo("freely");
		porterStemmerDemo("urgently");
		porterStemmerDemo("adaptively");
		porterStemmerDemo("continuously");	
		porterStemmerDemo("centralization");
		porterStemmerDemo("nomination");
		porterStemmerDemo("individualism");
		porterStemmerDemo("effectiveness");
		porterStemmerDemo("forgetfulness");
		porterStemmerDemo("awfulness");
		porterStemmerDemo("abnormality");
		porterStemmerDemo("creativity");
		porterStemmerDemo("capability");
		porterStemmerDemo("commercialize");
		porterStemmerDemo("monosyllabicity");
		porterStemmerDemo("alphanumerical");
		porterStemmerDemo("encountered");
		porterStemmerDemo("used");
	}

}
