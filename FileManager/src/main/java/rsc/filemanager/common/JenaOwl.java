package rsc.filemanager.common;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.util.iterator.ExtendedIterator;

import net.sf.extjwnl.data.Synset;

/**
 * 
 * @author Diah S R
 *
 */

public class JenaOwl {
	
	private OntModel model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MICRO_RULE_INF);
//	private OntModel model = ModelFactory.createOntologyModel(OntModelSpec.RDFS_MEM);
	private String inputFileName;
	private String rdf;
	private String owl;
	private String xsd;
	private String rdfs;
	private String uri;
	private InputStream input;
	
	public JenaOwl() {
		
	}
	
	public JenaOwl(String file, String currentFileUri) {
		this.setFile(file);
		inputStream(this.getFile());
		model.read(input, null);
//		model.read(file, currentFileUri);
		
		this.setRdf("http://www.w3.org/1999/02/22-rdf-syntax-ns");
		this.setOwl("http://www.w3.org/2002/07/owl");
		this.setXsd("http://www.w3.org/2001/XMLSchema");
		this.setRdfs("http://www.w3.org/2000/01/rdf-schema");
		this.setUri(currentFileUri);
	}
	
	public String getFile() {
		return inputFileName;
	}

	public void setFile(String inputFileName) {
		this.inputFileName = inputFileName;
	}

	public String getRdf() {
		return rdf;
	}

	public void setRdf(String rdf) {
		this.rdf = rdf;
	}

	public String getOwl() {
		return owl;
	}

	public void setOwl(String owl) {
		this.owl = owl;
	}

	public String getXsd() {
		return xsd;
	}

	public void setXsd(String xsd) {
		this.xsd = xsd;
	}

	public String getRdfs() {
		return rdfs;
	}

	public void setRdfs(String rdfs) {
		this.rdfs = rdfs;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public void inputStream(String file) {
		input = FileManager.get().open(file);
		if (input == null) {
		    throw new IllegalArgumentException(
		         "File: " + file + " not found");
		}
	}

	public void runQuery(String queryString) {
		Query query = QueryFactory.create(queryString);
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet rs =  qe.execSelect();
		System.out.println("Row number: " + rs.getRowNumber());
		ResultSetFormatter.out(System.out, rs, query);
		qe.close();
	}
	
	public void printLabel(String queryString){
		Query query = QueryFactory.create(queryString);
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet rs =  qe.execSelect();
		for ( ; rs.hasNext() ; ) {
			final QuerySolution qs  = rs.nextSolution();
			System.out.println( qs.getLiteral("label"));
		}
		qe.close();
	}
	
	/*Retrieve list of individual(s) belongs to another individual based on selected property */
	public Set<String> listIndividualByProperty(String individual, String property){
		String queryString = 
			"PREFIX rdfs: <" + rdfs + "#> " +
			"PREFIX : <" + uri + "#> " +
			"SELECT DISTINCT ?rsLabel " +
			"WHERE { " + 
				"?in :" + property + " ?rs. " +
				"?rs rdfs:label ?rsLabel. " +
				"?in rdfs:label ?inLabel. " +
				"?in rdfs:label \"" + individual + "\"@en " +
			"}";
		
		/* Query execution */
		Query query = QueryFactory.create(queryString);
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet rs =  qe.execSelect();
		
		/* Prepare variable to store result */
		String strResult = "";
		Set<String> setResult = new HashSet<String>();
		
		/* Retrieving result(s) */
		for (; rs.hasNext(); ) {
			
			QuerySolution qs  = rs.nextSolution();
			
			strResult = qs.getLiteral("rsLabel").toString().substring(0, 
							qs.getLiteral("rsLabel").toString().length()-3);
			setResult.add(strResult);			
		}
		
		qe.close();
		return setResult;
	}
	
	public Set<String> searchIndividualLikely(String individual){
		String queryString = 
			"PREFIX rdfs: <" + rdfs + "#> " +
			"PREFIX : <" + uri + "#> " +
			"SELECT ?inLabel " +
			"WHERE { " + 
				"?in rdfs:label ?inLabel. " +
				"FILTER regex(?inLabel, \"" + individual + "\", \"i\")" +
			"}";
		
		/* Query execution */
		Query query = QueryFactory.create(queryString);
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet rs =  qe.execSelect();
		
		/* Prepare variable to store result */
		String strResult = "";
		Set<String> setResult = new HashSet<String>();
		
		/* Retrieving result(s) */
		for (; rs.hasNext(); ) {
			
			QuerySolution qs  = rs.nextSolution();
			
			strResult = qs.getLiteral("inLabel").toString().substring(0, 
							qs.getLiteral("inLabel").toString().length()-3);
			setResult.add(strResult);		
		}
		
		qe.close();
		return setResult;
	}
	
	public boolean findIndividualLikely(String individual){
		Set<String> setResult = new HashSet<String>();
		setResult = this.searchIndividualLikely(individual);
		if(setResult.isEmpty()) return false;
		return true;
	}
	
	public Set<String[]> listAllLevel(String in){
		
		Set<String[]> arrayLevel = new HashSet<String[]>();
		Set<String> setIn = this.searchIndividualLikely(in);

		for(String l: setIn){
			String[] termArray = new String[1];
			termArray[0] = l;
			arrayLevel.add(termArray);			
		}
		
		Set<String[]> level = this.level(arrayLevel, "hasSuperior");
		level = this.level(level, "hasSubordinate");
		
		return level;
	}
	
	public Set<String[]> level(Set<String[]> strList, String property){
		
		Set<String> rsSet = new HashSet<String>();
		Set<String[]> levelSet = new HashSet<String[]>();
		
		for(String[] stArr: strList){
			String[] tempArr = stArr;
			
			/* Get related individual */
			if(property.equals("hasSuperior")){
				rsSet = this.listIndividualByProperty(stArr[0], property);
				/* Check Result */
				if(rsSet.size() >= 1){
					
					for(String l: rsSet){
						String[] level = new String[tempArr.length + 1];
						
						for(int i = 0; i < level.length; i++){
							if(i == 0){
								level[i] = l.toLowerCase();
							} else {
								level[i] = tempArr[i-1];
							}
						}
						
						/* Add array into level array */
						levelSet.add(level);
					}
					
					
				} 				
			} else if(property.equals("hasSubordinate")){
				rsSet = this.listIndividualByProperty(stArr[stArr.length-1], property);
				/* Check Result */
				if(rsSet.size() >= 1){
					
					for(String l: rsSet){
						String[] level = new String[tempArr.length + 1];
						
						for(int i = 0; i < level.length; i++){
							if(i == level.length - 1){
								level[i] = l.toLowerCase();
							} else {
								level[i] = tempArr[i];
							}
						}
						
						/* Add array into level array */
						levelSet.add(level);
					}
						
				} 
			}
		}
		
		if(!levelSet.isEmpty()){
			strList.removeAll(strList);
			for(String[] arrLevel: levelSet){
				Set<String[]> tempLevel = new HashSet<String[]>();
				tempLevel.add(arrLevel);
				tempLevel = level(tempLevel, property);
				strList.addAll(tempLevel);
			}
		}
		
		return strList;
	}
	
	public void printURI(String queryString){
		Query query = QueryFactory.create(queryString);
		QueryExecution qe = QueryExecutionFactory.create(query, model);
		ResultSet rs =  qe.execSelect();
		for ( ; rs.hasNext() ; ) {
			final QuerySolution qs  = rs.nextSolution();
			System.out.println( qs.getResource("s").getURI());
		}
		qe.close();
	}
	
	public void individualSize(){
        int size = 0;

        ExtendedIterator<Individual> individuals = model.listIndividuals();

        while (individuals.hasNext()) {
        	size++;
        	individuals.next();
        }

        System.out.println("Individual size = " + size);
	}
	
	public void listInstances(String uri, String theClass){
		// Get the class.
        final OntClass thing = model.getOntClass(uri + "#" + theClass);

        // Print each of its instances.
        for ( final ExtendedIterator<? extends OntResource> things = thing.listInstances(); things.hasNext(); ) {
            System.out.println( things.next() );
        }
	}
	
	@SuppressWarnings("rawtypes")
	public void listClassesInstancesWithWhile() {
		// Listing all classes and instances in a Jena ontology model
		
		ExtendedIterator classes = model.listClasses();
		
	    while (classes.hasNext()) {
	    	
	    	OntClass thisClass = (OntClass) classes.next();
	    	System.out.println("Found class: " + thisClass.toString());
	    	ExtendedIterator instances = thisClass.listInstances();
	    	
	    	while (instances.hasNext()) {
	    		
	    		Individual thisInstance = (Individual) instances.next();
	    		System.out.println("  Found instance: " + thisInstance.toString());
	      }
	    }
	}
	
	public void getTypeByIndividual(String uri, String indiv) {
		String RDF = model.getNsPrefixURI("rdf");
		Property type = model.getProperty(RDF, "type");
		Individual individual = model.getIndividual(uri + "#" + indiv);
		System.out.println(individual.getProperty(type).getClass());
	}
	
	
	@SuppressWarnings("rawtypes")
	public void getPropertyValue() {
		ArrayList<Resource> results = new ArrayList<Resource>();            
        ExtendedIterator  individuals = model.listIndividuals();
        
        while (individuals.hasNext()) {
            Resource individual = (Resource) individuals.next();
            results.add(individual);
        }
        System.out.println();
        
        for (int i = 0; i < results.size(); i++) {
        	Individual ind = model.getIndividual(results.get(i).toString());
        	
            // Check whether ind is null.
            System.out.println( "individual: " + ind );
            
            // Iterate over all the statements that have ind as subject.
            StmtIterator statementIterator = ind.listProperties();
            
//            System.out.println( statementIterator.next() );
            
            while ( statementIterator.hasNext() ) {
            	Statement statement = (Statement) statementIterator.next();
            	
            	if (statement.getObject().isLiteral()) {
            		System.out.println(statement.getLiteral().getLexicalForm().toString() + 
            				" type = " + statement.getPredicate().getLocalName());
            	} else {
//            		System.out.println(statement);
            		System.out.println(statement.getObject().toString() + 
            				" type = " + statement.getPredicate().getLocalName());
            	}
            }
        }
	}

}
