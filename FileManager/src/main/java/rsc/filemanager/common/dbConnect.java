package rsc.filemanager.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Modified from source:
 * - JDBC example: https://www.tutorialspoint.com/jdbc/
 *
 */

public class dbConnect {

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_DRIVER = "jdbc:mysql:";
	static final String DB_HOST = "localhost";
	static final String DB_NAME = "file_manager";
	static final String DB_URL = DB_DRIVER + "//" + DB_HOST + "/" + DB_NAME;
	
	// Database credentials
	static final String USER = "root";
	static final String PASS = "password";
	
	private Connection openConnection(){
		Connection conn = null;
		
		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");
			
			// Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return conn;
	}
	
	private void closeConnection(Connection conn){
		try {
			if(conn!=null) conn.close();
			System.out.println("Database connection closed...");
	      
		} catch(SQLException se) {
			se.printStackTrace();
	      
		}
	}
	
	public boolean runUpdate(String query){
		/*Getting connection*/
		Connection conn = this.openConnection();
		Statement stmt = null;
		
		label: try {
			if(conn==null) break label;
			stmt = conn.createStatement();
			stmt.executeUpdate(query);	 
			return true;
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(stmt!=null) stmt.close();
			} catch(SQLException se) {
				// Do nothing
			}
			this.closeConnection(conn);
		}
		return false;
	}
	
	public List<Map<String, String>> runRead(String query){
		/*Getting connection*/
		Connection conn = this.openConnection();
		ResultSet rs = null;
		Statement stmt = null;
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		
		label: try {
			if(conn==null) break label;
			stmt = conn.createStatement(); 
			rs = stmt.executeQuery(query);
			int column = rs.getMetaData().getColumnCount();
			Map<String, String> map = new HashMap<String, String>();
			while(rs.next()){
				for(int i=1; i<column; i++) {
					String columnName = rs.getMetaData().getColumnName(i);
					String columnValue = rs.getString(i);
					System.out.println(columnName + ": " + columnValue);
					map.put(columnName, columnValue);
				}
				result.add(map);
		    }
			rs.close();
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if(stmt!=null) stmt.close();
			} catch(SQLException se) {
				// Do nothing
			}
			this.closeConnection(conn);
		}
		return result;
	}
	
	public boolean alreadyExists(String query) throws SQLException{
		List<Map<String, String>> rs = this.runRead(query);
		System.out.println(rs.size() + " row(s) selected.");
		if(rs.size() > 0) return true;
	    return false;
	}
	
	public void testConnection(){
		Connection conn = null;
		Statement stmt = null;
		   
		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			// Open a connection
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// Execute a query
			System.out.println("Read database...");
			stmt = conn.createStatement();
		      
			String sql = "select * from users";
			ResultSet rs = stmt.executeQuery(sql);
			System.out.println("Database read successfully...");
		   
			// Extract data from result set
			while(rs.next()){

				//Retrieve by column name
				String username = rs.getString("username");
				String status = rs.getString("activation_status");

				//Display values
				System.out.println("Username: " + username + ", status: " + status);
		      }
		      rs.close();
			
		} catch(SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		   
		} catch(Exception e) {
		      // Handle errors for Class.forName
		      e.printStackTrace();
		   
		} finally {
		      // Finally block used to close resources
		      
			try {
				if(stmt!=null)
					stmt.close();
		      
			} catch(SQLException se2) {
				// Do nothing
			}
		      
			try {
				if(conn!=null)
					conn.close();
		      
			} catch(SQLException se) {
				se.printStackTrace();
		      
			}
		   
		}
		
		System.out.println("Goodbye!");
	}

}
