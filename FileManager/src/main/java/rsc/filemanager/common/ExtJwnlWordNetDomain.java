package rsc.filemanager.common;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import net.sf.extjwnl.data.Synset;
import rsc.filemanager.model.WordNetDomain;
import rsc.filemanager.service.WordNetDomainService;

/**
 * 
 * @author Diah S R
 *
 */

public class ExtJwnlWordNetDomain {

	@Autowired
	WordNetDomainService wordNetDomainService;
	
	private ExtJwnl extJwnl = new ExtJwnl();
	
	public void insertDomain(){
		Set<Synset> senses = new HashSet<>();
		Map<Synset, Integer> domainCount = new HashMap<>();
		
		/* Get all synset from WordNet */
		senses = extJwnl.getAllSynset();
		
		/* Get all synset's and its gloss domain */
		for (Synset sense: senses){
			Map<Synset, Integer> tempDomainCount = new HashMap<>();
			tempDomainCount.putAll(extJwnl.getSenseGlossDomain(sense));
			for (Map.Entry<Synset, Integer> domain : tempDomainCount.entrySet()) {
				System.out.println(domain.getKey().toString());
				System.out.println(domain.getValue());
				if (domainCount.containsKey(domain.getKey())) {
					domainCount.put(domain.getKey(), domainCount.get(domain.getKey()) + domain.getValue());
				} else {
					domainCount.put(domain.getKey(), domain.getValue());
				}
			}
		}
		
		for (Map.Entry<Synset, Integer> domain : domainCount.entrySet()) {
			WordNetDomain wordNetDomain = new WordNetDomain();
			wordNetDomain.setDomainOffset((int) (long) domain.getKey().getOffset());
			wordNetDomain.setDomainPos(domain.getKey().getPOS().getKey());
			wordNetDomain.setDomainMembership((int) (long) domain.getValue());
			wordNetDomainService.saveDomain(wordNetDomain);
		}
		
	}
	
}
