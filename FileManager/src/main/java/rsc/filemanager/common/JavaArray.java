package rsc.filemanager.common;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Diah S R
 *
 */

public class JavaArray {
	
	/* Method to get unique ascending array */
	public String[] uniqueArrayAsc(String[] words){
		Set<String> uniqueList = new HashSet<String>(Arrays.asList(words));
		Set<String> temp = new HashSet<String>();
		for(String s: uniqueList) if(s.equals("") || s.equals(null) || s.isEmpty()) temp.add(s); 
		uniqueList.removeAll(temp);
		words = new String[uniqueList.size()];
		words = uniqueList.toArray(words);
		Arrays.sort(words);
		return words;
	}
	
	/* Method to get selective second index of multidimensional array */
	public String[] getBySecondIndex(String[][] arr, int index){
		String[] result = new String[arr.length];
		for (int i = 0; i < arr.length; i++) {
			result[i] = arr[i][index];
		}
		return result;
	}
	
	/* Method to concatenate two array
	 * Source: https://community.oracle.com/thread/2103771?start=0&tstart=0 */
	public String[] concatArray(String[] arrayA, String[] arrayB) {
		int aLength = arrayA.length;
		int bLength = arrayB.length;
		String[] result= new String[aLength + bLength];
		System.arraycopy(arrayA, 0, result, 0, aLength);
		System.arraycopy(arrayB, 0, result, aLength, bLength);
		result = uniqueArrayAsc(result); /* added to get unique concatenate result */
		return result;
	}
	
	/* Method to sort two dimensional array */
	public String[][] sortTwoDimension(String[][] words){
		Arrays.sort(words, new Comparator<String[]>() {
	        @Override
	        public int compare(final String[] entry1, final String[] entry2) {
	            final String word1 = entry1[0];
	            final String word2 = entry2[0];
	            return word1.compareTo(word2);
	        }
        });
		return words;
	}

}
