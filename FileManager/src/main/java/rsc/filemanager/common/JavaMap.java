package rsc.filemanager.common;

/**
 * 
 * @author Diah S R
 *
 */

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class JavaMap {
	
	/* Method to map string as index from array
	 * Source: http://docs.oracle.com/javase/6/docs/api/java/util/Map.html */
	public Map<String, Double> mapTermArray(String[][] term){
		Map<String, Double> map = new HashMap<>();
		
		for (int i = 0; i < term.length; i++) {
			map.put(term[i][0], Double.valueOf(term[i][1]));
		}
		
		return map;
	}
	
	/* Method to calculate addition result of all numeric value inside map */
	public double mapAddAll(Map<String, Double> map){
		double result = 0;
		for (Map.Entry<String, Double> m : map.entrySet()) {
			result = result + m.getValue();
		}
		return result;
	}
	
	/* Method to calculate dot product result of two maps with same indexes */
	public Map<String, Double> mapsDotProduct(Map<String, Double> mapA, Map<String, Double> mapB){
		Map<String, Double> dotProduct = new HashMap<>();
		for (Map.Entry<String, Double> m : mapA.entrySet()) {
			System.out.println(m.getKey() + ": " + m.getValue() + " * " + mapB.get(m.getKey()));
			if(mapB.get(m.getKey())!=0) dotProduct.put(m.getKey(), m.getValue() * mapB.get(m.getKey()));
		}
		return dotProduct;
	}
	
	/* Method to print map, sort by ascending */
	public void printMap(Map<String, Double> map){
		Map<String, Double> treeMap = new TreeMap<String, Double>(map);
		for (Map.Entry<String, Double> m : treeMap.entrySet()) {
			System.out.println(m.getKey() + ": " + m.getValue());
	    }
	}
	
}
