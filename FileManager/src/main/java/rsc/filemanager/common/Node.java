package rsc.filemanager.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Source: http://programtalk.com/java/java-tree-implementation/
 *
 * @param <T>
 */
 
public class Node<T> {
	private T nodename;
	private final List<Node<T>> children = new ArrayList<>();
	private final Node<T> parent;
 
 	public Node(Node<T> parent) {
 		this.parent = parent;
 	}
 
 	public T getNodeName() {
 		return nodename;
 	}
 
 	public void setNodeName(T nodename) {
 		this.nodename = nodename;
 	}
 
 	public List<Node<T>> getChildren() {
 		return children;
 	}
 
 	public Node<T> getParent() {
 		return parent;
 	}
 	
 	public Node<T> addChild(Node<T> parent, T child) { 
 		Node<T> node = new Node<T>(parent);
 		node.setNodeName(child);
 		parent.getChildren().add(node);
 		return node;
 	}
 	
 	public Node<T> addChildren(Node<T> root, T parent, T child) { 
 		Node<T> nodeParent = this.findNode(root, parent);
 		Node<T> nodeChildren = new Node<T>(nodeParent);
 		nodeChildren.setNodeName(child);
 		nodeParent.getChildren().add(nodeChildren);
 		return nodeChildren;
 	}
 	
 	public void printTree(Node<T> node, String appender) {
 	   System.out.println(appender + node.getNodeName());
 	   for (Node<T> each : node.getChildren()) {
 	       printTree(each, appender + "\t");
 	   }
 	}
 	
 	/* Additional method to find node by input */
	public Node<T> findNode(Node<T> node, T input) {
		Node<T> result = new Node<T>(null);
		if(node.getNodeName().equals(input)){
			result = node;
		} else {
			for (Node<T> n : node.getChildren()) {
				result = findNode(n, input);
	 			if(n.getNodeName().toString().equals(input.toString())){
//	 				System.out.println(n.getNodeName() + " found.");
	 				result = n;
	 			}
	 			if (result.getNodeName() != null){
	 				break;
	 			}
	 		}
		}
 		return result;
 	}
	
	/* Additional method */
	public List<Node<T>> findHypernymPath(List<Node<T>> path, Node<T> node) {
		path.add(path.size(), node);
		if(node.getParent() != null) {
			path = this.findHypernymPath(path, node.getParent());
		}
		return path;
 	}
	
	/* Additional method */
	public Map<T, Integer> mapHypernymPathLevel(Node<T> node) {
		Map<T, Integer> mapPath = new HashMap<>();
		List<Node<T>> listPath = new ArrayList<>();
		listPath = this.findHypernymPath(listPath, node);
		int level = 0;
		for (int i = listPath.size() - 1; i >= 0; i--) {
			mapPath.put(listPath.get(i).getNodeName(), level);
			level++;
		}
		return mapPath;
 	}
	
	/* Additional method to find node by input */
	@SuppressWarnings("unused")
	public boolean isNode(Node<T> node, T input) {
		Node<T> result = this.findNode(node, input);
		System.out.println("Find result: " + result.getNodeName());
		if (result == null) {
			System.out.println(result.getNodeName() + " false.");
 			return false;
 		} else {
 			System.out.println(result.getNodeName() + " true.");
 			return true;
 		}
 	}
	
	/* Additional method to find whether the input is descendant node of the respective ancestor */
	public boolean isDecendant(Node<T> node, T ancestor, T descendant) {
		boolean result = false;
		Node<T> a = this.findNode(node, ancestor);
 		for (Node<T> n : a.getChildren()) {
 			result = isDecendant(n, n.getNodeName(), descendant);
// 			System.out.println("Result of ancestor " + n.getNodeName() + " and descendant " + descendant + " is " + result);
 			if(n.getNodeName().toString().equals(descendant.toString())){
// 				System.out.println(n.getNodeName() + " is descendant.");
 				result = true;
 			}
 			if(result){
 				break;
 			}
 		}
 		return result;
 	}
	
	/* Additional method */
	public boolean isInTheSamePath(Node<T> node, T a, T b){
		boolean result = false;
		if (isDecendant(node, a, b) || isDecendant(node, b, a)){
			result = true;
		}
		return result;
	}
	
	/* Additional method */
	public boolean isChild(Node<T> node, T parent, T child) {
		boolean result = false;
		Node<T> nodeParent = this.findNode(node, parent);
		List<Node<T>> listChildNode= nodeParent.getChildren();
//		System.out.print(parent + " as " + nodeParent.getNodeName() + ": ");
		for (Node<T> cn: listChildNode) {
//			System.out.print(cn.getNodeName() + " ");
			if(cn.getNodeName().toString().equals(child.toString())){
				result = true;
			}
		}
//		System.out.println();
		return result;
 	}
	
	/* Additional method to find whether the input is descendant node of the respective ancestor */
	public Integer countDecendant(Node<T> node, T ancestor, T descendant) {
		int result = 0;
		Node<T> a = this.findNode(node, ancestor);
 		for (Node<T> n : a.getChildren()) {
 			result = result + countDecendant(n, n.getNodeName(), descendant);
 			if(n.getNodeName().toString().equals(descendant.toString())){
 				result = result + 1;
 			}
 		}
 		return result;
 	}

}