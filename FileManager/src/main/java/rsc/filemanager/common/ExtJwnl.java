package rsc.filemanager.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import net.sf.extjwnl.JWNLException;
import net.sf.extjwnl.data.IndexWord;
import net.sf.extjwnl.data.POS;
import net.sf.extjwnl.data.Pointer;
import net.sf.extjwnl.data.PointerType;
import net.sf.extjwnl.data.PointerUtils;
import net.sf.extjwnl.data.Synset;
import net.sf.extjwnl.data.Word;
import net.sf.extjwnl.data.list.PointerTargetNode;
import net.sf.extjwnl.data.list.PointerTargetNodeList;
import net.sf.extjwnl.data.list.PointerTargetTree;
import net.sf.extjwnl.data.relationship.AsymmetricRelationship;
import net.sf.extjwnl.data.relationship.Relationship;
import net.sf.extjwnl.data.relationship.RelationshipFinder;
import net.sf.extjwnl.data.relationship.RelationshipList;
import net.sf.extjwnl.dictionary.Dictionary;

/**
 * 
 * @author Diah S R
 *
 */

public class ExtJwnl {
	
	private Dictionary dictionary;
	private IrPreprocessing irpreprocessing = new IrPreprocessing();
	private JavaArray javaArray = new JavaArray();
	@SuppressWarnings("rawtypes")
	private JavaList javalist = new JavaList();
	
	public ExtJwnl() {
		try {
			dictionary = Dictionary.getDefaultResourceInstance();
		} catch (JWNLException e) {
			e.printStackTrace();
		}
	}
	
	/* Method to check whether the given word available or not in dictionary
	 * based on part-of-speech key
	 * 
	 * Notes
	 * - 4 possible part-of-speech classes: 
	 *   1. POS.ADJECTIVE (a)
	 *   2. POS.ADVERB (r)
	 *   3. POS.NOUN (n)
	 *   4. POS.VERB (v) */
	public IndexWord checkWordByKey(char key, String input) {
		IndexWord indexWord = null;
		try {
			switch (key) {
				case 'a': indexWord = dictionary.lookupIndexWord(POS.ADJECTIVE, input); break;
				case 'r': indexWord = dictionary.lookupIndexWord(POS.ADVERB, input); break;
				case 'n': indexWord = dictionary.lookupIndexWord(POS.NOUN, input); break;
				case 'v': indexWord = dictionary.lookupIndexWord(POS.VERB, input); break;
				default: break;
			}
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return indexWord;
	}
	
	/* Method to check whether the given word available or not in dictionary (adjective) */
	public IndexWord checkWordAdjective(String input) {
		IndexWord indexWord = this.checkWordByKey('a', input);
		return indexWord;
	}
	
	/* Method to check whether the given word available or not in dictionary (adverb) */
	public IndexWord checkWordAdverb(String input) {
		IndexWord indexWord = this.checkWordByKey('r', input);
		return indexWord;
	}
	
	/* Method to check whether the given word available or not in dictionary (noun) */
	public IndexWord checkWordNoun(String input) {
		IndexWord indexWord = this.checkWordByKey('n', input);
		return indexWord;
	}
	
	/* Method to check whether the given word available or not in dictionary (noun) */
	public IndexWord checkWordVerb(String input) {
		IndexWord indexWord = this.checkWordByKey('v', input);
		return indexWord;
	}
	
	/* Method to list the given word's index if any */
	public Set<IndexWord> listIndexWord(String input){
		Set<IndexWord> indexWords = new HashSet<IndexWord>();
		IndexWord indexWord;
		
		/* Check on Adjective */
		indexWord = this.checkWordAdjective(input);
		if (indexWord != null) { indexWords.add(indexWord); }
		
		/* Check on Adverb */
		indexWord = this.checkWordAdverb(input);
		if (indexWord != null) { indexWords.add(indexWord); }
		
		/* Check on Noun */
		indexWord = this.checkWordNoun(input);
		if (indexWord != null) { indexWords.add(indexWord); }
		
		/* Check on Verb */
		indexWord = this.checkWordVerb(input);
		if (indexWord != null) { indexWords.add(indexWord); }
		
		return indexWords;
	}
	
	public Set<String> validateAnnotation(Map<String,String> input, Set<String> keywords, String pdfContent){
		Map<Synset, Set<Synset>> mapRelation = new HashMap<Synset, Set<Synset>>();
		Set<Synset> synset = new HashSet<Synset>();
		Set<Synset> key = new HashSet<Synset>();
		Set<Synset> wordnetText = new HashSet<Synset>();
		Set<Synset> dbPedia = new HashSet<Synset>();
		Set<String> matchedAll = new HashSet<String>();
		int wordnetNum = 0;
		
		for(Map.Entry<String,String> map: input.entrySet()) {
			String strTemp = map.getKey().substring(8);
			if(!map.getKey().substring(0, 7).contains("wordnet")) continue;
			Set<Synset> temp = new HashSet<Synset>();
			temp.addAll(this.getSynsetByString(strTemp));
			for(Synset sense: temp)
				if(sense.getGloss().contains(map.getValue())) {
					Set<Synset> tempSynset = new HashSet<Synset>();
					Set<Synset> tempRelation = this.getSenseDomain(sense);
					tempRelation.addAll(this.getTwoLevelAboveHypernymSynset(sense.getPOS().getKey().charAt(0), Long.toString(sense.getOffset())));
					synset.add(sense);
					tempSynset.add(sense);
					tempSynset.addAll(tempRelation);
					mapRelation.put(sense, tempRelation);
					wordnetNum++;
					/* Get list of keyword's senses, domain and hypernym */
					for(String k: keywords) if(k.contains(strTemp)) key.addAll(tempSynset);
					String[] split = strTemp.split(" ");
					for(int i=0;i<split.length;i++)
						if(pdfContent.contains(split[i]) && !wordnetText.contains(strTemp)) { 
							wordnetText.addAll(tempSynset);
							break;
						}
					if(input.get("dbpedia:"+strTemp)!=null && !input.get("dbpedia:"+strTemp).isEmpty() 
						&& !dbPedia.contains(strTemp)) dbPedia.addAll(tempSynset);
				}
		}
		
		for(Synset s: key) System.out.println("Key:" + s.toString());
		Set<Synset> relatedWithKey = new HashSet<Synset>();
		for(Map.Entry<Synset,Set<Synset>> map: mapRelation.entrySet()) {
			System.out.println("Map senses:" + map.getKey().toString());
			Set<Synset> temp = new HashSet<Synset>();
			Set<Synset> tempRelation = new HashSet<Synset>();
			for(Synset s: map.getValue()) {
				if(key.contains(s)) temp.add(s);
				if(synset.contains(s)) tempRelation.add(s);
				System.out.println("Map related senses:" + s.toString());
			}
			if(!key.contains(map.getKey()) && temp.size()<1) continue;
			relatedWithKey.add(map.getKey());
			if(tempRelation.size()>0) relatedWithKey.addAll(tempRelation); 
		}
		
		int relatedNum = 0;
		for(Synset s: relatedWithKey) {
			relatedNum = relatedNum + s.getWords().size();
			matchedAll.addAll(this.getLemmaToString(s));
			System.out.println("Related to keywords:" + s.toString());
		}
		if(wordnetNum>0) 
			System.out.println("Number of wordnet annotation related to keywords: " + 
			relatedNum + " of " + wordnetNum + " (" + ((double)relatedNum/(double)wordnetNum)*100 + "%)");
		else
			System.out.println("Number of wordnet annotation related to keywords: " + 
					relatedNum + " of " + wordnetNum);
					
		/* Count number of wordnet annotation related to document's text */
//		wordnetText.retainAll(synset);
//		relatedNum = 0;
//		for(Synset s: wordnetText) {
//			relatedNum = relatedNum + s.getWords().size();
//			matchedAll.addAll(this.getLemmaToString(s));
//			System.out.println("Related to text:" + s.toString());
//		}
//		if(wordnetNum>0)
//			System.out.println("Number of wordnet annotation related to document's text: " + 
//			relatedNum + " of " + wordnetNum + " (" + ((double)relatedNum/(double)wordnetNum)*100 + "%)");
//		else
//			System.out.println("Number of wordnet annotation related to document's text: 0 of 0");
		
		/* Count number of wordnet annotation that matched dbPedia result */
		dbPedia.retainAll(synset);
		relatedNum = 0;
		for(Synset s: dbPedia) {
			relatedNum = relatedNum + s.getWords().size();
			matchedAll.addAll(this.getLemmaToString(s));
			System.out.println("Related to dbpedia:" + s.toString());
		}
		if(wordnetNum>0)
			System.out.println("Number of wordnet annotation related to dbpedia result: " + 
					relatedNum + " of " + wordnetNum + " (" + ((double)relatedNum/(double)wordnetNum)*100 + "%)");
		else
			System.out.println("Number of wordnet annotation related to dbpedia result: 0 of 0.");
		
		/* Count number of wordnet annotation that matched all criteria */
		if(wordnetNum>0)
			System.out.println("Number of wordnet annotation that matched minimum one criteria: " + 
					matchedAll.size() + " of " + wordnetNum + " (" + ((double)matchedAll.size()/(double)wordnetNum)*100 + "%)");
		else
			System.out.println("Number of wordnet annotation that matched minimum one criteria: 0 of 0.");
		
		return matchedAll;
	}
	
	public Set<Synset> getSynsetByString(String input){
		Set<Synset> result = new HashSet<Synset>();
		Set<IndexWord> indexword = this.listIndexWord(input);
		
		for(IndexWord i: indexword) result.addAll(i.getSenses());
		
		return result;
	}
	
	public List<IndexWord> listIndexMultipleWord(List<String> input){
		List<IndexWord> indexWords = new ArrayList<IndexWord>();
		IndexWord indexWord;
		
		for (String in: input) {
			/* Check on Adjective */
			indexWord = this.checkWordAdjective(in);
			if (indexWord != null) { indexWords.add(indexWord); }
			
			/* Check on Adverb */
			indexWord = this.checkWordAdverb(in);
			if (indexWord != null) { indexWords.add(indexWord); }
			
			/* Check on Noun */
			indexWord = this.checkWordNoun(in);
			if (indexWord != null) { indexWords.add(indexWord); }
			
			/* Check on Verb */
			indexWord = this.checkWordVerb(in);
			if (indexWord != null) { indexWords.add(indexWord); }
		}
		
		return indexWords;
	}
	
	/* Method to handle multiple words input preprocessing */
	public List<String> inputPrep(String input) {
		String[] arrayInput = irpreprocessing.stopwordsRemoval((irpreprocessing.tokenization(input)));
		Arrays.sort(arrayInput);
		arrayInput = javaArray.uniqueArrayAsc(arrayInput);
		List<String> listInput = Arrays.asList(arrayInput);
		for (int i = 0; i < listInput.size(); i++) {
			listInput.set(i, irpreprocessing.porterStemmer(listInput.get(i)));
		}
		return listInput;
	}
	
	/* Method to get synset by its offset */
	public Synset getSynsetByOffset(POS pos, long offset) {
		Synset synset = null;
		try {
			synset = dictionary.getSynsetAt(pos, offset);
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return synset;
	}
	
	public Synset getSynsetByOffset(char pos, String offset) {
		Synset synset = null;
		try {
			switch (pos) {
				case 'a': synset = dictionary.getSynsetAt(POS.ADJECTIVE, Long.parseLong(offset)); break;
				case 'r': synset = dictionary.getSynsetAt(POS.ADVERB, Long.parseLong(offset)); break;
				case 'n': synset = dictionary.getSynsetAt(POS.NOUN, Long.parseLong(offset)); break;
				case 'v': synset = dictionary.getSynsetAt(POS.VERB, Long.parseLong(offset)); break;
				default: break;
			}
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return synset;
	}

	/* Method to get all synset */
	public Set<Synset> getAllSynsetByPosKey(char key) {	
		Set<Synset> senses = new HashSet<>();
		Iterator<IndexWord> indexWords = null;
		try {
			switch (key) {
				case 'a': indexWords = dictionary.getIndexWordIterator(POS.ADJECTIVE); break;
				case 'r': indexWords = dictionary.getIndexWordIterator(POS.ADVERB); break;
				case 'n': indexWords = dictionary.getIndexWordIterator(POS.NOUN); break;
				case 'v': indexWords = dictionary.getIndexWordIterator(POS.VERB); break;
				default: break;
			}
			while (indexWords.hasNext()){
				IndexWord indexWord = indexWords.next();
				senses.addAll(indexWord.getSenses());
				for (Synset sense : senses) {
//					System.out.println(indexWord + " " + sense.toString());
//					System.out.println(sense.toString());
				}
			}
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return senses;
	}
	
	/* Method to get all synset */
	public Set<Synset> getAllSynset(){	
		Set<Synset> senses = new HashSet<>();
		senses.addAll(this.getAllSynsetByPosKey('a'));
		senses.addAll(this.getAllSynsetByPosKey('r'));
		senses.addAll(this.getAllSynsetByPosKey('n'));
		senses.addAll(this.getAllSynsetByPosKey('v'));
		return senses;
	}
	
	/* Test the library's method */
 	public void testSynset(String input) {		
		Set<IndexWord> indexWords = this.listIndexWord(input);
		
		for (IndexWord indexWord : indexWords) {
			if (indexWord != null) {
				/* List different senses */
				List<Synset> senses = indexWord.getSenses();
		
				for (Synset sense : senses) {
					/* Get short description of the sense, called the gloss */
					System.out.println(
						indexWord + " " + sense.getOffset() + " : " + 
						sense.getGloss()
					);
					
					/* List other lemma in the synset */
					List<Word> words = sense.getWords();
					
					for (Word word : words) {
						/* Get its lemma and its POS for each word */
						System.out.println(
							"\t POS: " + word.getPOS().getKey() + " | " +
							"Lemma: " + word.getLemma()
						);
					}
				}
			}
		}
		
	}
 	
 	public Set<String> getLemmaToString(Synset sense){
 		Set<String> result = new TreeSet<>();
 		
		/* List other lemma in the synset */
 		List<Word> words = sense.getWords();
		for (Word word : words) {
			/* Get its lemma and its POS for each word */
//			System.out.println(
//				"\t POS: " + word.getPOS().getKey() + " | " +
//				"Lemma: " + word.getLemma()
//			);
			result.add(word.getLemma());
			
		}
		return result;
 	}
 	
 	public String getLemmaString(Synset sense){
 		String result = "";
 		
		/* List other lemma in the synset */
 		List<Word> words = sense.getWords();
		for (Word word : words) {
			/* Get its lemma and its POS for each word */
//			System.out.println(
//				"\t POS: " + word.getPOS().getKey() + " | " +
//				"Lemma: " + word.getLemma()
//			);
			result = result + "#" + word.getLemma();
			
		}
		return result;
 	}
 	
 	/* Get hypernym on tree structure */	
	public Node<Long> getHypernymAsTree(Synset sense, Node<Long> treeRootNodeLong){
		try {
			PointerTargetTree hypernyms = PointerUtils.getHypernymTree(sense);
			List<PointerTargetNodeList> listHypernym = hypernyms.toList();
			for (PointerTargetNodeList h: listHypernym){
				long tempOffset = (long) 0;
//				System.out.print("List: ");
				for (int i = h.size() - 1; i >= 0; i--) {
//					System.out.print(h.get(i).getSynset().getOffset() + " ");
					if (!treeRootNodeLong.isChild(treeRootNodeLong, tempOffset, h.get(i).getSynset().getOffset())){
//						System.out.println("Parent " + tempOffset + " insert " + h.get(i).getSynset().getOffset());
//						System.out.println(!treeRootNodeLong.isChild(treeRootNodeLong, tempOffset, h.get(i).getSynset().getOffset()));
						treeRootNodeLong.addChild(
							treeRootNodeLong.findNode(treeRootNodeLong, tempOffset), 
							h.get(i).getSynset().getOffset()
						);
					} 
					tempOffset = h.get(i).getSynset().getOffset();
				}
//				System.out.println();
			}
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return treeRootNodeLong;
	}
	
	/* Getting all hypernym and hyponym as an array */
	public List<Synset[]> getArrayLevel(List<Synset> senses){
		
		List<Synset[]> levelList = new ArrayList<Synset[]>();
		
		for(Synset sense: senses){
			/* Get related individual */
			try {
				PointerTargetTree hypernyms = PointerUtils.getHypernymTree(sense);
				List<PointerTargetNodeList> listHypernym = hypernyms.toList();
				for (PointerTargetNodeList hp: listHypernym){
					Synset[] tempTopLevel = new Synset[hp.size() + 1];
//					System.out.println("List: " + sense.toString());
					for (int i = hp.size() - 1; i >= 0; i--) {
//						System.out.println(hp.get(i).getSynset().toString());
						tempTopLevel[hp.size() - 1 - i] = hp.get(i).getSynset();
					}
//					System.out.println();
					PointerTargetTree hyponyms = PointerUtils.getHyponymTree(sense);
					List<PointerTargetNodeList> listHyponym = hyponyms.toList();
					for (PointerTargetNodeList ho: listHyponym){
						Synset[] tempLevel = new Synset[tempTopLevel.length + ho.size() - 2]; 
						for (int i = 0; i < tempTopLevel.length - 1; i++) {
							tempLevel[i] = tempTopLevel[i];
						}
						for (int i = 1; i < ho.size() - 1; i++) {
//							System.out.println(ho.get(i).getSynset().toString());
							tempLevel[tempTopLevel.length - 2 + i] = ho.get(i).getSynset();
						}
//						for(Synset s: tempLevel){
//							if(s != null)
//								System.out.println(s.toString());
//						}
//						System.out.println();
						levelList.add(tempLevel);
					}
//					System.out.println();
				}
				
			} catch (JWNLException e) {
				e.printStackTrace();
			}
			
		}
//		System.out.println("Total: " + levelList.size());
		return levelList;
	}
	
	/* Getting all hypernym and hyponym as an array */
	public List<Synset[]> getArrayLevelHypernymHyponym(Set<Synset> senses){
		
		List<Synset[]> levelList = new ArrayList<Synset[]>();
		
		for(Synset sense: senses){
			/* Get related individual */
			try {
				PointerTargetTree hypernyms = PointerUtils.getHypernymTree(sense);
				PointerTargetTree hyponyms = PointerUtils.getHyponymTree(sense);
				List<PointerTargetNodeList> listHypernym = hypernyms.toList();
				List<PointerTargetNodeList> listHyponym = hyponyms.toList();
				for (PointerTargetNodeList hp: listHypernym){
					for (PointerTargetNodeList ho: listHyponym){
						Synset[] tempLevel = new Synset[hp.size() + ho.size()];
						for (int i = hp.size() - 1; i >= 0; i--) {
							tempLevel[i] = hp.get(hp.size() - 1 - i).getSynset();
							for (int j = hp.size(); j < hp.size() + ho.size() - 1; j++) {
								tempLevel[j] = ho.get(j - hp.size()).getSynset();
							}
						}
//						for(Synset t: tempLevel){
//							if(t != null) System.out.println(t.toString());
//						}
//						System.out.println();
						if(!levelList.contains(tempLevel)) levelList.add(tempLevel);
					}
				}
				
			} catch (JWNLException e) {
				e.printStackTrace();
				continue;
			}
			
		}
		return levelList;
	}
	
	/* Map all string with its list of array level */
	public Map<String, Set<Synset[]>> getMappedWordAllLevelSynset(List<String> input){
		Map<String, Set<Synset[]>> mapWordSynset = new TreeMap<String, Set<Synset[]>>();
		int total = 0;
		
		for(String word: input){
			Set<IndexWord> indexWords = this.listIndexWord(word);
			Set<Synset[]> listAllLevel = new HashSet<Synset[]>();
			for (IndexWord indexWord : indexWords) {
				
				/* List different senses */
				Set<Synset> senses = new HashSet<Synset>(indexWord.getSenses());
				
				/* Get all level */
				listAllLevel.addAll(this.getArrayLevelHypernymHyponym(senses));
			}
			total = total + listAllLevel.size();
//			System.out.println("Total list: " + total);
			mapWordSynset.put(word, listAllLevel);
		}
		
		return mapWordSynset;
	}
	
	public Set<Synset[]> getMappedWordAllLevelSynsetInSet(List<String> input){
		Set<Synset[]> setSynset = new HashSet<Synset[]>();
		int total = 0;
		
		for(String word: input){
			Set<IndexWord> indexWords = this.listIndexWord(word);
			Set<Synset[]> listAllLevel = new HashSet<Synset[]>();
			for (IndexWord indexWord : indexWords) {
				
				/* List different senses */
				Set<Synset> senses = new HashSet<Synset>(indexWord.getSenses());
				
				/* Get all level */
				listAllLevel.addAll(this.getArrayLevelHypernymHyponym(senses));
			}
			total = total + listAllLevel.size();
//			System.out.println("Total list: " + total);
			setSynset.addAll(listAllLevel);
		}
		
		return setSynset;
	}
	
	/* Get all synset belongs to all input */
	public Set<Synset> getWordSynset(List<String> input){
		Set<Synset> senses = new HashSet<Synset>();
		List<IndexWord> indexWords = this.listIndexMultipleWord(input);
		for (IndexWord indexWord : indexWords) {
			/* List all senses */
			senses.addAll(indexWord.getSenses());
		}
//		System.out.println("Total senses: " + senses.size());
		return senses;
	}
	
	public List<Synset> getWordSynset(String input){
		List<Synset> senses = new ArrayList<Synset>();
		Set<IndexWord> indexWords = this.listIndexWord(input);
		for (IndexWord indexWord : indexWords) {
			/* List all senses */
			senses.addAll(indexWord.getSenses());
		}
		return senses;
	}
	
	public Map<String, String> getLemmaDefinitionAsString(Synset s){
		Map<String, String>	map = new HashMap<String, String>();
//		System.out.println(s.toString());
		List<Word> words = s.getWords();
		for (Word word : words) {
			/* Get its lemma and its POS for each word */
//			System.out.println(
//				"\t POS: " + word.getPOS().getKey() + " | " +
//				"Lemma: " + word.getLemma()
//			);
			map.put(word.getLemma(), s.getGloss());
		}
		
		return map;
	}
	
	/* Count Specification Marks Score and method's result */
	public Set<Synset> specificationMarksResult(Set<Synset> input, Map<String, Set<Synset[]>> map){
		Set<Synset> result = new HashSet<Synset>();
		for (Map.Entry<String, Set<Synset[]>> m : map.entrySet()) {
//			System.out.println(m.getKey());
			int score = 0;
			List<Synset> tempResult = new ArrayList<Synset>();
			for(Synset[] arraySyn: m.getValue()){
				Synset temp = null;
				int tempScore = 0;
				for(Synset in: input){
					for(Synset s: arraySyn){
						if(in.equals(s)){
							tempScore++;
							temp = s;
						}
					}
				}
				if(score < tempScore && temp != null){
					score = tempScore;
					tempResult.removeAll(tempResult);
					tempResult.add(temp);
				} else if (score == tempScore && temp != null){
//					System.out.println(m.getKey() + " with score " + tempScore);
					tempResult.removeAll(tempResult);
//					tempResult.add(temp);
				}
			}
			result.addAll(tempResult);
		}
		return result;
	}
	
	public List<Synset> specificationMarksResult(Map<String, Integer> input, Map<String, Set<Synset[]>> map){
		List<Synset> result = new ArrayList<Synset>();
		for (Map.Entry<String, Set<Synset[]>> m : map.entrySet()) {
			int score = 0;
			List<Synset> tempResult = new ArrayList<Synset>();
			for(Synset[] arraySyn: m.getValue()){
				Synset temp = null;
				int tempScore = 0;
				for (Map.Entry<String, Integer> in : input.entrySet()) {
					goThroughLevel: for(Synset s: arraySyn){
						try {
							if(s.toString().isEmpty()) System.out.println("Null"); 
						} catch (Exception e) {
							// TODO Auto-generated catch block
//							e.printStackTrace();
							continue goThroughLevel;
						}
//						System.out.println("The synset: " + s.toString());
//						System.out.println("Words: " + s.getWords().toString());
//						System.out.println("Contains: " + in.getKey());
						if(s.containsWord(in.getKey())){
//							System.out.println("SM contains: " + in.getKey());
							tempScore += in.getValue();
							temp = s;
						}
					}
				}
				if(score < tempScore && temp != null){
					score = tempScore;
					tempResult.removeAll(tempResult);
					tempResult.add(temp);
				} else if (score == tempScore && temp != null){
					tempResult.removeAll(tempResult);
				}
			}
			result.addAll(tempResult);
		}
		return result;
	}
	
	public Set<Synset> specificationMarksResult(Map<String, Integer> input, Set<Synset[]> set){
		Set<Synset> result = new HashSet<Synset>();
		Set<Synset> tempResult = new HashSet<Synset>();
		for(Synset[] syn: set){
			int score = 0;
			int tempScore = 0;
			goThroughLevel: for(Synset s: syn){
				try {
					if(s.toString().isEmpty()) System.out.println("Null"); 
				} catch (Exception e) {
					continue goThroughLevel;
				}
				Set<String> lemma = this.getLemmaToString(s);
				for(String l: lemma){
					if(input.containsKey(l)){
						score += input.get(l);
						tempResult.add(s);
						break;
					}
				}
			}
			if(score < tempScore && !tempResult.isEmpty()){
				score = tempScore;
				tempResult.removeAll(tempResult);
				tempResult.addAll(tempResult);
			}
		}
		result.addAll(tempResult);
		return result;
	}
	
	public Set<Synset> specificationMarksWordNet(List<String> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		/* Get input's hypernym and hyponym level as arrays */
		Map<String, Set<Synset[]>> mapWordSynset = this.getMappedWordAllLevelSynset(input);
		
		/* Get Specification Marks method's result as a set of array synset */
		Set<Synset> wordSynset = this.getWordSynset(input);
		Set<Synset> synResult = this.specificationMarksResult(wordSynset, mapWordSynset);
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		System.out.println("Specification Marks result: " + synResult.size() + " senses.");
		return synResult;
	}
	
	public Set<Synset> specificationMarksWordNet(Map<String, Integer> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		List<String> in = new ArrayList<String>(input.keySet());
		
		/* Get input's hypernym and hyponym level as arrays */
		Map<String, Set<Synset[]>> mapWordSynset = this.getMappedWordAllLevelSynset(in);
//		Set<Synset[]> setSynset = this.getMappedWordAllLevelSynsetInSet(in);
		
		/* Get Specification Marks method's result as a set of array synset */
		Set<Synset> synResult = new HashSet<Synset>();
		synResult.addAll(this.specificationMarksResult(input, mapWordSynset));
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		System.out.println("Specification Marks result: " + synResult.size() + " senses.");
		return synResult;
	}
	
	public List<Synset> hypernymHyponymHeuristicWordNet(List<String> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		List<Synset> synResult = new ArrayList<Synset>();
		
		/* Get input's hypernym and hyponym level as arrays */
		Map<String, Set<Synset[]>> mapWordLevelSynset = this.getMappedWordAllLevelSynset(input);
		
		/* Get input's synset list for the respective word */
		Map<String, Set<Synset>> mapWordSynset = this.getMappedInputSenses(input);
		
		for (Map.Entry<String, Set<Synset[]>> m : mapWordLevelSynset.entrySet()) {
			List<Synset> tempSyn = new ArrayList<Synset>();
			double score = 0;
			
			for(Synset[] sa: m.getValue()){
				Synset tempSense = null;
				double tempScore = 0;
				
				senseLoop:
				for(int i = 0; i < sa.length; i++){
					Set<String> tempSet = new TreeSet<String>();
					if(sa[i] != null ) tempSet = this.getLemmaToString(sa[i]);
					else continue senseLoop;
					
					for(String ts: tempSet){
						String[] tempStr = irpreprocessing.tokenization(ts);
						List<String> tempListStr = Arrays.asList(tempStr);
						for(String str: tempListStr){
							if(input.contains(str)){
								tempScore += (i+1)/sa.length;
								if(mapWordSynset.get(m.getKey()).contains(sa[i]))
									tempSense = sa[i];
								continue senseLoop;
							}
						}
					}
				}
				if(score < tempScore && tempSense != null){
					score = tempScore;
					tempSyn.removeAll(tempSyn);
					tempSyn.add(tempSense);
				} else if(score == tempScore && tempSense != null) {
					tempSyn.removeAll(tempSyn);
//					tempSyn.add(tempSense);
				}
			}
			synResult.addAll(tempSyn);
		}
		
		/* Map lemma and gloss definition */
		Map<String, String> mapLemmaGloss = new TreeMap<String, String>();
		for(Synset s: synResult){
			mapLemmaGloss.putAll(this.getLemmaDefinitionAsString(s));
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		System.out.println("Hypernym/Hyponym Heuristic result: " + synResult.size() + " senses.");
		return synResult;
	}
	
	public Set<Synset> hypernymHyponymHeuristicWordNet(Map<String, Integer> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		Set<Synset> synResult = new HashSet<Synset>();
		
		/* Get input's hypernym and hyponym level as arrays */
		List<String> in = new ArrayList<String>(input.keySet());
		Map<String, Set<Synset[]>> mapWordLevelSynset = this.getMappedWordAllLevelSynset(in);
		
		/* Get input's synset list for the respective word */
		Map<String, Set<Synset>> mapWordSynset = this.getMappedInputSenses(in);
		
		for (Map.Entry<String, Set<Synset[]>> m : mapWordLevelSynset.entrySet()) {
			List<Synset> tempSyn = new ArrayList<Synset>();
			double score = 0;
			
			for(Synset[] sa: m.getValue()){
				Synset tempSense = null;
				double tempScore = 0;
				
				senseLoop:
				for(int i = 0; i < sa.length; i++){
					Set<String> tempSet = new TreeSet<String>();
					if(sa[i] != null ) tempSet = this.getLemmaToString(sa[i]);
					else continue senseLoop;
					
					for(String ts: tempSet){
						String[] tempStr = irpreprocessing.tokenization(ts);
						List<String> tempListStr = Arrays.asList(tempStr);
						for(String str: tempListStr){
							if(input.containsKey(str)){
								tempScore += ((i+1)/sa.length)*input.get(str);
								if(mapWordSynset.get(m.getKey()).contains(sa[i]))
									tempSense = sa[i];
								continue senseLoop;
							}
						}
					}
				}
				if(score < tempScore && tempSense != null){
					score = tempScore;
					tempSyn.removeAll(tempSyn);
					tempSyn.add(tempSense);
				} else if(score == tempScore && tempSense != null) {
					tempSyn.removeAll(tempSyn);
//					tempSyn.add(tempSense);
				}
			}
			synResult.addAll(tempSyn);
		}
		
		/* Map lemma and gloss definition */
		Map<String, String> mapLemmaGloss = new TreeMap<String, String>();
		for(Synset s: synResult){
			mapLemmaGloss.putAll(this.getLemmaDefinitionAsString(s));
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		System.out.println("Hypernym/Hyponym Heuristic result: " + synResult.size() + " senses.");
		return synResult;
	}
	
	public List<Synset> definitionHeuristicWordNet(List<String> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		List<Synset> synResult = new ArrayList<Synset>();
		
		/* Get input's synset list for the respective word */
		Map<String, Set<Synset>> mapWordSynset = this.getMappedInputSenses(input);
		
		for (Map.Entry<String, Set<Synset>> m : mapWordSynset.entrySet()) {
			List<Synset> tempSyn = new ArrayList<Synset>();
			int score = 0;
			for(Synset s: m.getValue()){
				int tempScore = 0;
				String[] tempGloss = irpreprocessing.tokenization(s.getGloss());
				for(String gloss: tempGloss){
					if(input.contains(gloss)){
						tempScore++;
					}
				}
				if(score < tempScore){
					score = tempScore;
					tempSyn.removeAll(tempSyn);
					tempSyn.add(s);
				} else if (score == tempScore){
					tempSyn.removeAll(tempSyn);
//					tempSyn.add(s);
				}
			}
			synResult.addAll(tempSyn);
		}
		
		/* Map lemma and gloss definition */
		Map<String, String> mapLemmaGloss = new TreeMap<String, String>();
		for(Synset s: synResult){
			mapLemmaGloss.putAll(this.getLemmaDefinitionAsString(s));
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		System.out.println("Definition Heuristic result: " + synResult.size() + " senses.");		
		return synResult;
	}
	
	public Set<Synset> definitionHeuristicWordNet(Map<String, Integer> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		Set<Synset> synResult = new HashSet<Synset>();
		List<String> in = new ArrayList<String>(input.keySet());
		
		/* Get input's synset list for the respective word */
		Map<String, Set<Synset>> mapWordSynset = this.getMappedInputSenses(in);
		
		for (Map.Entry<String, Set<Synset>> m : mapWordSynset.entrySet()) {
			List<Synset> tempSyn = new ArrayList<Synset>();
			int score = 0;
			for(Synset s: m.getValue()){
				int tempScore = 0;
				String[] tempGloss = irpreprocessing.tokenization(s.getGloss());
				for(String gloss: tempGloss){
					if(input.containsKey(gloss)){
						tempScore += input.get(gloss);
					}
				}
				if(score < tempScore){
					score = tempScore;
					tempSyn.removeAll(tempSyn);
					tempSyn.add(s);
				} else if (score == tempScore){
					tempSyn.removeAll(tempSyn);
				}
			}
			synResult.addAll(tempSyn);
		}
		
		/* Map lemma and gloss definition */
		Map<String, String> mapLemmaGloss = new TreeMap<String, String>();
		for(Synset s: synResult){
			mapLemmaGloss.putAll(this.getLemmaDefinitionAsString(s));
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		System.out.println("Definition Heuristic result: " + synResult.size() + " senses.");		
		return synResult;
	}
	
	public List<Synset> glossHypernymHyponymHeuristicWordNet(List<String> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		List<Synset> synResult = new ArrayList<Synset>();
		List<Synset> allSynset = new ArrayList<Synset>(this.getWordSynset(input));
		
		/* Get input's synset list for the respective word */
		Map<String, Set<Synset[]>> mapWordSynset = this.getMappedWordAllLevelSynset(input);
		
		for (Map.Entry<String, Set<Synset[]>> m : mapWordSynset.entrySet()) {
			List<Synset> tempSyn = new ArrayList<Synset>();
			int score = 0;
			
			for(Synset[] sa: m.getValue()){
				int tempScore = 0;
				Synset temp = null;
				
				for(Synset s: sa){
					if (s != null) {
						String[] tempGloss = irpreprocessing.tokenization(s.getGloss());
						for(String gloss: tempGloss){
							if(input.contains(gloss)){
								if(allSynset.contains(s)) temp = s;
								tempScore++;
							}
						}
					}
				}
				if(score < tempScore && temp != null){
					score = tempScore;
					tempSyn.removeAll(tempSyn);
					tempSyn.add(temp);
				} else if (score == tempScore && temp != null){
					tempSyn.removeAll(tempSyn);
//					tempSyn.add(temp);
				}
			}
			synResult.addAll(tempSyn);
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		System.out.println("Gloss Hypernym/Hyponym Heuristic result: " + synResult.size() + " senses.");		
		return synResult;
	}
	
	public Set<Synset> glossHypernymHyponymHeuristicWordNet(Map<String, Integer> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		Set<Synset> synResult = new HashSet<Synset>();
		List<String> in = new ArrayList<String>(input.keySet());
		List<Synset> allSynset = new ArrayList<Synset>(this.getWordSynset(in));
		
		/* Get input's synset list for the respective word */
		Map<String, Set<Synset[]>> mapWordSynset = this.getMappedWordAllLevelSynset(in);
		
		for (Map.Entry<String, Set<Synset[]>> m : mapWordSynset.entrySet()) {
			List<Synset> tempSyn = new ArrayList<Synset>();
			int score = 0;
			
			for(Synset[] sa: m.getValue()){
				int tempScore = 0;
				Synset temp = null;
				
				for(Synset s: sa){
					if (s != null) {
						String[] tempGloss = irpreprocessing.tokenization(s.getGloss());
						for(String gloss: tempGloss){
							if(input.containsKey(gloss)){
								if(allSynset.contains(s)) temp = s;
								tempScore += input.get(gloss);
							}
						}
					}
				}
				if(score < tempScore && temp != null){
					score = tempScore;
					tempSyn.removeAll(tempSyn);
					tempSyn.add(temp);
				} else if (score == tempScore && temp != null){
					tempSyn.removeAll(tempSyn);
//					tempSyn.add(temp);
				}
			}
			synResult.addAll(tempSyn);
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		System.out.println("Gloss Hypernym/Hyponym Heuristic result: " + synResult.size() + " senses.");		
		return synResult;
	}
	
	public Map<String, String> refineResult(String code, List<String> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		List<Synset> synResult = new ArrayList<Synset>();
		
		if(code.equals("all")){
			synResult.addAll(this.specificationMarksWordNet(input));
			synResult.addAll(this.hypernymHyponymHeuristicWordNet(input));
			synResult.addAll(this.definitionHeuristicWordNet(input));
			synResult.addAll(this.glossHypernymHyponymHeuristicWordNet(input));
		}
		
		/* Map lemma and gloss definition */
		Map<String, String> mapLemmaGloss = new TreeMap<String, String>();
		for(Synset s: synResult){
			Set<String> lemma = this.getLemmaToString(s);
			for(String l: lemma){
				if(input.contains(l)){
					mapLemmaGloss.putAll(this.getLemmaDefinitionAsString(s));
					break;
				}
			}
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		System.out.println("Refine result: " + mapLemmaGloss.size() + " words.");		
		return mapLemmaGloss;
		
	}
	
	public Map<String, String> refineResult(String code, Map<String, Integer> input){
		/* Calculate execution time */
		long startTime = System.currentTimeMillis();
		
		Set<Synset> synResult = new HashSet<Synset>();
		Map<Synset, Integer> mapKey = new HashMap<Synset, Integer>();
		
		for(Map.Entry<String, Integer> m : input.entrySet()) {
			Set<Synset> temp = this.getSynsetByString(m.getKey()); 
			if(temp.size() == 1) synResult.addAll(temp);
			if(m.getValue() == 1) continue;
			List<Synset> listKey = new ArrayList<Synset>();
			if(m.getValue() == 3) {
				String[] split = m.getKey().split(" ");
				for(String s: split) listKey.addAll(this.getDomainSynset(s));
			}
			listKey.addAll(this.getDomainSynset(m.getKey()));
			for(Synset s: listKey)
				if(mapKey.containsKey(s)) mapKey.put(s, mapKey.get(s) + 1);
				else mapKey.put(s, 1);
		}
		
		Set<Synset> keySet = new HashSet<Synset>();
		for(Map.Entry<Synset, Integer> map : mapKey.entrySet()) { 
			if((Double.valueOf(map.getValue())/mapKey.size()) > 0.25) keySet.add(map.getKey()); 
		}
		
		if(code.equals("all")){
			synResult.addAll(this.specificationMarksWordNet(input));
			synResult.addAll(this.hypernymHyponymHeuristicWordNet(input));
			synResult.addAll(this.definitionHeuristicWordNet(input));
			synResult.addAll(this.glossHypernymHyponymHeuristicWordNet(input));
		}
		
		/* Map lemma and gloss definition */
		Map<String, String> mapLemmaGloss = new TreeMap<String, String>();
		
		mainFor: for(Synset s: synResult){
			Set<String> lemma = this.getLemmaToString(s);
			for(String l: lemma){
				if(input.containsKey(l)){
					/* Get senses with same domain as keywords */
					if(this.intersectionSynset(keySet, this.getSenseDomain(s)).size() < 1) continue mainFor;					
					
					mapLemmaGloss.putAll(this.getLemmaDefinitionAsString(s));
					
					/* Get direct hypernym */
					Set<String> hypernym = this.getDirectHypernym(s.getPOS().getKey().charAt(0), Long.toString(s.getOffset()));
					for(String hyp: hypernym) {
						String[] splitHyper = hyp.split("#");
						for(int i = 2; i < splitHyper.length - 1; i++) {
							mapLemmaGloss.put(splitHyper[i], splitHyper[splitHyper.length-1]);
						}
					}
					
					break;
				}
			}
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time consumed: " + (endTime - startTime) + " milliseconds.");
		
		System.out.println("Refine result: " + mapLemmaGloss.size() + " words.");		
		return mapLemmaGloss;
		
	}
	
	public Set<Synset> intersectionSynset(Set<Synset> inputA, Set<Synset> inputB){
		Set<Synset> synResult = new HashSet<Synset>(inputA);
		synResult.retainAll(inputB);		
		return synResult;
	}
	
	
	/* Get hyponym on tree structure */
	public Node<Long> getHyponymAsTree(Synset sense, Node<Long> treeRootNodeLong){
		try {
			PointerTargetTree hyponyms = PointerUtils.getHyponymTree(sense);
			List<PointerTargetNodeList> listHyponym = hyponyms.toList();
			for (PointerTargetNodeList h: listHyponym){
				long tempOffset = sense.getOffset();
				for (int i = 0; i < h.size(); i++) {
					if (!treeRootNodeLong.isChild(treeRootNodeLong, tempOffset, h.get(i).getSynset().getOffset())){
						treeRootNodeLong.addChild(
							treeRootNodeLong.findNode(treeRootNodeLong, tempOffset), 
							h.get(i).getSynset().getOffset()
						);
					} 
					tempOffset = h.get(i).getSynset().getOffset();
				}
			}
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return treeRootNodeLong;
	}
 	
	/* Get hypernym tree as node tree */
	public Node<Long> getHypernymNodeTree(String input){
		List<String> listInput = Arrays.asList(irpreprocessing.tokenization(input));
		List<IndexWord> indexWords = this.listIndexMultipleWord(listInput);
		Node<Long> treeRootNodeLong = new Node<Long>(null);
		treeRootNodeLong.setNodeName((long) 0);
		
		for (IndexWord indexWord : indexWords) {
			/* List different senses */
			List<Synset> senses = indexWord.getSenses();
				
			for (Synset sense : senses) {
				System.out.println(sense.toString());
				treeRootNodeLong = this.getHypernymAsTree(sense, treeRootNodeLong);
			}
		}
		treeRootNodeLong.printTree(treeRootNodeLong, " ");
		return treeRootNodeLong;
	}
	
	/* Get hyponym tree as node tree */
	public Node<Long> getHyponymNodeTree(String input){
		List<String> listInput = Arrays.asList(irpreprocessing.tokenization(input));
		List<IndexWord> indexWords = this.listIndexMultipleWord(listInput);
		Node<Long> treeRootNodeLong = new Node<Long>(null);
		treeRootNodeLong.setNodeName((long) 0);
		
		for (IndexWord indexWord : indexWords) {
			/* List different senses */
			List<Synset> senses = indexWord.getSenses();
				
			for (Synset sense : senses) {
				treeRootNodeLong = this.getHyponymAsTree(sense, treeRootNodeLong);
			}
		}
		treeRootNodeLong.printTree(treeRootNodeLong, " ");
		return treeRootNodeLong;
	}
	
	/* Get hypernym and hyponym tree as node tree */
	public Node<Long> getHypernymHyponymNodeTree(String input){
		List<String> listInput = Arrays.asList(irpreprocessing.tokenization(input));
		List<IndexWord> indexWords = this.listIndexMultipleWord(listInput);
		Node<Long> treeRootNodeLong = new Node<Long>(null);
		treeRootNodeLong.setNodeName((long) 0);
		
		for (IndexWord indexWord : indexWords) {
			/* List different senses */
			List<Synset> senses = indexWord.getSenses();
				
			for (Synset sense : senses) {
				treeRootNodeLong = this.getHypernymAsTree(sense, treeRootNodeLong);
				treeRootNodeLong = this.getHyponymAsTree(sense, treeRootNodeLong);
			}
		}
//		treeRootNodeLong.printTree(treeRootNodeLong, " ");
		return treeRootNodeLong;
	}
	
	public Node<Long> getHypernymHyponymNodeTree(List<String> input){
		List<IndexWord> indexWords = this.listIndexMultipleWord(input);
		Node<Long> treeRootNodeLong = new Node<Long>(null);
		treeRootNodeLong.setNodeName((long) 0);
		
		for (IndexWord indexWord : indexWords) {
			/* List different senses */
			List<Synset> senses = indexWord.getSenses();
				
			for (Synset sense : senses) {
				treeRootNodeLong = this.getHypernymAsTree(sense, treeRootNodeLong);
				treeRootNodeLong = this.getHyponymAsTree(sense, treeRootNodeLong);
			}
		}
//		treeRootNodeLong.printTree(treeRootNodeLong, " ");
		return treeRootNodeLong;
	}
	
	/* Get mapped input and its senses */
	@SuppressWarnings("unchecked")
	public Map<String, List<Synset>> getMappedInputSenses(String input) {
		Map<String, List<Synset>> mapStringSenses = new HashMap<>();
		List<String> listInput = this.inputPrep(input);
		for(String in: listInput) {
			Set<IndexWord> indexWords = this.listIndexWord(in);
			List<Synset> senses = new ArrayList<>();
			for (IndexWord indexWord : indexWords) {
				senses = javalist.concatList(senses, indexWord.getSenses());
			}
			mapStringSenses.put(in, senses);
//			System.out.print(in + ": " + senses.size() + "; Offset: ");
//			for (Synset sense: senses) {
//				System.out.print(sense.getOffset() + " ");
//			}
//			System.out.println();
		}
		return mapStringSenses;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Set<Synset>> getMappedInputSenses(List<String> input) {
		Map<String, Set<Synset>> mapStringSenses = new HashMap<>();
		for(String in: input) {
			Set<IndexWord> indexWords = this.listIndexWord(in);
			List<Synset> senses = new ArrayList<>();
			for (IndexWord indexWord : indexWords) {
				senses = javalist.concatList(senses, indexWord.getSenses());
			}
			Set<Synset> tempSenses = new HashSet<Synset>(senses);
			mapStringSenses.put(in, tempSenses);
//			System.out.print(in + ": " + senses.size() + "; Offset: ");
//			for (Synset sense: senses) {
//				System.out.print(sense.getOffset() + " ");
//			}
//			System.out.println();
		}
		return mapStringSenses;
	}
	
	/* Method to get Specification Marks Method result */
	public Map<String, Synset> getSpecificationMarks(String input){
		List<String> listInput = Arrays.asList(irpreprocessing.tokenization(input));
		Map<String, Synset> result = new HashMap<>();
		Map<String, List<Synset>> mapStringSenses = this.getMappedInputSenses(input);
		Node<Long> treeRootNodeLong = this.getHypernymHyponymNodeTree(input);
		
		/* Get the specification marks result */
		for(String in: listInput) {
			Synset tempResult = this.checkSpecificationMarks(in, treeRootNodeLong, mapStringSenses);
			if (tempResult != null) {
				System.out.println("Scoring " + in + " result " + tempResult.toString());
				result.put(in, tempResult);
			}
		}
		
		return result;		
	}	
	
	public Set<String> getSpecificationMarksWordNet(List<String> input){
		Set<String> result = new TreeSet<>();
//		Node<Long> treeRootNodeLong = this.getHypernymHyponymNodeTree(input);
//		Map<String, List<Synset>> mapStringSenses = this.getMappedInputSenses(input);
//		
//		for(String in: input){
//			System.out.println(in);
//			
//			/* Get the specification marks result */
//			Synset tempResult = this.checkSpecificationMarks(in, treeRootNodeLong, mapStringSenses);
//			if (tempResult != null) {
//				System.out.println("Scoring " + in + " result " + tempResult.toString());
//				result.addAll(this.getLemmaToString(tempResult));
//			}
//		}
//		
//		for(String rs: result){
//			System.out.println(rs);
//		}
		
		return result;		
	}
	
	/* Method to walk through tree to find Specification Marks result */
	public Synset checkSpecificationMarks(String in, Node<Long> treeRootNodeLong, Map<String, List<Synset>> mapStringSenses){
		Synset result = null;
		List<Node<Long>> listNode = treeRootNodeLong.getChildren();
		outerloop:
		for (Node<Long> node: listNode) {
			Map<Synset, Double> tempResult = new HashMap<>();
//			System.out.println("Specification marks: " + node.getNodeName());
			for (Synset sense: mapStringSenses.get(in)) {
//				System.out.println("Check sense: " + sense.toString());
				if (treeRootNodeLong.isDecendant(treeRootNodeLong, node.getNodeName(), sense.getOffset())){
//					System.out.println("Score " + sense.getOffset() + ": 1");
					tempResult.put(sense, 1.0);
				}
				for (Map.Entry<String, List<Synset>> m : mapStringSenses.entrySet()) {
					if (!m.getKey().equals(in)) {
						for (Synset s: m.getValue()) {
//							System.out.println(node.getNodeName() + " ancestor " + s.getOffset() + "?");
							if (treeRootNodeLong.isDecendant(treeRootNodeLong, node.getNodeName(), s.getOffset())
								&& tempResult.containsKey(sense)){
//								System.out.println("Score " + sense.getOffset() + ": " + (tempResult.get(sense) + 1));
								if (treeRootNodeLong.isInTheSamePath(node, sense.getOffset(), s.getOffset())){
									tempResult.put(sense, (tempResult.get(sense) + 1));
								} 
//								else {
//									tempResult.put(sense, (tempResult.get(sense) + 0.0));
//								}
							}
						}
					}
				}
			}
			
			Double max = 0.0;
			int count = 0;
			
			if (tempResult.size() > 0) {
//				System.out.print("Scoring \"" + in + "\" with specification marks " + node.getNodeName() + ": ");
				for (Map.Entry<Synset, Double> m : tempResult.entrySet()) {
//					System.out.print("Score " + m.getKey().getOffset() + " = " + m.getValue() + " ");
				}
				max = Collections.max(tempResult.values());
//				System.out.println();
				count = Collections.frequency(tempResult.values(), max);
//				System.out.print("Max = " + max);
//				System.out.print(" ");
//				System.out.print("Count = " + count);
//				System.out.println();
			}
			
			if (count == 1 && tempResult.size() > 1) {
				for (Map.Entry<Synset, Double> m : tempResult.entrySet()) {
					if (m.getValue() == max) {
						result = m.getKey();
						break outerloop;
					}
				}
			} else {
				result = this.checkSpecificationMarks(in, node, mapStringSenses);
			}
		}
		
		return result;
	}
	
	/* Method to get Hypernym Heuristic result */
	public Map<String, Synset> getHypernymHeuristic(String input) {
		Node<Long> treeRootNodeLong = this.getHypernymNodeTree(input);
		List<String> listInput = Arrays.asList(irpreprocessing.tokenization(input));
		Map<String, List<Synset>> mapStringSenses = this.getMappedInputSenses(input);
		Map<String, Synset> result = new HashMap<>();
		
		for (String in: listInput) {
			System.out.println("Scoring " + in + " with Hypernym Heuristic");
			Map<Synset, Double> tempResult = new HashMap<>();
			Synset tempSense = null;
			for (Synset sense: mapStringSenses.get(in)) {
				Map<Long, Integer> mapPath = treeRootNodeLong.mapHypernymPathLevel(treeRootNodeLong.findNode(treeRootNodeLong, sense.getOffset()));
				double score = 0;
				for (Map.Entry<String, List<Synset>> m : mapStringSenses.entrySet()) {
					if (!m.getKey().equals(in)) {
						for (Synset s: m.getValue()) {
							if (mapPath.containsKey(s.getOffset())) {
								double level = mapPath.get(s.getOffset()),
								depth = mapPath.size() - 1;
								System.out.println(
									m.getKey() + " [" +
									s.getOffset() + "] in path, score = " + 
									score + " + (" +
									mapPath.get(s.getOffset()) + "/" +
									(mapPath.size()-1) + ") = " +
									(score + (level/depth))
								);
								score = score + (level/depth);
								tempResult.put(sense, score);
							}
						}
					}
				}
//				System.out.println("Score " + sense.getOffset() + ": " + score);
			}
			double max = 0.0;
			int count = 0;
			
			if (tempResult.size() > 0) {
				for (Map.Entry<Synset, Double> m : tempResult.entrySet()) {
					System.out.print("Score " + m.getKey().getOffset() + " = " + m.getValue() + " ");
				}
				System.out.println();
				max = Collections.max(tempResult.values());
				count = Collections.frequency(tempResult.values(), max);
				System.out.print("Max = " + max);
				System.out.print(" ");
				System.out.print("Count = " + count);
				System.out.println();
			}
			
			if (count == 1) {
				for (Map.Entry<Synset, Double> m : tempResult.entrySet()) {
					if (m.getValue() == max) {
						tempSense = m.getKey();
						result.put(in, tempSense);
						break;
					}
				}
			}
			System.out.println("Result " + in + ": " + tempSense);
		}
		return result;
	}
	
	/* Method to get Definition Heuristic result */
	public Map<String, Synset> getDefinitionHeuristic(String input) {
		List<String> listInput = Arrays.asList(irpreprocessing.tokenization(input));
		Map<String, List<Synset>> mapStringSenses = this.getMappedInputSenses(input);
		Map<String, Synset> result = new HashMap<>();
		
		for (String in: listInput) {
			System.out.println("Scoring " + in + " with Definition Heuristic");
			Map<Synset, Double> tempResult = new HashMap<>();
			Synset tempSense = null;
			for (Synset sense: mapStringSenses.get(in)) {
				System.out.println("Count score for " + sense.getOffset());
				System.out.println("Gloss: " + sense.getGloss());
				List<String> tempGlossList = Arrays.asList(irpreprocessing.tokenization(sense.getGloss()));
				double score = 0;
//				for (int i = 0; i < tempGlossList.size(); i++) {
//					tempGlossList.set(i, irpreprocessing.porterStemmer(tempGlossList.get(i)));
//				}
				for (String inp: listInput) {
					System.out.println("Check on " + inp);
					if (tempGlossList.contains(inp)) {
						double occurences = Collections.frequency(tempGlossList, inp);
						System.out.println("Gloss of " + sense.getOffset() + " contains " + inp);
						System.out.println("Score = " +	score + " + " +	occurences + " = " + (score + occurences));
						score = score + occurences;
						tempResult.put(sense, score);
					}
				}
			}
			double max = 0.0;
			int count = 0;
			
			if (tempResult.size() > 0) {
				for (Map.Entry<Synset, Double> m : tempResult.entrySet()) {
					System.out.print("Score " + m.getKey().getOffset() + " = " + m.getValue() + " ");
				}
				System.out.println();
				max = Collections.max(tempResult.values());
				count = Collections.frequency(tempResult.values(), max);
				System.out.print("Max = " + max);
				System.out.print(" ");
				System.out.print("Count = " + count);
				System.out.println();
			}
			
			if (count == 1) {
				for (Entry<Synset, Double> m : tempResult.entrySet()) {
					if (m.getValue() == max) {
						tempSense = m.getKey();
						result.put(in, tempSense);
						break;
					}
				}
			}
			System.out.println("Result " + in + ": " + tempSense);
		}
		return result;
	}
	
	/* Method to get Gloss Hypernym Heuristic result */
	public void getGlossHypernymHeuristic(String input) {
		Node<Long> treeRootNodeLong = this.getHypernymNodeTree(input);
		List<String> listInput = Arrays.asList(irpreprocessing.tokenization(input));
		Map<String, List<Synset>> mapStringSenses = this.getMappedInputSenses(input);
		Map<String, Synset> result = new HashMap<>();
		
		for (String in: listInput) {
			System.out.println("Scoring " + in + " with Gloss Hypernym Heuristic");
			Map<Synset, Double> tempResult = new HashMap<>();
			Synset tempSense = null;
			for (Synset sense: mapStringSenses.get(in)) {
				Map<Long, Integer> mapPath = 
					treeRootNodeLong.mapHypernymPathLevel(treeRootNodeLong.findNode(treeRootNodeLong, sense.getOffset()));
				POS pos = sense.getPOS();
				double score = 0;
				for (Map.Entry<Long, Integer> m : mapPath.entrySet()) {
					if (m.getKey() > 0) {
						Synset pathSynset = this.getSynsetByOffset(pos, m.getKey());
						List<String> tempGlossList = Arrays.asList(irpreprocessing.tokenization(pathSynset.getGloss()));
						System.out.println("Gloss: " + pathSynset.getGloss());
						for (String inp: listInput) {
							System.out.println("Check on " + inp);
							if (tempGlossList.contains(inp)) {
								double occurences = Collections.frequency(tempGlossList, inp);
								System.out.println("Gloss of " + pathSynset.getOffset() + " contains " + inp);
								System.out.println("Score = " +	score + " + " +	occurences + " = " + (score + occurences));
								score = score + occurences;
								tempResult.put(pathSynset, score);
							}
						}
					}
				}
//				System.out.println("Score " + sense.getOffset() + ": " + score);
			}
			double max = 0.0;
			int count = 0;
			
			if (tempResult.size() > 0) {
				for (Map.Entry<Synset, Double> m : tempResult.entrySet()) {
					System.out.print("Score " + m.getKey().getOffset() + " = " + m.getValue() + " ");
				}
				System.out.println();
				max = Collections.max(tempResult.values());
				count = Collections.frequency(tempResult.values(), max);
				System.out.print("Max = " + max);
				System.out.print(" ");
				System.out.print("Count = " + count);
				System.out.println();
			}
			
			if (count == 1) {
				for (Map.Entry<Synset, Double> m : tempResult.entrySet()) {
					if (m.getValue() == max) {
						tempSense = m.getKey();
						result.put(in, tempSense);
						break;
					}
				}
			}
			System.out.println("Result " + in + ": " + tempSense);
		}
//		return result;
	}
	
	/* Method to get Domain WSD Heuristic based on input */
	public void getDomainHeuristic(String input) {
		Map<String, List<Synset>> mapStringSenses = this.getMappedInputSenses(input);
		Map<Synset, Integer> domainCount = new HashMap<>();
//		Set<Synset> domainSet = new HashSet<>();
		for (Map.Entry<String, List<Synset>> m : mapStringSenses.entrySet()) {
			System.out.println("Word: " + m.getKey());
			for (Synset sense: m.getValue()) {
				domainCount.putAll(this.getSenseGlossDomain(sense));
			}
			for (Map.Entry<Synset, Integer> map : domainCount.entrySet()) {
				System.out.println("Domain synset: " + map.getKey().toString());
				System.out.println("Domain pos: " + map.getKey().getPOS().getKey());
				System.out.println("Domain count: " + map.getValue());
			}
		}
	}
	
	/* Method to get sense's and its gloss domain */
	public Map<Synset, Integer> getSenseGlossDomain(Synset sense) {
		Map<Synset, Integer> domainCount = new HashMap<>();
		List<Synset> domainSet = new ArrayList<>();
//		System.out.println("Sense synset: " + sense.toString());
		Map<String, List<Synset>> mapStringSensesGloss = this.getMappedInputSenses(sense.getGloss());
		for (Map.Entry<String, List<Synset>> map : mapStringSensesGloss.entrySet()) {
//			System.out.println("Gloss word: " + map.getKey());
			for (Synset senseGloss: map.getValue()) {
				domainSet.addAll(this.getSenseDomain(senseGloss));
//				if (domainCount.containsKey(senseGloss)) {
//					domainCount.put(senseGloss, domainCount.get(senseGloss) + 1);
//				} else {
//					domainCount.put(senseGloss, 1);
//				}
			}
		}
		domainSet.addAll(this.getSenseDomain(sense));
		for (Synset domain: domainSet) {
			if (domainCount.containsKey(domain)) {
				domainCount.put(domain, domainCount.get(domain) + 1);
			} else {
				domainCount.put(domain, 1);
			}
		}
		return domainCount;
	}
	
	/* Method to get sense's domain */
	public Set<Synset> getSenseDomain(Synset sense) {
		Set<Synset> domainSet = new HashSet<>();
		List<Pointer> pointerList = new ArrayList<>();
		pointerList.addAll(sense.getPointers(PointerType.CATEGORY));
		pointerList.addAll(sense.getPointers(PointerType.USAGE));
		pointerList.addAll(sense.getPointers(PointerType.REGION));
		for (Pointer pointer : pointerList) {
			try {
				Synset syn = pointer.getTargetSynset();
//				System.out.println(pointerList.size());
//				System.out.println("Domain synset: " + syn.toString());
				domainSet.add(syn);
			} catch (JWNLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		Synset tempDomain = null; 
//		for (Synset domain: domainSet) {
//			if (!domain.equals(tempDomain)) {
//				tempDomain = domain;
//				System.out.println("Domain synset: " + domain.toString());
//				System.out.println("Domain count: " + Collections.frequency(domainSet, domain));
//			}
//		}
		return domainSet;
	}
	
	/* Method to get sense's domain */
	public List<Synset> getSenseDomainMember(Synset sense) {
		List<Synset> domainMemberSet = new ArrayList<>();
		List<Pointer> pointerList = new ArrayList<>();
		pointerList.addAll(sense.getPointers(PointerType.CATEGORY_MEMBER));
		pointerList.addAll(sense.getPointers(PointerType.USAGE_MEMBER));
		pointerList.addAll(sense.getPointers(PointerType.REGION_MEMBER));
		for (Pointer pointer : pointerList) {
			try {
				Synset syn = pointer.getTargetSynset();
				domainMemberSet.add(syn);
			} catch (JWNLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
		return domainMemberSet;
	}
	
	/* Test to get hypernyms (parents) of the senses */
	public void testHypernyms(String input) {
		Set<IndexWord> indexWords = this.listIndexWord(input);
		for (IndexWord indexWord : indexWords) {
			if (indexWord != null) {
				/* List different senses */
				List<Synset> senses = indexWord.getSenses();
				
				for (Synset sense : senses) {
					try {
//						PointerTargetNodeList hypernyms = PointerUtils.getDirectHypernyms(sense);
						PointerTargetTree hypernyms = PointerUtils.getHypernymTree(sense);
						System.out.println(
							indexWord.getLemma() + " " +
							sense.getOffset() +
							" has hypernyms : "
						);
						hypernyms.print();
					} catch (JWNLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	/* Test to get hyponyms (children) of the senses */
	public void testHyponyms(String input) {
		Set<IndexWord> indexWords = this.listIndexWord(input);
		for (IndexWord indexWord : indexWords) {
			if (indexWord != null) {
				/* List different senses */
				List<Synset> senses = indexWord.getSenses();
				
				for (Synset sense : senses) {
					try {
						PointerTargetTree hyponyms = PointerUtils.getHyponymTree(sense);
						System.out.println(indexWord + " has hyponyms : ");
						hyponyms.print();
					} catch (JWNLException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public void tryGetAsymmetricRelation(String start, String end) {
		IndexWord indexWordStart = this.checkWordNoun(start);
		IndexWord indexWordEnd = this.checkWordNoun(end);
		
		try {
			this.demonstrateAsymmetricRelationshipOperation(indexWordStart, indexWordEnd);
		} catch (JWNLException e) {
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
	
	/* source: https://github.com/extjwnl/extjwnl/blob/master/utilities/src/main/java/net/sf/extjwnl/utilities/Examples.java */
	private void demonstrateAsymmetricRelationshipOperation(IndexWord start, IndexWord end) 
		throws JWNLException, CloneNotSupportedException {
        /* Try to find a relationship between the first sense of <var>start</var> 
         * and the first sense of <var>end</var> */
        RelationshipList list = RelationshipFinder.findRelationships(
        	start.getSenses().get(0), 
        	end.getSenses().get(0), 
        	PointerType.HYPERNYM
        	);
        System.out.println("Hypernym relationship between \"" + 
        	start.getLemma() + "\" and \"" + 
        	end.getLemma() + "\":");
        for (Object aList : list) {
            ((Relationship) aList).getNodeList().print();
        }
        System.out.println(
        	"Common Parent Index: " + 
        	((AsymmetricRelationship) list.get(0)).getCommonParentIndex());
        System.out.println("Depth: " + list.get(0).getDepth());
    }
	
	public void tryGetSymmetricRelation(String start, String end) {
		IndexWord indexWordStart = this.checkWordAdjective(start);
		IndexWord indexWordEnd = this.checkWordAdjective(end);
		
		try {
			this.demonstrateSymmetricRelationshipOperation(indexWordStart, indexWordEnd);
		} catch (JWNLException e) {
			e.printStackTrace();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}
	
	/* source: https://github.com/extjwnl/extjwnl/blob/master/utilities/src/main/java/net/sf/extjwnl/utilities/Examples.java */
	private void demonstrateSymmetricRelationshipOperation(IndexWord start, IndexWord end) 
		throws JWNLException, CloneNotSupportedException {
        /* find all synonyms that <var>start</var> and <var>end</var> have in common */
        RelationshipList list = RelationshipFinder.findRelationships(
        	start.getSenses().get(0), 
        	end.getSenses().get(0), 
        	PointerType.SIMILAR_TO
        );
        System.out.println(
        	"Synonym relationship between \"" + 
        	start.getLemma() + "\" and \"" + 
        	end.getLemma() + "\":"
        );
        for (Object aList : list) {
            ((Relationship) aList).getNodeList().print();
        }
        System.out.println("Depth: " + list.get(0).getDepth());
    }
	
	public Set<String> getDomain(String input){
		Set<String> result = new HashSet<String>();
		Set<IndexWord> indexWords = this.listIndexWord(input);
		
		for (IndexWord indexWord : indexWords) {
			if (indexWord != null) {
				/* List different senses */
				List<Synset> senses = indexWord.getSenses();
		
				for (Synset sense : senses) {										
					
					/* Getting all domain */
					Set<Synset> sensesDomain = this.getSenseDomain(sense);
					for (Synset s : sensesDomain) {
//						System.out.println(s.getSynset());
//						System.out.println(s.getOffset());
						
						/* List other lemma in the synset */
						String allDomain = this.getLemmaString(s);
						result.add(s.getOffset()+"#"+s.getPOS()+allDomain+"#"+s.getGloss());
					}
				}
			}
		}
		return result;
	}
	
	public Set<Synset> getDomainSynset(String input){
		Set<Synset> result = new HashSet<Synset>();
		Set<IndexWord> indexWords = this.listIndexWord(input);
		
		for (IndexWord indexWord : indexWords) {
			if (indexWord != null) {
				/* List different senses */
				
				for (Synset sense : indexWord.getSenses()) {										
					
					/* Getting all domain */
					result.addAll(this.getSenseDomain(sense));
				}
			}
		}
		return result;
	}
	
	public Map<String, Set<String>> getDomainMemberHyponym(String input){
		Map<String, Set<String>> result = new HashMap<String, Set<String>>(); 
		Set<IndexWord> indexWords = this.listIndexWord(input);
		
		for (IndexWord indexWord : indexWords) {
			if (indexWord != null) {
				/* List different senses */
				List<Synset> senses = indexWord.getSenses();
		
				for (Synset sense : senses) {
					/* Get short description of the sense, called the gloss */
//					System.out.println(
//						indexWord + " " + sense.toString() + "(" + sense.getOffset() + ") : " + 
//						sense.getGloss()
//					);			
					
					/* Getting all domain and its hyponym */
					Set<Synset> sensesDomain = this.getSenseDomain(sense);
					for (Synset s : sensesDomain) {
						Set<String> memberList = new HashSet<String>(); 
//						System.out.println(s.getSynset());
						
						/* List other lemma in the synset */
						String lemmaString = this.getLemmaString(s);
						memberList.add(s.getOffset()+"#"+s.getPOS().getKey()+lemmaString+"#"+s.getGloss());
						
						memberList.addAll(this.getMapAllHyponymWord(s));
						
						/* Getting all domain member and its hyponym */
						memberList.addAll(this.getSenseDomainMemberHyponym(s));
						
						result.put(s.getOffset()+"#"+s.getPOS().getKey()+lemmaString+"#"+s.getGloss(), memberList);
					}
				}
			}
		}
		return result;
	}
	
	public List<String> getSenseDomainMemberHyponym(Synset sense){
		List<String> result = new ArrayList<String>();
		
		/* Getting all domain member and its hyponym */
		List<Synset> sensesDomainMember = this.getSenseDomainMember(sense);
		for (Synset sdm : sensesDomainMember) {			
			/* List other lemma in the synset */
			String lemmaString = this.getLemmaString(sdm);
			result.add(sdm.getOffset() + "#" + sdm.getPOS().getKey() + lemmaString + "#" + sdm.getGloss());
			
			result.addAll(this.getMapAllHyponymWord(sdm));
		}
		
		return result;
	}
	
	public List<Synset[]> getFullHyponymLevel(Synset sense){
		List<Synset[]> listAll = new ArrayList<Synset[]>();
		try {
			PointerTargetTree hyponyms = PointerUtils.getHyponymTree(sense);
			List<PointerTargetNodeList> listHyponym = hyponyms.toList();
			for (PointerTargetNodeList ho: listHyponym){
				Synset[] tempLevel = new Synset[ho.size()];
				for (int j = 0; j < ho.size() - 1; j++) {
					tempLevel[j] = ho.get(j).getSynset();
				}
				listAll.add(tempLevel);
			}						
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return listAll;
	}
	
	public Map<String, Synset> getDirectHyponym(Synset sense){
		Map<String, Synset> result = new HashMap<String, Synset>();
		try {
			PointerTargetNodeList hyponym = PointerUtils.getDirectHyponyms(sense);
			for (int i = 0; i < hyponym.size() - 1; i++) {
				System.out.println("Direct hyponym: " + hyponym.get(i).getSynset().getWords().toString());
			}							
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Set<String> getDirectHyponym(char pos, String offset){
		Synset sense = this.getSynsetByOffset(pos, offset);
		Set<String> result = new HashSet<String>();
		label: try {
			PointerTargetNodeList hyponym = PointerUtils.getDirectHyponyms(sense);
			if (hyponym.isEmpty()) {
//				System.out.println("No direct hyponym.");
				break label;
			}
			for (int i = 0; i < hyponym.size() - 1; i++) {
				Synset hypo = hyponym.get(i).getSynset();
//				System.out.println("Direct hyponym: " + hypo.toString());
				result.add(hypo.getOffset()+"#"+hypo.getPOS().getKey()
						+this.getLemmaString(hypo)+"#"+hypo.getGloss());
			}							
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Set<Synset> getDirectHyponymSynset(char pos, String offset){
		Synset sense = this.getSynsetByOffset(pos, offset);
		Set<Synset> result = new HashSet<Synset>();
		label: try {
			PointerTargetNodeList hyponym = PointerUtils.getDirectHyponyms(sense);
			if (hyponym.isEmpty()) {
				break label;
			}
			for (int i = 0; i < hyponym.size() - 1; i++) {
				result.add(hyponym.get(i).getSynset());
			}							
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Set<Synset> getTwoLevelBelowHyponymSynset(char pos, String offset){
		Set<Synset> result = this.getDirectHyponymSynset(pos, offset);
		Set<Synset> temp = new HashSet<Synset>();
		for(Synset r: result) {
			temp.addAll(this.getDirectHyponymSynset(r.getPOS().getKey().charAt(0), Long.toString(r.getOffset())));
		}
		result.addAll(temp);
		return result;
	}
	
	public Set<String> getTwoLevelAboveHypernym(char pos, String offset){
		Set<String> result = this.getDirectHypernym(pos, offset);
		for(String r: result) {
			String[] split = r.split("#");
			result.addAll(this.getDirectHypernym(split[1].charAt(0), split[0]));
		}
		return result;
	}
	
	public Set<Synset> getTwoLevelAboveHypernymSynset(char pos, String offset){
		Set<Synset> result = this.getDirectHypernymSynset(pos, offset);
		Set<Synset> temp = new HashSet<Synset>();
		for(Synset r: result) {
			temp.addAll(this.getDirectHypernymSynset(r.getPOS().getKey().charAt(0), Long.toString(r.getOffset())));
		}
		result.addAll(temp);
		return result;
	}
	
	public Set<String> getDirectHypernym(char pos, String offset){
		Synset sense = this.getSynsetByOffset(pos, offset);
		Set<String> result = new HashSet<String>();
		label: try {
			PointerTargetNodeList hypernym = PointerUtils.getDirectHypernyms(sense);
			if (hypernym.isEmpty()) {
//				System.out.println("No direct hypernym.");
				break label;
			}
			for (int i = hypernym.size() - 1; i >= 0; i--) {
				Synset hyper = hypernym.get(i).getSynset();
//				System.out.println("Direct hypernym: " + hyper.toString());
				result.add(hyper.getOffset()+"#"+hyper.getPOS().getKey()
						+this.getLemmaString(hyper)+"#"+hyper.getGloss());
			}							
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public Set<Synset> getDirectHypernymSynset(char pos, String offset){
		Set<Synset> result = new HashSet<Synset>();
		Synset sense = this.getSynsetByOffset(pos, offset);
		label: try {
			PointerTargetNodeList hypernym = PointerUtils.getDirectHypernyms(sense);
			if (hypernym.isEmpty()) {
				break label;
			}
			for (int i = hypernym.size() - 1; i >= 0; i--) {
				result.add(hypernym.get(i).getSynset());
			}							
		} catch (JWNLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public List<String> getMapAllHyponymWord(Synset sense){
		List<String> result = new ArrayList<String>();
		List<Synset[]> listAll = this.getFullHyponymLevel(sense);
		for(Synset[] all: listAll){
			for (Synset level: all) {
				if(level == null) continue; 
				
				String lemma = this.getLemmaString(level);
				result.add(level.getOffset() + "#" + level.getPOS().getKey() + lemma + "#" + level.getGloss());
				
			}
		}
		return result;
	}

}
