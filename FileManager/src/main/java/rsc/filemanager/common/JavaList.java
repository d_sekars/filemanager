package rsc.filemanager.common;

/**
 * 
 * @author Diah S R
 *
 */

import java.util.ArrayList;
import java.util.List;

public class JavaList<T> {

	/* Concat two list */
	public List<T> concatList(List<T> listA, List<T> listB) {
		List<T> newList = new ArrayList<T>(listA);
		newList.addAll(listB);
		return newList;
	}
	
}
