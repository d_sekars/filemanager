package rsc.filemanager.common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.mysql.fabric.xmlrpc.base.Array;

public class AnnotationValidation {
	
	private PDFBoxManager pbm = new PDFBoxManager();
	private AnnotationProcess ap = new AnnotationProcess();
	private ExtJwnl ej = new ExtJwnl();
	private JenaDBpedia jd = new JenaDBpedia();
	
	public void annotationValidation() throws Exception{
		File folder = new File("D:\\Tomcat\\apache-tomcat-8.0.45\\uploads");
		
		/* Read all files inside folder */
		List<File> listFile = pbm.listFilesForFolder(folder);
		for(File lf: listFile){
			this.annotationFileValidation(lf);
		}
	}
	
	public void annotationFileValidation(File file) throws Exception{
		System.out.println();
		System.out.println("Annotation validation for " + file.getName());
		System.out.println();
		
		pbm.setFilePath(file.getAbsolutePath());
		String pdfContent = pbm.ToText(1,1);
		Set<String> setAnno = new HashSet<String>(Arrays.asList(pbm.retrieveDocumentInformation(file, "annotation").split(";")));
		Set<String> temp = new HashSet<String>();
		
		System.out.println("Number of annotation list: " + setAnno.size());
		
		Map<String,String> mapTemp = new HashMap<String, String>();
		for(String sa: setAnno){
			String wordnetAnno = pbm.retrieveDocumentInformation(file, "wordnet:"+sa);
			if(wordnetAnno!=null && !wordnetAnno.isEmpty()) {
				mapTemp.put("wordnet:"+sa, wordnetAnno);
			}
			String dbpediaAnno = pbm.retrieveDocumentInformation(file, "dbpedia:"+sa);
			if(dbpediaAnno!=null && !dbpediaAnno.isEmpty()) {
				mapTemp.put("dbpedia:"+sa, dbpediaAnno);
			}
		}
		
		/* Count number of annotation that matched file's keywords */
		temp.addAll(ap.getKeywords(file.getAbsolutePath())); /* Get keywords */
		int tempNum = temp.size();
		temp.retainAll(setAnno);
		if(tempNum>0)
			System.out.println("Number of annotation validated: " + 
			temp.size() + " of " + tempNum + " (" + ((double)temp.size()/(double)tempNum)*100 + "%)");
		else
			System.out.println("No keywords found in annotation.");
		
		/* Count number of annotation that related to the keywords, dbpedia and text (based on wordnet) 
		 * (1) member of the same domain
		 * (2) hypernym of matched words */
		temp.addAll(ej.validateAnnotation(mapTemp, temp, pdfContent));
		
		/* Count number of annotation that related to the keywords (based on dbpedia) 
		 * (1) category of matched words
		 * (2) related wordnet result */
		temp.addAll(jd.validateAnnotation(mapTemp, temp, pdfContent));
		
		if(temp.size()>0)
			System.out.println("Number of annotation validated: " + 
			temp.size() + " of " + setAnno.size() + " (" + ((double)temp.size()/(double)setAnno.size())*100 + "%)");
		else
			System.out.println("No annotation found.");

		System.out.println();
	}

}
