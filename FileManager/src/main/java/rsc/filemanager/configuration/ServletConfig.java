package rsc.filemanager.configuration;

/**
 * Adopt code from:
 * - https://www.mkyong.com/spring-security/spring-security-custom-login-form-annotation-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class ServletConfig extends WebMvcConfigurerAdapter {}
