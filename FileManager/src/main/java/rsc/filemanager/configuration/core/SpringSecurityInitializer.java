package rsc.filemanager.configuration.core;

/**
 * Adopt code from:
 * - https://www.mkyong.com/spring-security/spring-security-custom-login-form-annotation-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import javax.servlet.ServletContext;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.web.multipart.support.MultipartFilter;

public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

	 @Override
	    protected void beforeSpringSecurityFilterChain(ServletContext servletContext) {
	        insertFilters(servletContext, new MultipartFilter());
	    }
	
}
