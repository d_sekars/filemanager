package rsc.filemanager.configuration.core;

/**
 * Adopt code from:
 * - https://www.mkyong.com/spring-security/spring-security-custom-login-form-annotation-example/
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.io.File;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletRegistration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import rsc.filemanager.configuration.RootConfig;
import rsc.filemanager.configuration.ServletConfig;

@Configuration
public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
      return new Class[]{RootConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
      return new Class[]{ServletConfig.class};
    }

    @Override
    protected String[] getServletMappings() { return new String[]{"/"}; }
    
    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        registration.setMultipartConfig(getMultipartConfigElement());
    }
 
    private MultipartConfigElement getMultipartConfigElement() {
    	
    	/* Temporary location where files will be stored */
    	String rootPath = System.getProperty("catalina.home");
    	File dir = new File(rootPath + File.separator + "temp");
    	if (!dir.exists())
    		dir.mkdirs();
        final String LOCATION = dir.getAbsolutePath();
     
        /* Maximum file size (set to 25MB), beyond that size, system will throw exception */
        final long MAX_FILE_SIZE = 1024 * 1024 * 25;
        
        /* Total request size containing Multi part (set to 30MB) */
        final long MAX_REQUEST_SIZE = 1024 * 1024 * 30;
        
        /* Size threshold after which files will be written to disk */
        final int FILE_SIZE_THRESHOLD = 0;
    	
        MultipartConfigElement multipartConfigElement = new MultipartConfigElement( LOCATION, MAX_FILE_SIZE, MAX_REQUEST_SIZE, FILE_SIZE_THRESHOLD);
        return multipartConfigElement;
    }
    
}
