package rsc.filemanager.dao;

/**
 * @author Diah S R
 */

import java.util.List;
import rsc.filemanager.model.DocumentDictionary;

public interface DocumentDictionaryDao {
	
	DocumentDictionary findById(int dcId);
	
	DocumentDictionary findByToken(String token);
	
	List<DocumentDictionary> findByTokenLike(String token);
    
    void saveDictionary(DocumentDictionary dictionary);
    
    void updateDocFrequency(String token, int frequency);
    
    void deleteToken(int dictionaryId);
     
    List<DocumentDictionary> findAll();

}
