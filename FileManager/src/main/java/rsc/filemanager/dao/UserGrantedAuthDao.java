package rsc.filemanager.dao;

/**
 * @author Diah S R
 */

import rsc.filemanager.model.UserGrantedAuth;

public interface UserGrantedAuthDao {
	
	void save(UserGrantedAuth userGrantedAuth);
	
	void updateGrantedUser(String oldGrantedUser, String newGrantedUser);

}
