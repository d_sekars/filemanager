package rsc.filemanager.dao;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.List;
import rsc.filemanager.model.UserDocument;
 
public interface UserDocumentDao {
 
    List<UserDocument> findAll();
    
    List<UserDocument> findAllByUsername(String username);
     
    UserDocument findByDocId(int docId);
    
    UserDocument findByDocName(String username, String docName);
    
    /* Get biggest number of docId */
    int findBiggestDocId();
     
    void save(UserDocument document);
    
    /* Update document's user */
    void updateUser(String oldUser, String newUser);
    
    /* Update document's content */
    void updateContent(int docId, byte[] docContent);
     
    void deleteByDocId(int docId);
    
}
