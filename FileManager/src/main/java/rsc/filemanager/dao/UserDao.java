package rsc.filemanager.dao;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.List;
import rsc.filemanager.model.User;

public interface UserDao {
 
    User findByUsername(String username);
    
    void saveUser(User user);
    
    /* Update username */
    void updateUsername(String oldUsername, String newUsername);
    
    void deleteUser(String username);
     
    List<User> findAllUsers();
     
}
