package rsc.filemanager.dao;

import java.util.List;

import rsc.filemanager.model.AnnotationMapping;

/**
 * 
 * @author Diah S R
 *
 */

public interface AnnotationMappingDao {
	
	List<AnnotationMapping> findAllAnnoByDocId(int docId);
	
	List<AnnotationMapping> findAllAnnoByCatDocId(int annoCatId, int docId);
	
	AnnotationMapping findAnnoByAllId(int annoCatId, int docId, int docDicId);
	
	void save(AnnotationMapping annotationMapping);
	
	int findBiggestId();
	
	void deleteByDocId(int docId);
}
