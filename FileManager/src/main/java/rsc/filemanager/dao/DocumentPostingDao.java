package rsc.filemanager.dao;

import java.util.List;

/**
 * 
 * @author Diah S R
 *
 */

import rsc.filemanager.model.DocumentPosting;

public interface DocumentPostingDao {
	
	List<DocumentPosting> findByDocId(int docId);
	
	DocumentPosting findPostingByDocId(int docId, int dictioId);
	
	void save(DocumentPosting documentPosting);
	
	void deleteByDocId(int docId);
}
