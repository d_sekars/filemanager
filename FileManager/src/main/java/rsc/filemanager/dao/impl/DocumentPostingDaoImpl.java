package rsc.filemanager.dao.impl;

import java.util.List;

/**
 * 
 * @author Diah S R
 *
 */

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rsc.filemanager.dao.AbstractDao;
import rsc.filemanager.dao.DocumentPostingDao;
import rsc.filemanager.model.DocumentPosting;

@Repository("documentPostingDao")
public class DocumentPostingDaoImpl
extends AbstractDao<Integer, DocumentPosting> implements DocumentPostingDao {
	
	@SuppressWarnings("unchecked")
	public List<DocumentPosting> findByDocId(int docId){
		Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("documentId", docId));
        return (List<DocumentPosting>) criteria.list();
	}
	
	public DocumentPosting findPostingByDocId(int docId, int dictioId){
		Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("documentId", docId));
        criteria.add(Restrictions.eq("dictionaryId", dictioId));
        return (DocumentPosting) criteria.uniqueResult();
	}
	
	public void save(DocumentPosting documentPosting){
		persist(documentPosting);
	}
	
	public void deleteByDocId(int docId){
		List<DocumentPosting> posting = (List<DocumentPosting>) findByDocId(docId);
		for(DocumentPosting post: posting){
			delete(post);
		}
	}
}
