package rsc.filemanager.dao.impl;

/**
 * @author Diah S R
 */

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rsc.filemanager.dao.AbstractDao;
import rsc.filemanager.dao.DocumentDictionaryDao;
import rsc.filemanager.model.DocumentDictionary;

@Repository("documentDictionaryDao")
public class DocumentDictionaryDaoImpl extends AbstractDao<Integer, DocumentDictionary> implements DocumentDictionaryDao {
	
	public DocumentDictionary findById(int dcId) {
		Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("dictionaryId", dcId));
        return (DocumentDictionary) criteria.uniqueResult();
	}
	
	public DocumentDictionary findByToken(String token) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("dictionaryToken", token));
        return (DocumentDictionary) criteria.uniqueResult();
    }
	
	@SuppressWarnings("unchecked")
	public List<DocumentDictionary> findByTokenLike(String token) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.like("dictionaryToken", token, MatchMode.ANYWHERE));
		return (List<DocumentDictionary>)criteria.list();
	}
	
	public void saveDictionary(DocumentDictionary dictionary) {
        persist(dictionary);
    }
	
	public void updateDocFrequency(String token, int frequency) {
		Query query = getSession().createQuery("update DocumentDictionary set docFrequency = :frequency" +
    			" where dictionaryToken = :token");
    	query.setParameter("frequency", frequency);
    	query.setParameter("token", token);
    	query.executeUpdate();
	}
	
	public void deleteToken(int dictionaryId) {
        DocumentDictionary dictionary = getByKey(dictionaryId);
        delete(dictionary);
    }
	
	@SuppressWarnings("unchecked")
    public List<DocumentDictionary> findAll(){
		Criteria crit = createEntityCriteria();
        return (List<DocumentDictionary>)crit.list();
    }
	
}
