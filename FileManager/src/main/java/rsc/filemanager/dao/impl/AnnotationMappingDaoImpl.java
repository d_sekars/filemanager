package rsc.filemanager.dao.impl;

import java.util.List;

/**
 * 
 * @author Diah S R
 *
 */

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rsc.filemanager.dao.AbstractDao;
import rsc.filemanager.dao.AnnotationMappingDao;
import rsc.filemanager.model.AnnotationMapping;
import rsc.filemanager.model.DocumentPosting;

@Repository("AnnotationMappingDao")
public class AnnotationMappingDaoImpl
extends AbstractDao<Integer, AnnotationMapping> implements AnnotationMappingDao {
	
	@SuppressWarnings("unchecked")
	public List<AnnotationMapping> findAllAnnoByDocId(int docId){
		Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("docId", docId));
        return (List<AnnotationMapping>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<AnnotationMapping> findAllAnnoByCatDocId(int annoCatId, int docId){
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("annoCatId", annoCatId));
        criteria.add(Restrictions.eq("docId", docId));
        return (List<AnnotationMapping>) criteria.list();
	}
	
	public AnnotationMapping findAnnoByAllId(int annoCatId, int docId, int docDicId){
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("annoCatId", annoCatId));
        criteria.add(Restrictions.eq("docId", docId));
        criteria.add(Restrictions.eq("docDicId", docDicId));
        return (AnnotationMapping) criteria.uniqueResult();
	}
	
	public void save(AnnotationMapping annotationMapping){
		persist(annotationMapping);
	}
	
	public int findBiggestId(){
    	int maxId = 0;
    	Criteria criteria = createEntityCriteria();
    	criteria.setProjection(Projections.max("annoId"));
    	try {
    		maxId = (int)criteria.uniqueResult();
    	} catch (java.lang.NullPointerException e) {
    		maxId = 0;
    	}
    	return maxId;
    }
	
	public void deleteByDocId(int docId){
		List<AnnotationMapping> posting = (List<AnnotationMapping>) findAllAnnoByDocId(docId);
		for(AnnotationMapping post: posting){
			delete(post);
		}
	}
}
