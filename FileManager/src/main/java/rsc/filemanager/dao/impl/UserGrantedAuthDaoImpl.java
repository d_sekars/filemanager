package rsc.filemanager.dao.impl;

import org.hibernate.Query;

/**
 * @author Diah S R
 */

import org.springframework.stereotype.Repository;
import rsc.filemanager.dao.AbstractDao;
import rsc.filemanager.dao.UserGrantedAuthDao;
import rsc.filemanager.model.UserGrantedAuth;

@Repository("userGrantedAuthDao")
public class UserGrantedAuthDaoImpl 
extends AbstractDao<Integer, UserGrantedAuth> implements UserGrantedAuthDao {
	
	public void save(UserGrantedAuth userGrantedAuth) {
        persist(userGrantedAuth);
    }
	
	public void updateGrantedUser(String oldGrantedUser, String newGrantedUser) {
		Query query = getSession().createQuery("update UserGrantedAuth set" +
				" grantedUser = :newGrantedUser" +
    			" where grantedUser = :oldGrantedUser");
    	query.setParameter("newGrantedUser", newGrantedUser);
    	query.setParameter("oldGrantedUser", oldGrantedUser);
    	query.executeUpdate();
	}
}
