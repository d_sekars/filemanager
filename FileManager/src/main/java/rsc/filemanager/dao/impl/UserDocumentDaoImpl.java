package rsc.filemanager.dao.impl;

/**
 * Adopt code from:
 * - http://websystique.com/springmvc/spring-mvc-4-fileupload-download-hibernate-example/
 * 
 * Some changes added to cater the business process requirements.
 */

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rsc.filemanager.dao.AbstractDao;
import rsc.filemanager.dao.UserDocumentDao;
import rsc.filemanager.model.UserDocument;
 
@Repository("userDocumentDao")
public class UserDocumentDaoImpl 
extends AbstractDao<Integer, UserDocument> implements UserDocumentDao {
 
    @SuppressWarnings("unchecked")
    public List<UserDocument> findAll() {
        Criteria crit = createEntityCriteria();
        return (List<UserDocument>)crit.list();
    }
 
    @SuppressWarnings("unchecked")
    public List<UserDocument> findAllByUsername(String username){
        Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("docUser", username)).addOrder(Order.asc("docName"));
        return (List<UserDocument>)crit.list();
    }
     
    public UserDocument findByDocId(int docId) {
        return getByKey(docId);
    }
    
    public UserDocument findByDocName(String username, String docName){
    	Criteria crit = createEntityCriteria();
    	crit.add(Restrictions.eq("docUser", username));
        crit.add(Restrictions.eq("docName", docName));
        return (UserDocument)crit.uniqueResult();
    }
    
    public int findBiggestDocId(){
    	int maxId = 0;
    	Criteria criteria = createEntityCriteria();
    	criteria.setProjection(Projections.max("docId"));
    	try {
    		maxId = (int)criteria.uniqueResult();
    	} catch (java.lang.NullPointerException e) {
    		maxId = 0;
    	}
    	return maxId;
    }
    
    public void save(UserDocument document) {
        persist(document);
    }
    
    public void updateUser(String oldUser, String newUser){
    	Query query = getSession().createQuery("update UserDocument set docUser = :newUser" +
    			" where docUser = :oldUser");
    	query.setParameter("newUser", newUser);
    	query.setParameter("oldUser", oldUser);
    	query.executeUpdate();
    }
    
    public void updateContent(int docId, byte[] docContent){
    	Query query = getSession().createQuery("update UserDocument set docContent = :docContent," +
    			" docAnnotationFlag = 1 where docId = :docId");
    	query.setParameter("docId", docId);
    	query.setParameter("docContent", docContent);
    	query.executeUpdate();
    }
 
     
    public void deleteByDocId(int docId) {
        UserDocument document =  getByKey(docId);
        delete(document);
    }
 
}
