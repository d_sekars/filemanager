package rsc.filemanager.dao.impl;

import org.springframework.stereotype.Repository;

import rsc.filemanager.dao.AbstractDao;
import rsc.filemanager.dao.WordNetDomainDao;
import rsc.filemanager.model.WordNetDomain;

/**
 * 
 * @author Diah S R
 *
 */

@Repository("wordNetDomainDao")
public class WordNetDomainDaoImpl extends AbstractDao<Integer, WordNetDomain> implements WordNetDomainDao {

	public void saveDomain(WordNetDomain domain){
		persist(domain);
	}
	
}
