package rsc.filemanager.dao.impl;

import java.util.List;

/**
 * 
 * @author Diah S R
 *
 */

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rsc.filemanager.dao.AbstractDao;
import rsc.filemanager.dao.AnnotationDao;
import rsc.filemanager.model.Annotation;

@Repository("AnnotationDao")
public class AnnotationDaoImpl
extends AbstractDao<Integer, Annotation> implements AnnotationDao {
	
	@SuppressWarnings("unchecked")
	public List<Annotation> findAllAnnoByDocId(int docId){
		Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("docId", docId));
        return (List<Annotation>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Annotation> findAllAnnoByCatDocId(int annoCatId, int docId){
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("annoCatId", annoCatId));
        criteria.add(Restrictions.eq("docId", docId));
        return (List<Annotation>) criteria.list();
	}
	
	public void save(Annotation annotation){
		persist(annotation);
	}
	
	public int findBiggestId(){
    	int maxId = 0;
    	Criteria criteria = createEntityCriteria();
    	criteria.setProjection(Projections.max("annoId"));
    	try {
    		maxId = (int)criteria.uniqueResult();
    	} catch (java.lang.NullPointerException e) {
    		maxId = 0;
    	}
    	return maxId;
    }
}
