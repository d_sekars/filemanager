package rsc.filemanager.dao;

import rsc.filemanager.model.WordNetDomain;

/**
 * 
 * @author Diah S R
 *
 */

public interface WordNetDomainDao {
	
	/* Insert domain */
	void saveDomain(WordNetDomain domain);
	
}
