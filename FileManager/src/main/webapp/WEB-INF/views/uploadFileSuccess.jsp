<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--
 * Adopt code from:
 * - http://www.journaldev.com/2573/spring-mvc-file-upload-example-single-multiple-files
 * 
 * Some changes added to cater the business process requirements.
 -->

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>File Manager</title>
	</head>
	<body>
		<h1>File(s) Upload</h1>
		<p>Following files are uploaded successfully.</p>
		<ol>
			<c:forEach items="${files}" var="file">
				<li>${file}</li>
			</c:forEach>
		</ol>
		<a href="<c:url value='/home' />">Home</a>
	</body>
</html>