<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--
 * Adopt code from:
 * - http://websystique.com/spring-security/spring-security-4-hibernate-annotation-example/
 * 
 * Some changes added to cater the business process requirements.
 -->

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>File Manager</title>
	</head>
	<body>
		<div>
	        <div>
	              <!-- Default panel contents -->
	            <div><span>List of Users </span></div>
	            <div>
	                <table>
	                    <thead>
	                        <tr>
	                            <th>Username</th>
	                            <th width="100"></th>
	                            <th width="100"></th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    <c:forEach items="${users}" var="user">
	                        <tr>
	                            <td>${user.username}</td>
	                            <td><a href="<c:url value='/edit-user-${user.username}' />">edit</a></td>
	                            <td><a href="<c:url value='/delete-user-${user.username}' />">delete</a></td>
	                        </tr>
	                    </c:forEach>
	                    </tbody>
	                </table>
	            </div>
	        </div>
	        <div>
	            <a href="<c:url value='/signup' />">Add New User</a>
	        </div>
    	</div>
	</body>
</html>