<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--
 * Adopt code from:
 * - http://websystique.com/spring-security/spring-security-4-hibernate-annotation-example/
 * 
 * Some changes added to cater the business process requirements.
 -->

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>File Manager</title>
	</head>
	<body>
		<div>
	        <div>
	            ${success}
	        </div>
	         
	        <span>
	            <a href="<c:url value='/add-document-${username}' />">Click here to upload/manage your documents</a>   
	        </span>
	        <span>
	            Go to <a href="<c:url value='/userlist' />">Users List</a>
	        </span>
    	</div>
	</body>
</html>