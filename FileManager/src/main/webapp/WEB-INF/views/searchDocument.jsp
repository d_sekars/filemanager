<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link href = "<c:url value = "/resources/css/main.css" />" rel = "stylesheet" >
		<title>File Manager - Search Documents</title>
	</head>
	<body>
		<div>
			<ul class = "mainnavbar">
				<li>
					<a href="<c:url value='/home' />">Home</a>
				</li>
				<li>
					<a href="<c:url value='/edit-user-${username}' />">User Management</a>
				</li>
				<li>
					<a href="<c:url value='/add-document-${username}' />">Document Management</a>
				</li>
				<li>
					<a href="<c:url value='/search-document-${username}' />">Search Document</a>
				</li>
			</ul>
    	</div>
    	<div class = "main">
    	<div class = "block">
             
            	<div><span>Search Document</span></div>
            	<div>
                	<form:form method="POST" modelAttribute="searchDocument" enctype="multipart/form-data">
                		<div>
                		<table class = "transparent">
		                    <tr>
		                        <td style="width:50%">
		                        	<form:input type="text" width="100" path="searchQuery" id="searchQuery"/>
		                        	<input type="submit" value="Search"/>
		                        </td>
		                        <td style="width:50%">
		                        	<form:radiobutton path="annoReference" value="file" label="Embedded Annotation" />
            						<form:radiobutton path="annoReference" value="db" label="Database" />
								</td>
		                    </tr>
		                    <tr>
		                        <td colspan="2">
		                        	<c:if test="${not empty query}">
		                        	<p>Searching result of "<span style="color:green">${query}</span>"</p>
		                        	</c:if>
		                        </td>
		                    </tr>
		                    <c:if test="${not empty query}">
		                    	<tr>
		                        	<td style="width:50%">
		                        		<p>Execution time ${irTime} milliseconds with VSM Weighting</p>
		                        	</td>
		                        	<td style="width:50%">
		                        		<p>Execution time ${annoTime} milliseconds with Document Annotation</p>
		                        	</td>
		                        </tr>
		                        <tr valign="top">
		                        	<td>
		                        		<table class = "common">
		                        			<tr>
		                    					<th class = "common">No.</th>
		                        				<th class = "common">File</th>
                        					</tr>
		                        			<c:forEach items="${irRows}" var="r" varStatus="counter">
		                        			<tr>
		                        				<c:if test="${not empty irResult[r].documentName}"><td class = "common">${counter.index + 1}</td></c:if>
		                        				<td class = "common">
		                        					<a href="<c:url value='/download-document-${user}-${irResult[r].documentId}' />" >
		                            					${irResult[r].documentName}
		                            				</a>
		                        				</td>
		                        			</tr>
		                        			<!--tr>
		                        				<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${irResult[r].documentScore}"/></td>
		                        			</tr-->
		                        			</c:forEach>
		                        		</table>
		                        	</td>
		                        	<td>
		                        		<table class = "common">
		                        			<tr>
		                    					<th class = "common">No.</th>
		                        				<th class = "common">File</th>
                        					</tr>
		                        			<c:forEach items="${anRows}" var="r" varStatus="counter">
		                        			<tr>
		                        				<c:if test="${not empty annoResult[r].documentName}"><td class = "common">${counter.index + 1}</td></c:if>
		                        				<td class = "common">
		                        					<a href="<c:url value='/download-annotation-${user}-${annoResult[r].documentId}' />" >
		                            					${annoResult[r].documentName}
		                            				</a>
		                        				</td>
		                        			</tr>
		                        			<!--tr>
		                        				<td><fmt:formatNumber type="number" maxFractionDigits="3" value="${annoResult[r].documentScore}"/></td>
		                        			</tr-->
		                        			</c:forEach>
		                        		</table>
		                        	</td>
		                        </tr>
							</c:if>
	                    </table>
     					</div>
                	</form:form>
                </div>
        	</div>
        	</div>
	</body>
</html>