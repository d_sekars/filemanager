<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--
 * Adopt code from:
 * - http://websystique.com/spring-security/spring-security-4-hibernate-annotation-example/
 * 
 * Some changes added to cater the business process requirements.
 -->

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link href = "<c:url value = "/resources/css/main.css" />" rel = "stylesheet" >
		<title>File Manager</title>
	</head>
	<body>
		<c:if test="${edit}">
			<div>
				<ul class = "mainnavbar">
					<li>
						<a href="<c:url value='/home' />">Home</a>
					</li>
					<li>
						<a href="<c:url value='/edit-user-${username}' />">User Management</a>
					</li>
					<li>
						<a href="<c:url value='/add-document-${username}' />">Document Management</a>
					</li>
					<li>
						<a>Search Document</a>
					</li>
				</ul>
    		</div>
		</c:if>
		<div>
		    <div>User Registration Form</div>
		    <form:form method="POST" modelAttribute="user">
		        
		        <div>
		            <div>
		                <label for="username">Username</label>
		                <div>
		                    <form:input type="text" path="username" id="username"/>
		                    <div>
		                        <form:errors path="username"/>
		                    </div>
		                </div>
		            </div>
		        </div>
		        
		        <div>
		            <div>
		                <label for="password">Password</label>
		                <div>
		                    <form:input type="password" path="password" id="password"/>
		                    <div>
		                        <form:errors path="password"/>
		                    </div>
		                </div>
		            </div>
		        </div>
		 
		        <div>
		            <div>
		                <c:choose>
		                    <c:when test="${edit}">
		                        <input type="submit" value="Update"/> or <a href="<c:url value='/userlist' />">Cancel</a>
		                    </c:when>
		                    <c:otherwise>
		                        <input type="submit" value="Register"/> or <a href="<c:url value='/userlist' />">Cancel</a>
		                    </c:otherwise>
		                </c:choose>
		            </div>
		        </div>
		         
		        <c:if test="${edit}">
		            <span>
		                <a href="<c:url value='/add-document-${username}' />">Click here to upload/manage your documents</a>   
		            </span>
		        </c:if>
		         
		    </form:form>
	    </div>
	</body>
</html>