<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--
 * Adopt code from:
 * - http://websystique.com/spring-security/spring-security-4-hibernate-annotation-example/
 * 
 * Some changes added to cater the business process requirements.
 -->

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link href = "<c:url value = "/resources/css/main.css" />" rel = "stylesheet" >
		<title>File Manager - Manage Documents</title>
	</head>
	<body>
		<div>
			<ul class = "mainnavbar">
				<li>
					<a href="<c:url value='/home' />">Home</a>
				</li>
				<li>
					<a href="<c:url value='/edit-user-${username}' />">User Management</a>
				</li>
				<li>
					<a href="<c:url value='/add-document-${username}' />">Document Management</a>
				</li>
				<li>
					<a href="<c:url value='/search-document-${username}' />">Search Document</a>
				</li>
			</ul>
    	</div>
		<div class = "main">
        	<div class = "block">
				<!-- Default panel contents -->
				<div class = "header"><span>List of Documents</span></div>
				<div>
					<table class = "common">
						<thead>
							<tr>
		                    	<th class = "common">No.</th>
		                        <th class = "common">File Name</th>
		                        <th class = "common">Type</th>
		                        <th class = "common">Description</th>
		                        <th class = "common" colspan="4">Action</th>
                        	</tr>
                    	</thead>
                    	<tbody>
                    		<c:forEach items="${documents}" var="doc" varStatus="counter">
		                        <tr>
		                            <td class = "common">${counter.index + 1}</td>
		                            <td class = "common">${doc.docName}</td>
		                            <td class = "common">${doc.docType}</td>
		                            <td class = "common">${doc.docDesc}</td>
		                            <td class = "common">
		                            	<a href="<c:url value='/download-document-${username}-${doc.docId}' />" >
		                            		download
		                            	</a>
		                            </td>
		                            <td class = "common">
		                            	<a href="<c:url value='/delete-document-${username}-${doc.docId}' />" >
		                            		delete
		                            	</a>
		                            </td>
		                            <td class = "common">
		                            	<c:choose>
    										<c:when test="${doc.docAnnotationFlag == '0'}">
        										<a href="<c:url value='/add-annotation-document-${username}-${doc.docId}' />" >
		                            				add annotation
		                            			</a>
    										</c:when>    
    										<c:otherwise>
        										add annotation 
    										</c:otherwise>
										</c:choose>
		                            </td>
		                            <td class = "common">
		                            	<a href="<c:url value='/migrate-annotation-document-${username}-${doc.docId}' />" >
		                            		migrate annotation to database
		                            	</a>
		                            </td>
		                        </tr>
                    		</c:forEach>
                    	</tbody>
                	</table>
            	</div>
        	</div>
        	<div class = "block">
             
            	<div><span>Upload New Document</span></div>
            	<div>
                	<form:form method="POST" modelAttribute="fileUpload" enctype="multipart/form-data">
                		
                		<table class = "transparent">
             
		                    <tr>
		                    	<td><label for="file">Upload a document</label></td>
		                    	<td>: </td>
		                        <td>
		                        	<form:input type="file" path="file" id="file"/>
		                        	<form:errors path="file"/>
		                        </td>
		                    </tr>
		                    
		                    <tr>
								<td><label for="description">Description: </label></td>
								<td>: </td>
								<td><form:input size="100" type="text" path="description" id="description"/></td>
		                    </tr>
	             
		                    <tr>
		                    	<td><input type="submit" value="Upload"/></td>
		                    	<td></td>
		                    </tr>
	                    
	                    </table>
     
                	</form:form>
                </div>
        	</div>
	        <div class = "block">
	            Go to <a href="<c:url value='/userlist' />">Users List</a>
	        </div>
    	</div>
	</body>
</html>