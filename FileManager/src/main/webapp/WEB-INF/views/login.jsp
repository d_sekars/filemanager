<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<!--
 * Adopt code from:
 * - https://www.mkyong.com/spring-security/spring-security-custom-login-form-annotation-example/
 * 
 * Some changes added to cater the business process requirements.
 -->

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>File Manager</title>
	</head>
	<body onload='document.loginForm.username.focus();'>
		<h3>File Manager</h3>
	
		<c:if test="${not empty error}"><div>${error}</div></c:if>
		<c:if test="${not empty message}"><div>${message}</div></c:if>
	
		<form name='loginForm' action="<c:url value='/login' />" method='POST'>
			<table>
				<tr>
					<td>Username:</td>
					<td><input type='text' name='username' value=''></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><input type='password' name='password' /></td>
				</tr>
				<tr>
					<td colspan='2'><input name="submit" type="submit" value="submit" /></td>
				</tr>
			</table>
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
		<br/>
		<a href="<c:url value='/signup' />">Register New User</a>
	</body>
</html>