<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" session="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href = "<c:url value = "/resources/css/main.css" />" rel = "stylesheet" >
		<title>File Manager - Home</title>
	</head>
	<body>		
		<div>
			<ul class = "mainnavbar">
				<li>
					<a href="<c:url value='/home' />">Home</a>
				</li>
				<li>
					<a href="<c:url value='/edit-user-${username}' />">User Management</a>
				</li>
				<li>
					<a href="<c:url value='/add-document-${username}' />">Document Management</a>
				</li>
				<li>
					<a href="<c:url value='/search-document-${username}' />">Search Document</a>
				</li>
			</ul>
    	</div>
	</body>
</html>